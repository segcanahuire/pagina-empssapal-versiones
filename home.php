<?php 
include 'conexion/conexion.php';?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <title>EMPSSAPAL SA</title>
    <meta name="description" content="Material Style Theme">
    <link rel="shortcut icon" href="assets/img/empssapal/empssapal.jpg?v=3">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="assets/css/preload.min.css">
    <link rel="stylesheet" href="assets/css/plugins.min.css">
    <link rel="stylesheet" href="assets/css/style.light-blue-500.min.css">
  </head>
<?php

$sel1=$con->query("SELECT * FROM aviso a, publicacion b WHERE a.idaviso=b.idaviso and a.estado=1");
$f1 = $sel1->fetch_assoc();
$valora=$f1['tipo'];
$valor1=$f1['urlimagen'];
$valor2=$f1['tituloimagen'];
$valor3=$f1['titulotexto'];
$valor4=$f1['cuerpotexto'];
$valor5=$f1['piepaginatexto'];
$valor6=$f1['extencionvideo'];

//IMAGEN 1
//TEXTO 2
//VIDEO 3
//NADA 0
if ($valora!=0) {
  if (2==$valora) {?>
    <body onload="texto()">
    <?php 
  }
  if(1==$valora){?>
    <body onload="imagen()">
  <?php }
  if (3==$valora) {?>
    <body onload="video()">
  <?php }
  if (4==$valora) {?>
    <body onload="textoimagen()">
  <?php } ?>


<?php } else{
?>
<body>
<?php } ?>
    <div id="ms-preload" class="ms-preload">
      <div id="status">
        <div class="spinner">
          <div class="dot1"></div>
          <div class="dot2"></div>
        </div>
      </div>
    </div>
    <div class="ms-site-container">
      <!-- Modal -->
      <div class="modal modal-primary" id="ms-account-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog animated zoomIn animated-3x" role="document">
          <div class="modal-content">
            <div class="modal-header d-block shadow-2dp no-pb">
              <button type="button" class="close d-inline pull-right mt-2" data-dismiss="modal" aria-label="Close">
                
              </button>
              <div class="modal-title text-center">
                <img src="assets/img/empssapal/empssapal.jpg" width="50%" height="50%">
               
              </div>
              <div class="modal-header-tabs">
                <ul class="nav nav-tabs nav-tabs-full nav-tabs-3 nav-tabs-primary" role="tablist">
                  <li class="nav-item" role="presentation">
                    <a href="#ms-login-tab" aria-controls="ms-login-tab" role="tab" data-toggle="tab" class="nav-link active withoutripple">
                      <i class="zmdi zmdi-account"></i> ACCESO</a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="modal-body">
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade active show" id="ms-login-tab">
                  <form action="login/index.php" method="POST" autocomplete="off">
                    <fieldset>
                      <div class="form-group label-floating">
                        <div class="input-group">
                          <span class="input-group-addon">
                            <i class="zmdi zmdi-account"></i>
                          </span>
                          <label class="control-label" for="ms-form-user">Usuario</label>
                          <input type="text" name="usuario" id="usuario" class="form-control"> </div>
                      </div>
                      <div class="form-group label-floating">
                        <div class="input-group">
                          <span class="input-group-addon">
                            <i class="zmdi zmdi-lock"></i>
                          </span>
                          <label class="control-label" for="ms-form-pass">Contraseña</label>
                          <input type="password"  name="contra" id="contra"  class="form-control"> </div>
                      </div>
                      <div class="row mt-2">
                        <div class="col-md-6">
                          <div class="form-group no-mt">
                            <div class="checkbox">
                              <label>
                                <input type="checkbox"> Recordar Contraseña </label>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <button class="btn btn-raised btn-primary pull-right">INGRESAR</button>
                        </div>
                      </div>
                    </fieldset>
                  </form>
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
      <header class="ms-header">
        <!--ms-header-white-->
         <a href="index.php"><img src="assets/img/checca/logorojo.jpg" width="80%"  height="1%"></a>
        
  <div class="header-right">
             <script languaje="JavaScript">
              var mydate=new Date()
              var year=mydate.getYear()
              if (year < 1000)
              year+=1900
              var day=mydate.getDay()
              var month=mydate.getMonth()
              var daym=mydate.getDate()
              if (daym<10)
              daym="0"+daym
              var dayarray=new Array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado")
              var montharray=new

              Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre")
              document.write(""+dayarray[day]+", "+daym+" de "+montharray[month]+" de "+year+"")
              </script><br></br>


            <a href="https://www.youtube.com/channel/UCdg82YQefxbGK43Lu4fyZgg?view_as=subscriber"  target="_blank" class="btn-circle btn-circle-danger">
                <i class="zmdi zmdi-youtube-play"></i>
            </a>
            <a href="https://www.facebook.com/empssapal.sicuani.7" target="_blank" class="btn-circle btn-circle-primary animated  animation-delay-7">
                <i class="zmdi zmdi-facebook-box"></i>
            </a>            
            
            <a href="javascript:void(0)" class="btn-circle btn-circle-primary no-focus animated zoomInDown animation-delay-8" data-toggle="modal" data-target="#ms-account-modal">
              <i class="zmdi zmdi-account"></i>
            </a>
            
            <a href="javascript:void(0)" class="btn-ms-menu btn-circle btn-circle-primary ms-toggle-left animated zoomInDown animation-delay-10">
              <i class="zmdi zmdi-menu"></i>
            </a><br>
            
             <br> </br>
      

        </div>
      </header>
      <nav class="navbar navbar-expand-md  navbar-static ms-navbar ms-navbar-primary">
        <div class="container container-full">
          <div class="navbar-header">
             <img src="assets/img/empssapal/empssapal.jpg" width="10%" height="80%">
          <center><h4> ---       EMPSSAPAL       ---</h4></center>
          </div>

          <div class="collapse navbar-collapse" id="ms-navbar">
            <ul class="navbar-nav">
             
              <li class="nav-item dropdown">
                <a href="mantenimiento.php" class="nav-link dropdown-toggle animated fadeIn animation-delay-7" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-name="page">QUIENES SOMOS
                  <i class="zmdi zmdi-chevron-down"></i>
                </a>
                <ul class="dropdown-menu">
                  
                  <li>
                    <a class="dropdown-item" href="historia.php" class="dropdown-link">Historia</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="misionvision.php" class="dropdown-link">Mision y Vision</a>
                  </li>
                   <li>
                    <a class="dropdown-item" href="objetivoinst.php" class="dropdown-link">  Objetivo Institucional</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="orginst.php" class="dropdown-link">  Organigrama Institucional</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="ubigeo.php" class="dropdown-link">  Ubicacion Geografica</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="juntacionista.php" class="dropdown-link">Junta de Accionistas</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="directorio.php" class="dropdown-link">Directorio</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="planagerencial.php" class="dropdown-link">Plana Gerencial</a>
                  </li>                  
                </ul>
              </li>

              <li class="nav-item dropdown">
                <a href="mantenimiento.php" class="nav-link dropdown-toggle animated fadeIn animation-delay-7" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-name="page">TRANSPARENCIA
                  <i class="zmdi zmdi-chevron-down"></i>
                </a>
                <ul class="dropdown-menu">
                   <li class="dropdown-submenu">
                    <a href="javascript:void(0)" class="dropdown-item has_children">Plan Anual de Adquisicion</a>
                    <ul class="dropdown-menu dropdown-menu-left">
                      <li>
                        <a class="dropdown-item" href="pac2015.php">PAC 2015</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="pac2016.php">PAC 2016</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="pac2017.php">PAC 2017</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="pac2018.php">PAC 2018</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="pac2019.php">PAC 2019</a>
                      </li>
                    </ul>
                  </li>
                   <li class="dropdown-submenu">
                    <a href="javascript:void(0)" class="dropdown-item has_children">Presupuesto Anual (PIA)</a>
                    <ul class="dropdown-menu dropdown-menu-left">
                      <li>
                        <a class="dropdown-item" href="pia2015.php">PIA 2015</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="pia2016.php">PIA 2016</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="pia2017.php">PIA 2017</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="pia2018.php">PIA 2018</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="pia2019.php">PIA 2019</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="ejecucionpresupuestal.php">Ejecucion Presupuestal</a>
                      </li>

                    </ul>
                  </li>
                  <li>
                    <a class="dropdown-item" href="resoluciongerencia.php" class="dropdown-link">Resolucion de Gerencia</a>
                  </li>
                   <li>
                    <a class="dropdown-item" href="directivasemitidas.php" class="dropdown-link">Directivas Emitidas</a>
                  </li>
                   <li>
                    <a class="dropdown-item" href="metasgestion.php" class="dropdown-link">Metas de Gestion</a>
                  </li>
                   <li>
                    <a class="dropdown-item" href="audienciapublica.php" class="dropdown-link">Audiencia Publica</a>
                  </li>
                   <li>
                    <a class="dropdown-item" href="planopeinst.php" class="dropdown-link">Plan Operativo Institucional</a>
                  </li>
                   <li>
                    <a class="dropdown-item" href="planestinst.php" class="dropdown-link">Plan Estrategico Institucional</a>
                  </li>
                   <li>
                    <a class="dropdown-item" href="planorg.php" class="dropdown-link">Planeamiento y Organizacion</a>
                  </li>
                </ul>
              </li> 
               <li class="nav-item dropdown">
                <a href="mantenimiento.php" class="nav-link dropdown-toggle animated fadeIn animation-delay-7" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-name="page">GOBERNANZA GOBERNABILIDAD
                  <i class="zmdi zmdi-chevron-down"></i>
                </a>
                <ul class="dropdown-menu">
                  <li class="dropdown-submenu">
                    <a href="javascript:void(0)" class="dropdown-item has_children">Rendicion de cuentas</a>
                    <ul class="dropdown-menu dropdown-menu-left">
                      <li>
                        <a class="dropdown-item" href="resolucioncuentas.php">Resolucion de Rendicion de Cuentas</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="manualrendcuentas.php">Manual de Rendicion de Cuentas</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="rendcuentas2015.php">Rendicion de Cuentas 2015</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="rendcuentas2016.php">Rendicion de Cuentas 2016</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="rendcuentas2019.php">Rendicion de Cuentas 2019</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="formrendicioncuentas.php">Formulario de Rendicion de Cuentas</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="planoptimizado.php">Plan Maestro Optimizado Periodo 2009-2038</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="calendariorendicion.php">Calendario de Rendicion de Cuentas 2016</a>
                      </li>

                    </ul>
                  </li>
                  <li class="dropdown-submenu">
                    <a href="javascript:void(0)" class="dropdown-item has_children">Resolucion de Tarifas Colaterales</a>
                    <ul class="dropdown-menu dropdown-menu-left">
                      <li>
                        <a class="dropdown-item" href="colaterales.php">Colaterales</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="tarifa2015.php">Tarifa 2015</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="oficioincretar.php">Oficion de Incremento de Tarifa</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="restarifascolaterales.php">Resolucion de Tarifas Colaterales</a>
                      </li>
                    </ul>
                  </li>
                  <li class="dropdown-submenu">
                    <a href="javascript:void(0)" class="dropdown-item has_children">Documentos de Gestion</a>
                    <ul class="dropdown-menu dropdown-menu-left">
                      <li>
                        <a class="dropdown-item" href="cap.php">CAP</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="rof.php">MOF</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="mof.php">ROF</a>
                      </li>

                    </ul>
                  </li>
                  <li>
                    <a class="dropdown-item" href="estatuto.php" class="dropdown-link">Estatuto</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="contexploacion.php" class="dropdown-link">Contrato de Explotacion</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="paopeanual.php" class="dropdown-link">Plan Operativo Anual</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="memoriaanual.php" class="dropdown-link">Memoria Anual</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="resmodicion.php" class="dropdown-link">Resolucion de modificaciones</a>
                  </li>                
                </ul>
              </li>
              <li class="nav-item dropdown">
                <a href="mantenimiento.php" class="nav-link dropdown-toggle animated fadeIn animation-delay-7" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-name="page">ORIENTACION AL CLIENTE
                  <i class="zmdi zmdi-chevron-down"></i>
                </a>
                <ul class="dropdown-menu">
                  <li>
                    <a class="dropdown-item" href="valadmisibles.php" class="dropdown-link">Valores Maximos Admisibles</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="horarioatencion.php" class="dropdown-link">Horario de atencion</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="centrocobranza.php" class="dropdown-link">Centro de Cobranza</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="cronograma.php" class="dropdown-link">Cronogramas de Pagos</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="tarifarios.php" class="dropdown-link">Tarifarios</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="colaterales.php" class="dropdown-link">Colaterales</a>
                  </li>
                  
                </ul>
              </li>
              <li class="nav-item dropdown">
                <a href="mantenimiento.php" class="nav-link dropdown-toggle animated fadeIn animation-delay-7" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-name="page">CONTACTENOS
                  <i class="zmdi zmdi-chevron-down"></i>
                </a>
                <ul class="dropdown-menu">
                  <li>
                    <a class="dropdown-item" href="buzonsugerencias.php" class="dropdown-link">BUZON DE SUGERENCIAS</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="contactenos.php" class="dropdown-link">CONTACTENOS</a>
                  </li>
                </ul>
              </li>    

            </ul>
          </div>
          <a href="javascript:void(0)" class="ms-toggle-left btn-navbar-menu">
            <i class="zmdi zmdi-menu"></i>
          </a>
        </div>
        <!-- container -->
      </nav>
      <div class="ms-hero-page-override ms-hero-img-city ms-hero-bg-primary no-pb overflow-hidden ms-bg-fixed">
        <div class="container">
             <div class="row color-dark">
              <div class="col-xl-4 col-lg-6 col-sm-6">
                    
                    <a href="javascript:void(0)" class="btn btn-warning btn-raised"  data-toggle="modal" data-target="#myModal1">CONSULTE SU RECIBO</a>
                    <a href="javascript:void(0)" class="btn btn-info btn-raised"  data-toggle="modal" data-target="#myModal2">OBSERVE EL CRONOGRAMA DE PAGOS</a>
                <a href="javascript:void(0)" class="btn btn-danger btn-raised"  data-toggle="modal" data-target="#myModal4">ENVIENOS SU SUGERENCIA O RECLAMO</a>
                
                 
              </div>
              <div class="col-xl-4 col-lg-6 col-sm-8">
                <div id="carousel-example-generic5" class="ms-carousel ms-carousel-thumb carousel slide" data-ride="carousel">
                    <div class="card card-relative">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                                <img class="d-block img-fluid" src="assets/img/material/primero.jpg" width="100%" height="40%">
                                
                            </div>
                            <div class="carousel-item">
                                <img class="d-block img-fluid" src="assets/img/material/noveno.jpg" width="100%" height="40%">
                                
                            </div>
                            <div class="carousel-item">
                                <img class="d-block img-fluid" src="assets/img/material/tercero.jpg" width="100%" height="40%">
                            </div>
                        </div>
                            </div>
                    
                </div>
              </div>
            
            <div class="col-xl-4 col-lg-6 col-sm-6">
                <div id="carousel-example-generic5" class="ms-carousel ms-carousel-thumb carousel slide" data-ride="carousel">
                    <div class="card card-relative">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                                <div class="card">
                                    <div class="js-player" data-plyr-provider="youtube" data-plyr-embed-id="UwNoLolzA9U" ></div>
                                </div>
                            </div>
                              <?php
                                $sel = $con-> query("SELECT * FROM slider WHERE estado ='1' "); 
                                 ?>
                                 <?php while($f = $sel->fetch_assoc()){  
                                     ?>
                            <div class="carousel-item">
                                <div class="card">
                                <div class="js-player" data-plyr-provider="youtube"
                                data-plyr-embed-id="<?php echo $f['nomyou'] ?>"></div>
                            </div>
                          
                            </div>
                            <?php } ?>
                        </div>
                            </div>
                </div>
              </div>
              </div>

        </div>
        <div class="container">
             <div class="row color-dark">
              <div class="col-xl-3 col-lg-6 col-sm-6">
                <a href="#" class="img-thumbnail withripple">
                    <div class="thumbnail-container">
                        <img src="assets/img/material/primero.jpg"  class="img-fluid">
                    </div>
                </a>
              </div>
              <div class="col-xl-3 col-lg-6 col-sm-6">
                 <a href="#" class="img-thumbnail withripple">
                    <div class="thumbnail-container">
                        <img src="assets/img/material/tercero.jpg"  class="img-fluid">
                    </div>
                </a>
              </div>
              <div class="col-xl-3 col-lg-6 col-sm-6">
                 <a href="#" class="img-thumbnail withripple">
                    <div class="thumbnail-container">
                        <img src="assets/img/material/cuarto.jpg"  class="img-fluid">
                    </div>
                </a>
              </div>
              <div class="col-xl-3 col-lg-6 col-sm-6">
                 <a href="#" class="img-thumbnail withripple">
                    <div class="thumbnail-container">
                        <img src="assets/img/material/quinto.jpg"  class="img-fluid">
                    </div>
                </a>
              </div>
              
              </div>

        </div>
        <div class="container">
             <div class="row color-dark">
              <div class="col-xl-3 col-lg-6 col-sm-6">
                <a href="#" class="img-thumbnail withripple">
                    <div class="thumbnail-container">
                        <img src="assets/img/material/sexto.jpg"  class="img-fluid">
                    </div>
                </a>
              </div>
              <div class="col-xl-3 col-lg-6 col-sm-6">
                 <a href="#" class="img-thumbnail withripple">
                    <div class="thumbnail-container">
                        <img src="assets/img/material/septimo.jpg"  class="img-fluid">
                    </div>
                </a>
              </div>
              <div class="col-xl-3 col-lg-6 col-sm-6">
                 <a href="#" class="img-thumbnail withripple">
                    <div class="thumbnail-container">
                        <img src="assets/img/material/decimo.jpg"  class="img-fluid">
                    </div>
                </a>
              </div>
              <div class="col-xl-3 col-lg-6 col-sm-6">
                 <a href="#" class="img-thumbnail withripple">
                    <div class="thumbnail-container">
                        <img src="assets/img/material/noveno.jpg"  class="img-fluid">
                    </div>
                </a>
              </div>
              
              </div>

        </div>

    </div>

      


      <section class="mt-6">
        <div class="container">
          <h1 class="color-primary text-center" text-center>CONSULTA EN LINEA</h1>
          <div class="row d-flex justify-content-center">
            <div class="col-lg-4 col-md-6">
              <div class="card mt-4 card-danger wow zoomInUp">
                <div class="ms-hero-bg-danger ms-hero-img-city">
                  <img src="assets/img/checca/deseconomico.jpg" alt="..." class="img-avatar-circle"> </div>
                <div class="card-body pt-6 text-center">
                  <a href="#"><h3 class="color-danger">TUS PAGOS</h3></a>
                  <p>Ahora podras consultar tus pagos.</p>
                  
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6">
              <div class="card mt-4 card-info wow zoomInUp">
                <div class="ms-hero-bg-info ms-hero-img-city">
                  <img src="assets/img/checca/desocial.jpg" alt="..." class="img-avatar-circle"> </div>
                <div class="card-body pt-6 text-center">
                  <a href="#"><h3 class="color-info">GENERE SU PRESUPUESTO</h3></a>
                  <p>Consulte su presupuesto aproximado de su futura instalacion.</p>
                  
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6">
              <div class="card mt-4 card-warning wow zoomInUp">
                <div class="ms-hero-bg-warning ms-hero-img-city">
                  <img src="assets/img/checca/obras.png" alt="..." class="img-avatar-circle"> </div>
                <div class="card-body pt-6 text-center">
                  <a href="#"><h3 class="color-warning">MI CORREO.</h3></a>
                  <p>Ahora puedes recibir tu recibo a tu correo.</p>
                 
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
     

      <aside class="ms-footbar">
        <div class="container">
          <div class="row">
            <div class="col-lg-4 ms-footer-col">
              <div class="ms-footbar-block">
                <h3 class="ms-footbar-title">Nuestro Sitios</h3>
                <ul class="list-unstyled ms-icon-list three_cols">
                  <li>
                    <a href="https://www.facebook.com/empssapal.sicuani.7"  target="_blank" >
                      <i class="zmdi zmdi-home"></i> Facebook</a>
                  </li>
                  <li>
                    <a href="https://www.youtube.com/channel/UCdg82YQefxbGK43Lu4fyZgg?view_as=subscriber"  target="_blank" >
                      <i class="zmdi zmdi-edit"></i> Youtube</a>
                  </li>
                  
                  
                </ul>
              </div>
              <div class="ms-footbar-block">
                <h3 class="ms-footbar-title">Envie su Sugerencia</h3>
                <p class="">Su sugerencia sera evaluada por el area de Imagen Institucional de EMPSSAPAL.</p>
                <form>
                  <div class="form-group label-floating mt-2 mb-1">
                    <div class="input-group ms-input-subscribe">
                      <label class="control-label" for="ms-subscribe">
                        <i class="zmdi zmdi-email"></i> Correo Electronico</label>
                      <input type="email" id="ms-subscribe" class="form-control"> </div>
                  </div>
                  <button class="ms-subscribre-btn" type="button">ENVIAR</button>
                </form>
              </div>
            </div>
            <div class="col-lg-5 col-md-7 ms-footer-col ms-footer-alt-color">
              <div class="ms-footbar-block">
                <h3 class="ms-footbar-title text-center mb-2">NUESTRAS ACTIVIDADES</h3>
                <div class="ms-footer-media">
                  <div class="media">
                    <div class="media-left media-middle">
                      <a href="javascript:void(0)">
                        <img class="media-object media-object-circle" src="assets/img/demo/p75.jpg" alt="..."> </a>
                    </div>
                    <div class="media-body">
                      <h4 class="media-heading">
                        <a href="javascript:void(0)">BICICLETEADA</a>
                      </h4>
                      <div class="media-footer">
                        <span>
                          <i class="zmdi zmdi-time color-info-light"></i> 01/03/2019</span>
                        <span>
                          <i class="zmdi zmdi-folder-outline color-warning-light"></i>
                          <a href="javascript:void(0)"></a>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="media">
                    
                    
                  </div>
                  
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-5 ms-footer-col ms-footer-text-right">
              <div class="ms-footbar-block">
                <div class="ms-footbar-title">
                  <h3 class="no-m ms-site-title">EMPSSAPAL 
                    <span></span>
                  </h3>
                </div>
                <address class="no-mb">
                  
                    <i class="color-warning-light zmdi zmdi-map mr-1"></i> AV: CONFEDERACION</p>
                  <p>
                    <i class="color-info-light zmdi zmdi-email mr-1"></i>
                    <a href="mailto:joe@example.com">eps.empssapal.pe@gmail.com</a>
                  </p>
                  <p>
                    <i class="color-royal-light zmdi zmdi-phone mr-1"></i>+00000 </p>
                  <p>
                    <i class="color-success-light fa fa-fax mr-1"></i>+00000 </p>
                </address>
              </div>
              <div class="ms-footbar-block">
                <h3 class="ms-footbar-title">Nuestros Medios Sociales</h3>
                <div class="ms-footbar-social">
                  <a href="https://www.facebook.com/empssapal.sicuani.7"  target="_blank"  class="btn-circle btn-facebook">
                    <i class="zmdi zmdi-facebook"></i>
                  </a>
                  <a href="https://www.youtube.com/channel/UCdg82YQefxbGK43Lu4fyZgg?view_as=subscriber"  target="_blank" class="btn-circle btn-youtube">
                    <i class="zmdi zmdi-youtube-play"></i>
                  </a> 
                </div>
              </div>
            </div>
          </div>
        </div>
      </aside>
      <footer class="ms-footer">
        <div class="container">
          <p>Copyright &copy; Area de Informatica</p>
        </div>
      </footer>
      <div class="btn-back-top">
        <a href="mantenimiento.php" data-scroll id="back-top" class="btn-circle btn-circle-primary btn-circle-sm btn-circle-raised ">
          <i class="zmdi zmdi-long-arrow-up"></i>
        </a>
      </div>
    </div>
    <!-- ms-site-container -->
<!-- MODAL DE AVISOS RECIBOS -->
<div class="modal" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog animated zoomIn animated-10x" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title color-primary" id="myModalLabel">CONSULTA VIA INTERNET</h1>
            </div>
            
            <div class="modal-body">
                <h3>INGRESE SU COD USUARIO</h3>
                <div class="form-group has-success">
                    <label class="control-label" for="inputSuccess">CODIGO DE RECIBO O USUARIO</label>
                    <input type="text" class="form-control" id="codigo" size="25" name="codigo"  placeholder="INGRESE SU CODIGO "  name="pwd" onblur="may(this.value,this.id)"  ></center>
                </div>
                <div class="validacion"></div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                
            </div>
        </div>
    </div>
</div>


<!-- MODAL DE AVISOS CRONOGRAMA DE PAGOS -->
<div class="modal" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog animated zoomIn animated-10x" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title color-primary" id="myModalLabel">CRONOGRAMA DE PAGOS DE JULIO</h1>
            </div>
            <div class="modal-body">
                <h3><img src="img/mesjulio.jpg" width="100%" height="50%"></h3>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                
            </div>
        </div>
    </div>
</div>
<!-- MODAL DE SUGERENCIAS -->
<div class="modal" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog animated zoomIn animated-10x" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title color-primary" id="myModalLabel">ENVIENOS SU SUGERENCIA O RECLAMO</h4>
                <h6 class="modal-title color-primary" id="myModalLabel">En breve nos pondremos en contacto con usted.</h6>
            </div>
            <div class="modal-body">
                <h3>
                  <form action="sugerencia/ins_sug.php" method="POST" >
                    
                          <div class="form-group row">
                              <div class="col-lg-6">
                                  <input type="number" class="form-control" id="dni" name="dni" placeholder="DNI">
                              </div>
                              <div class="col-lg-12">
                                  <input type="text" class="form-control" id="nom_pe" name="nom_pe" placeholder="Nombres y apellidos">
                              </div>
                          </div>
                          
                          <div class="form-group row">
                              <div class="col-lg-6">
                                  <input type="email" class="form-control" id="correo" name="correo" placeholder="Correo">
                              </div>
                              <div class="col-lg-6">
                                  <input type="number" class="form-control" id="movil" name="movil" placeholder="Número Celular">
                              </div>
                          </div>
                          
                          <div class="form-group row justify-content-end">
                              <label for="textArea" class="col-lg-2 control-label">Sugerencia o Reclamo</label>

                              <div class="col-lg-10">
                                  <textarea class="form-control" rows="3" id="sugerencia" name="sugerencia"></textarea>
                                  <span class="help-block">Describanos su sugerencia</span>
                              </div>
                          </div>
                          <div class="form-group row justify-content-end">
                              <div class="col-lg-10">
                                  <button type="submit" class="btn btn-raised btn-primary">Enviar</button>
                                  <button type="button" class="btn btn-danger">Cancelar</button>
                              </div>
                          </div>
                  </form>

                </h3>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                
            </div>
        </div>
    </div>
</div>
<!-- MODAL DE AVISOS CHAT -->
<div class="modal" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog animated zoomIn animated-10x" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title color-primary" id="myModalLabel">ESPERE...</h1>
            </div>
            <div class="modal-body">
              <div class="panel panel-dark panel-default">
                  <h3>En breves momentos responderemos sus consultas.</h3>
                </div>
                <div class="form-group row justify-content-end">
                    <label for="textArea" class="col-lg-2 control-label">ESCRIBA SU CONSULTA</label>

                    <div class="col-lg-10">
                        <textarea class="form-control" rows="3" id="textArea"></textarea>
                        <span class="help-block">....</span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                
            </div>
        </div>
    </div>
</div>


<!-- AVISO  -->
<div class="modal" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3">
    <div class="modal-dialog modal-sm animated zoomIn animated-3x" role="document">
        ...
    </div>
</div>

    <div class="ms-slidebar sb-slidebar sb-left sb-style-overlay" id="ms-slidebar">
      <div class="sb-slidebar-container">
        <header class="ms-slidebar-header">
          <div class="ms-slidebar-login">
            
            <a href="javascript:void(0)" class="withripple" data-toggle="modal" data-target="#ms-account-modal">
              <i class="zmdi zmdi-account"></i> ACCEDER</a> 
          </div>
        </header>
        <ul class="ms-slidebar-menu" id="slidebar-menu" role="tablist" aria-multiselectable="true">
          <li class="card" role="tab" id="sch1">
            <a class="collapsed" role="button" data-toggle="collapse" href="#sc1" aria-expanded="false" aria-controls="sc1">
              <i class="zmdi zmdi-home"></i> QUIENES SOMOS </a>
            <ul id="sc1" class="card-collapse collapse" role="tabpanel" aria-labelledby="sch1" data-parent="#slidebar-menu">
              <li>
                <a href="historia.php">Historia</a>
              </li>
              <li>
                <a href="misionvision.php">Mision y Vision</a>
              </li>
              <li>
                <a href="objetivoinst.php">Objetivo Institucional</a>
              </li>
              <li>
                <a href="orginst.php">Organigrama Institucional</a>
              </li>
              <li>
                <a href="ubigeo.php">Ubicación Geografica</a>
              </li>
              <li>
                <a href="juntacionista.php">Junta de Accionista</a>
              </li>
              <li>
                <a href="directorio.php">Directorio</a>
              </li>
              <li>
                <a href="planagerencial.php">Plana Gerencial </a>
              </li>
            </ul>
          </li>
          <li class="card" role="tab" id="sch2">
            <a class="collapsed" role="button" data-toggle="collapse" href="#sc2" aria-expanded="false" aria-controls="sc2">
              <i class="zmdi zmdi-desktop-mac"></i> TRANSPARENCIA </a>
            <ul id="sc2" class="card-collapse collapse" role="tabpanel" aria-labelledby="sch2" data-parent="#slidebar-menu">
              
              <li>
                <a href="#">Presupuesto Anual</a>
              </li>
              <li>
                <a href="#">Plan Anual de Adquisicion</a>
              </li>
              <li>
                <a href="resoluciongerencia.php">Resolucion de Gerencia</a>
              </li>
              <li>
                <a href="directivasemitidas.php">Directivas Emitidas</a>
              </li>
              <li>
                <a href="metasgestion.php">Metas de Gestion</a>
              </li>
               <li>
                <a href="audienciapublica.php">Audiencia Publica</a>
              </li>
               <li>
                <a href="planopeinst.php">Plan Operativo Institucional</a>
              </li>
               <li>
                <a href="planestinst.php">Plan Estrategico Institucional</a>
              </li>
               <li>
                <a href="planorg.php">Planeamiento y Organizacion</a>
              </li>
               
              
            </ul>
          </li>
          <li class="card" role="tab" id="sch4">
            <a class="collapsed" role="button" data-toggle="collapse" href="#sc4" aria-expanded="false" aria-controls="sc4">
              <i class="zmdi zmdi-edit"></i> GOBERNABILIDAD </a>
            <ul id="sc4" class="card-collapse collapse" role="tabpanel" aria-labelledby="sch4" data-parent="#slidebar-menu">
               <li>
                <a href="#">Rendicion de Cuentas</a>
              </li>
               <li>
                <a href="#">Documentos de Gestion</a>
              </li>
               <li>
                <a href="estatuto.php">Estatuto</a>
              </li>
               <li>
                <a href="contexploacion.php">Contrato de Exploacion</a>
              </li>

               <li>
                <a href="paopeanual.php">Plan Operativo Anual</a>
              </li>

               <li>
                <a href="memoriaanual.php">Memoria Anual</a>
              </li>

               <li>
                <a href="resmodicion.php">Resolucion de Modificaciones</a>
              </li>
            </ul>
          </li>
          
          <li class="card" role="tab" id="sch6">
            <a class="collapsed" role="button" data-toggle="collapse" href="#sc6" aria-expanded="false" aria-controls="sc6">
              <i class="zmdi zmdi-collection-image-o"></i> ORIENTACION CLIENTE
            <ul id="sc6" class="card-collapse collapse" role="tabpanel" aria-labelledby="sch6" data-parent="#slidebar-menu">
               <li>
                <a href="valadmisibles.php">Valores Maximos Admisibles</a>
              </li>
               <li>
                <a href="horarioatencion.php">Horario de Atencion</a>
              </li>
               <li>
                <a href="centrocobranza.php">Centros de Cobranza</a>
              </li>
               <li>
                <a href="cronograma.php">Cronograma de Pagos</a>
              </li>
               <li>
                <a href="tarifarios.php">Tarifas</a>
              </li>

               <li>
                <a href="colaterales.php">Colaterales</a>
              </li>
             
            </ul>
          </li>
           <li class="card" role="tab" id="sch6">
            <a class="collapsed" role="button" data-toggle="collapse" href="#sc6" aria-expanded="false" aria-controls="sc6">
              <i class="zmdi zmdi-collection-image-o"></i> CONTACTOS
            <ul id="sc6" class="card-collapse collapse" role="tabpanel" aria-labelledby="sch6" data-parent="#slidebar-menu">
               <li>
                <a href="buzonsugerencias.php">Buzon de Sugerencias</a>
              </li>
               <li>
                <a href="contactenos.php">Contactenos</a>
              </li>
             
            </ul>
          </li>
          
        </ul>
        <div class="ms-slidebar-social ms-slidebar-block">
          <h4 class="ms-slidebar-block-title">NUESTRAS REDES SOCIALES</h4>
          <div class="ms-slidebar-social">
            <a href="https://www.facebook.com/empssapal.sicuani.7"   target="_blank"  class="btn-circle btn-circle-raised btn-facebook">
              <i class="zmdi zmdi-facebook"></i>
              <div class="ripple-container"></div>
            </a>
            <a href="https://www.youtube.com/channel/UCdg82YQefxbGK43Lu4fyZgg?view_as=subscriber"  target="_blank" class="btn-circle btn-circle-raised btn-google">
              <i class="zmdi zmdi-google"></i>
              <div class="ripple-container"></div>
            </a>
            
          </div>
        </div>
      </div>
    </div>



<!-- Modal -->
<div class="modal modal-danger" id="myModa111" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title color-primary" id="myModalLabel"><?php echo $valor3; ?></h1>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
            </div>
            <div class="modal-body">
                <h3><?php echo $valor4 ?></h3>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">CERRAR</button>
                <button type="button" class="btn  btn-primary"><?php echo $valor5; ?></button>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal" id="myModa222" tabindex="-1" role="dialog" aria-labelledby="myModalLabel11">
    <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
        <div class="modal-header red">
                <h1 class="modal-title color-primary" id="myModalLabel"><?php echo $valor2; ?></h1>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
            </div>
        <div class="modal-content">
            <img src="<?php echo $valor1 ?>" width="100%" height="100%">
            
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal" id="myModa333" tabindex="-1" role="dialog" aria-labelledby="myModalLabel11">
    <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
        <div class="modal-content">
            <div data-plyr-provider="youtube" data-plyr-embed-id="<?php echo $valor6 ?>" class="js-player" width="1000px" hidden="500px"></div>
        </div>
    </div>
</div>
<!-- Modal -->

<div class="modal modal-danger" id="myModa444" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title color-primary" id="myModalLabel"><?php echo $valor3; ?></h1>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
            </div>
            <div class="modal-body">
                <h3><?php echo $valor4 ?></h3>
            </div>
             <div class="modal-content">
                  <img src="<?php echo $valor1 ?>" width="100%" height="100%">
                  
              </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">CERRAR</button>
            </div>
        </div>
    </div>
</div>
<script>
function texto() {
   $(document).ready(function() {
    $('#myModa111').modal('show');
});
  
  
<?php  ?>
}
function imagen() {
  $(document).ready(function() {
    $('#myModa222').modal('show');
});
}
function textoimagen() {
  $(document).ready(function() {
    $('#myModa444').modal('show');
});
}
function video() {
  $(document).ready(function() {
    $('#myModa333').modal('show');
});
}
</script>

    <script src="assets/js/plugins.min.js"></script>
    <script src="assets/js/app.min.js"></script>
    <script>
  function may(obj,id){
    obj = obj.toUpperCase();
    document.getElementById(id).value = obj;
  }
  $('#codigo').change( function(){
    $.post('consulta/ajax.php',{
      codigo:$('#codigo').val(),

        beforeSend: function(){
            $('.validacion').html("Espere un momento por favor..");
        }
    
    }, function(respuesta){
        $('.validacion').html(respuesta);
      
    });
  }); 
</script>
  </body>
</html>