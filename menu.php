       <div id="ms-preload" class="ms-preload">
      <div id="status">
        <div class="spinner">
          <div class="dot1"></div>
          <div class="dot2"></div>
        </div>
      </div>
    </div>
    <div class="ms-site-container">
      <!-- Modal -->
      <header class="ms-header ms-header-white">
        <!--ms-header-white-->
        <div class="container">
          <div class="ms-title">
              <img class="animated fadeInLeft animation-delay-15  ms-hero-bg-danger animate__infinite animate__heartBeat infinite" src="img/empssapalgota.png" width="10%" height="10%">
              <h1 class="animated fadeInRight animation-delay-4 animate__infinite animate__rubberBand infinite" >  <center> <img src="img/empssapal.jpg" width="50%" height="50%" alt=""></center>
              </h1>
          <div class="header-right">
            <a href="http://35.198.55.197/e-siincoweb/empssapal/reclamos_web/" class="btn btn-warning btn-raised animate__infinite animate__shakeY infinite" >RECLAMOS</a>
            <a href="http://35.198.55.197/e-siincoweb/empssapal/consultas/" class="btn btn-danger btn-raised animate__infinite animate__shakeY infinite" >RECIBOS DIGITAL </a>
            </br>
            
            <a href="https://www.youtube.com/channel/UCdg82YQefxbGK43Lu4fyZgg?view_as=subscriber"  target="_blank" class="btn-circle btn-circle-danger  animated fadeInLeft animation-delay-15">
                <i class="zmdi zmdi-youtube-play"></i>
            </a>
            <a href="https://www.facebook.com/empssapal.sicuani.7" target="_blank" class="btn-circle btn-circle-primary animated  animation-delay-15 ">
                <i class="zmdi zmdi-facebook-box"></i>
            </a> <a href="https://webmail1.hostinger.es"  class="btn-circle btn-circle-danger animated  animation-delay-15 ">
                <i class="zmdi zmdi-email-open"></i>
            </a>

            <a href="javascript:void(0)" class="btn-circle btn-circle-primary no-focus animated zoomInDown animation-delay-15" data-toggle="modal" data-target="#ms-account-modal">
              <i class="zmdi zmdi-account"></i>
            </a>

            <a href="javascript:void(0)" class="btn-ms-menu btn-circle btn-circle-primary ms-toggle-left animated zoomInDown animation-delay-10">
              <i class="zmdi zmdi-menu"></i>
            </a>
            
            </div>

        </div>
        </div>
      </header>


 <div class="modal modal-primary" id="ms-account-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog animated zoomIn animated-3x" role="document">
          <div class="modal-content">
            <div class="modal-header d-block shadow-2dp no-pb">
              <button type="button" class="close d-inline pull-right mt-2" data-dismiss="modal" aria-label="Close">

              </button>
              <div class="modal-title text-center">
                <img src="assets/img/empssapal/empssapal.jpg" width="30%" height="30%">

              </div>
              <div class="modal-header-tabs">
                <ul class="nav nav-tabs nav-tabs-full nav-tabs-3 nav-tabs-primary" role="tablist">
                  <li class="nav-item" role="presentation">
                    <a href="#ms-login-tab" aria-controls="ms-login-tab" role="tab" data-toggle="tab" class="nav-link active withoutripple">
                      <i class="zmdi zmdi-account"></i> ACCESO</a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="modal-body">
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade active show" id="ms-login-tab">
                  <form action="login/index.php" method="POST" autocomplete="off">
                    <fieldset>
                      <div class="form-group label-floating">
                        <div class="input-group">
                          <span class="input-group-addon">
                            <i class="zmdi zmdi-account"></i>
                          </span>
                          <label class="control-label" for="ms-form-user">Usuario</label>
                          <input type="text" name="usuario" id="usuario" class="form-control"> </div>
                      </div>
                      <div class="form-group label-floating">
                        <div class="input-group">
                          <span class="input-group-addon">
                            <i class="zmdi zmdi-lock"></i>
                          </span>
                          <label class="control-label" for="ms-form-pass">Contraseña</label>
                          <input type="password"  name="contra" id="contra"  class="form-control"> </div>
                      </div>
                      <div class="row mt-2">
                        <div class="col-md-6">
                          <div class="form-group no-mt">
                            <div class="checkbox">
                              <label>
                                <input type="checkbox"> Recordar Contraseña </label>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <button class="btn btn-raised btn-primary pull-right">INGRESAR</button>
                        </div>
                      </div>
                    </fieldset>
                  </form>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>


      <nav class="navbar navbar-expand-md  navbar-static ms-navbar ms-navbar-primary">
        <div class="container container-full">
          <div class="navbar-header">
             <img src="assets/img/empssapal/empssapal.jpg" width="10%" height="80%">
          <center><h4> ---       EMPSSAPAL       ---</h4></center>
          </div>

          <div class="collapse navbar-collapse" id="ms-navbar">
            <ul class="navbar-nav">

              <li class="nav-item dropdown">
                <a href="mantenimiento.php" class="nav-link dropdown-toggle animated fadeIn animation-delay-7" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-name="page">QUIENES SOMOS
                  <i class="zmdi zmdi-chevron-down"></i>
                </a>
                <ul class="dropdown-menu">

                  <li>
                    <a class="dropdown-item" href="historia.php" class="dropdown-link">Historia</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="misionvision.php" class="dropdown-link">Mision y Vision</a>
                  </li>
                   <li>
                    <a class="dropdown-item" href="objetivoinst.php" class="dropdown-link">  Objetivo Institucional</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="orginst.php" class="dropdown-link">  Organigrama Institucional</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="ubigeo.php" class="dropdown-link">  Ubicacion Geografica</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="juntacionista.php" class="dropdown-link">Junta de Accionistas</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="directorio.php" class="dropdown-link">Directorio</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="planagerencial.php" class="dropdown-link">Plana Gerencial</a>
                  </li>
                </ul>
              </li>

              <li class="nav-item dropdown">
                <a href="mantenimiento.php" class="nav-link dropdown-toggle animated fadeIn animation-delay-7" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-name="page">TRANSPARENCIA
                  <i class="zmdi zmdi-chevron-down"></i>
                </a>
                <ul class="dropdown-menu">

                  <li class="dropdown-submenu">
                    <a href="javascript:void(0)" class="dropdown-item has_children">Control Interno</a>
                    <ul class="dropdown-menu dropdown-menu-left">
                      <li>
                        <a class="dropdown-item" href="impcontrol.php">Implementacion Control Interno</a>
                      </li>
                    </ul>
                  </li>
                  <li class="dropdown-submenu">
                    <a href="javascript:void(0)" class="dropdown-item has_children">Informe de Gestion</a>
                    <ul class="dropdown-menu dropdown-menu-left">
                      <li>
                        <a class="dropdown-item" href="infgestion.php">resumen ejecutivo 2019</a>
                      </li>
                    </ul>
                  </li>
                  <li class="dropdown-submenu">
                    <a href="javascript:void(0)" class="dropdown-item has_children">Ejecucion Presupuestal</a>
                    <ul class="dropdown-menu dropdown-menu-left">
                      <li>
                        <a class="dropdown-item" href="ejepresu2019.php">2019</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="ejepresu2020.php">2020</a>
                      </li>
                    </ul>
                  </li>
                   <li class="dropdown-submenu">
                    <a href="javascript:void(0)" class="dropdown-item has_children">Plan Anual de Adquisicion</a>
                    <ul class="dropdown-menu dropdown-menu-left">
                      <li>
                        <a class="dropdown-item" href="pac2015.php">PAC 2015</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="pac2016.php">PAC 2016</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="pac2017.php">PAC 2017</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="pac2018.php">PAC 2018</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="pac2019.php">PAC 2019</a>
                      </li>
                    </ul>
                  </li>
                   <li class="dropdown-submenu">
                    <a href="javascript:void(0)" class="dropdown-item has_children">Presupuesto Anual (PIA)</a>
                    <ul class="dropdown-menu dropdown-menu-left">
                      <li>
                        <a class="dropdown-item" href="pia2015.php">PIA 2015</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="pia2016.php">PIA 2016</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="pia2017.php">PIA 2017</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="pia2018.php">PIA 2018</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="pia2019.php">PIA 2019</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="ejecucionpresupuestal.php">Ejecucion Presupuestal</a>
                      </li>

                    </ul>
                  </li>
                  <li>
                    <a class="dropdown-item" href="resoluciongerencia.php" class="dropdown-link">Resolucion de Gerencia</a>
                  </li>
                   <li>
                    <a class="dropdown-item" href="directivasemitidas.php" class="dropdown-link">Directivas Emitidas</a>
                  </li>
                   <li>
                    <a class="dropdown-item" href="metasgestion.php" class="dropdown-link">Metas de Gestion</a>
                  </li>
                   <li>
                    <a class="dropdown-item" href="audienciapublica.php" class="dropdown-link">Audiencia Publica</a>
                  </li>
                   <li>
                    <a class="dropdown-item" href="planopeinst.php" class="dropdown-link">Plan Operativo Institucional</a>
                  </li>
                   <li>
                    <a class="dropdown-item" href="planestinst.php" class="dropdown-link">Plan Estrategico Institucional</a>
                  </li>
                   <li>
                    <a class="dropdown-item" href="planorg.php" class="dropdown-link">Planeamiento y Organizacion</a>
                  </li>
                </ul>
              </li>
               <li class="nav-item dropdown">
                <a href="mantenimiento.php" class="nav-link dropdown-toggle animated fadeIn animation-delay-7" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-name="page">GOBERNANZA GOBERNABILIDAD
                  <i class="zmdi zmdi-chevron-down"></i>
                </a>
                <ul class="dropdown-menu">
                  <li class="dropdown-submenu">
                    <a href="javascript:void(0)" class="dropdown-item has_children">Rendicion de cuentas</a>
                    <ul class="dropdown-menu dropdown-menu-left">
                      <li>
                        <a class="dropdown-item" href="resolucioncuentas.php">Resolucion de Rendicion de Cuentas</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="manualrendcuentas.php">Manual de Rendicion de Cuentas</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="rendcuentas2015.php">Rendicion de Cuentas 2015</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="rendcuentas2016.php">Rendicion de Cuentas 2016</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="rendcuentas2019.php">Rendicion de Cuentas 2019</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="rendcuentas2020.php">Rendicion de Cuentas 2020</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="formrendicioncuentas.php">Formulario de Rendicion de Cuentas</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="planoptimizado.php">Plan Maestro Optimizado Periodo 2009-2038</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="calendariorendicion.php">Calendario de Rendicion de Cuentas 2016</a>
                      </li>

                    </ul>
                  </li>
                  <li class="dropdown-submenu">
                    <a href="javascript:void(0)" class="dropdown-item has_children">Resolucion de Tarifas Colaterales</a>
                    <ul class="dropdown-menu dropdown-menu-left">
                      <li>
                        <a class="dropdown-item" href="colaterales.php">Colaterales</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="tarifa2015.php">Tarifa 2015</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="oficioincretar.php">Oficion de Incremento de Tarifa</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="restarifascolaterales.php">Resolucion de Tarifas Colaterales</a>
                      </li>
                    </ul>
                  </li>
                  <li class="dropdown-submenu">
                    <a href="javascript:void(0)" class="dropdown-item has_children">Documentos de Gestion</a>
                    <ul class="dropdown-menu dropdown-menu-left">
                      <li>
                        <a class="dropdown-item" href="cap.php">CAP</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="rof.php">MOF</a>
                      </li>
                      <li>
                        <a class="dropdown-item" href="mof.php">ROF</a>
                      </li>

                    </ul>
                  </li>
                  <li>
                    <a class="dropdown-item" href="factorriezgo.php" class="dropdown-link">Factores de Riesgo</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="estruccturasalarial.php" class="dropdown-link">Estructura Salarial</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="estatuto.php" class="dropdown-link">Estatuto</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="codbuengobierno.php" class="dropdown-link">Codigo de buen gobierno corporativo</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="contexploacion.php" class="dropdown-link">Contrato de Explotacion</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="paopeanual.php" class="dropdown-link">Plan Operativo Anual</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="memoriaanual.php" class="dropdown-link">Memoria Anual</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="resmodicion.php" class="dropdown-link">Resolucion de modificaciones</a>
                  </li>
                </ul>
              </li>
              <li class="nav-item dropdown">
                <a href="mantenimiento.php" class="nav-link dropdown-toggle animated fadeIn animation-delay-7" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-name="page">ORIENTACION AL CLIENTE
                  <i class="zmdi zmdi-chevron-down"></i>
                </a>
                <ul class="dropdown-menu">
                  <li>
                    <a class="dropdown-item" href="requisitos.php" class="dropdown-link">Requisitos Para Instalacion De Servicios</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="valadmisibles.php" class="dropdown-link">Valores Maximos Admisibles</a>
                  </li>
                  
                  <li>
                    <a class="dropdown-item" href="horarioatencion.php" class="dropdown-link">Horario de atencion</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="centrocobranza.php" class="dropdown-link">Centro de Cobranza</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="cronograma.php" class="dropdown-link">Cronogramas de Pagos</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="tarifarios.php" class="dropdown-link">Tarifarios</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="colaterales.php" class="dropdown-link">Colaterales</a>
                  </li>

                </ul>
              </li>
              <li class="nav-item dropdown">
                <a href="mantenimiento.php" class="nav-link dropdown-toggle animated fadeIn animation-delay-7" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-name="page">CONTACTENOS
                  <i class="zmdi zmdi-chevron-down"></i>
                </a>
                <ul class="dropdown-menu">
                  <li>
                    <a class="dropdown-item" href="buzonsugerencias.php" class="dropdown-link">BUZON DE SUGERENCIAS</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="contactenos.php" class="dropdown-link">CONTACTENOS</a>
                  </li>
                </ul>
              </li>

            </ul>
          </div>
          <a href="javascript:void(0)" class="ms-toggle-left btn-navbar-menu">
            <i class="zmdi zmdi-menu"></i>
          </a>
        </div>
        <!-- container -->
      </nav>
