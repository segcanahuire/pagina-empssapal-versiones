
      <aside class="ms-footbar">
        <div class="container">
          <div class="row">
            <div class="col-lg-4 ms-footer-col">
              <div class="ms-footbar-block">
                <h3 class="ms-footbar-title">NUESTRAS REDES SOCIALES</h3>
                <ul class="list-unstyled ms-icon-list three_cols">
                  <li>
                    <a href="https://www.facebook.com/empssapal.sicuani.7"  target="_blank" >
                      <i class="zmdi zmdi-home"></i> Facebook</a>
                  </li>
                  <li>
                    <a href="https://www.youtube.com/channel/UCdg82YQefxbGK43Lu4fyZgg?view_as=subscriber"  target="_blank" >
                      <i class="zmdi zmdi-edit"></i> Youtube</a>
                  </li>


                </ul>
              </div>
              <div class="ms-footbar-block">
                <h3 class="ms-footbar-title">ENVIENOS SU SUGERENCIA Y O RECLAMO.</h3>
                <p class="">Su sugerencia o reclamo es importante para nosotros para mejorar nuestros servicios con la poblacion.</p>

                  <a href="javascript:void(0)" class="btn btn-danger btn-raised"  data-toggle="modal" data-target="#myModal4">ENVIENOS SU SUGERENCIA O RECLAMO</a>

              </div>
            </div>
            <div class="col-lg-5 col-md-7 ms-footer-col ms-footer-alt-color">
              <div class="ms-footbar-block">
                <h3 class="ms-footbar-title text-center mb-2">NUESTRAS ACTIVIDADES</h3>
                <a href="https://bit.ly/mesadepartesvirtual"><img src="mesasunasss.png"
                         alt="sunass"
                         width="400" height="150" /></a>
              </div>
            </div>
            <div class="col-lg-3 col-md-5 ms-footer-col ms-footer-text-right">
              <div class="ms-footbar-block">
                <div class="ms-footbar-title">
                  <h3 class="no-m ms-site-title">EMPSSAPAL
                    <span></span>
                  </h3>
                </div>
                <address class="no-mb">

                    <i class="color-warning-light zmdi zmdi-map mr-1"></i> AV: CONFEDERACION</p>
                  <p>
                    <i class="color-info-light zmdi zmdi-email mr-1"></i>
                    <a href="mailto:joe@example.com">eps.empssapal.pe@gmail.com</a>
                  </p>
                  <p>
                    <i class="color-royal-light zmdi zmdi-phone mr-1"></i>084 352566 </p>
                  <p>
                    <i class="color-success-light fa fa-fax mr-1"></i>CEL EMERGENCIA 917356752 - 917356753</p>
                </address>
              </div>
              <div class="ms-footbar-block">
                <h3 class="ms-footbar-title">Nuestros Medios Sociales</h3>
                <div class="ms-footbar-social">
                  <a href="https://www.facebook.com/empssapal.sicuani.7"  target="_blank"  class="btn-circle btn-facebook">
                    <i class="zmdi zmdi-facebook"></i>
                  </a>
                  <a href="https://www.youtube.com/channel/UCdg82YQefxbGK43Lu4fyZgg?view_as=subscriber"  target="_blank" class="btn-circle btn-youtube">
                    <i class="zmdi zmdi-youtube-play"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </aside>
      <footer class="ms-footer">
        <div class="container">
          <p>Copyright &copy; Area de Informatica</p>
        </div>
      </footer>
      <div class="btn-back-top">
        <a href="mantenimiento.php" data-scroll id="back-top" class="btn-circle btn-circle-primary btn-circle-sm btn-circle-raised ">
          <i class="zmdi zmdi-long-arrow-up"></i>
        </a>
      </div>
    <!-- ms-site-container -->
<!-- MODAL DE AVISOS RECIBOS -->
<div class="modal" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog animated zoomIn animated-10x" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title color-primary" id="myModalLabel">CONSULTA VIA INTERNET</h1>
            </div>

            <div class="modal-body">
                <h3>INGRESE SU COD USUARIO</h3>
                <div class="form-group has-success">
                    <label class="control-label" for="inputSuccess">CODIGO DE RECIBO O USUARIO</label>
                    <input type="text" class="form-control" id="codigo" size="25" name="codigo"  placeholder="INGRESE SU CODIGO "  name="pwd" onblur="may(this.value,this.id)"  ></center>
                </div>
                <div class="validacion"></div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>

<div class="modal" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel11">
    <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
        <div class="modal-header red">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
            </div>
        <div class="modal-content">
            <img src="img/crono.PNG" width="100%" height="100%">

        </div>
    </div>
</div>
<div class="modal" id="myModalsegundo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel11">
    <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
        <div class="modal-header red">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
        </div>
        <div class="modal-content">
            <img src="horario.png" width="100%" height="100%">
        </div>
    </div>
</div>


<!-- MODAL DE SUGERENCIAS -->
<div class="modal" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog animated zoomIn animated-10x" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title color-primary" id="myModalLabel">ENVIENOS SU SUGERENCIA O RECLAMO</h4>
                <h6 class="modal-title color-primary" id="myModalLabel">En breve nos pondremos en contacto con usted.</h6>
            </div>
            <div class="modal-body">
                <h3>
                  <form action="sugerencia/ins_sug.php" method="POST" >

                          <div class="form-group row">
                              <div class="col-lg-6">
                                  <input type="number" required="required" class="form-control" id="dni" name="dni" placeholder="DNI">
                              </div>
                          </div>
                          <div class="form-group row">
                              <div class="col-lg-6">
                                  <input type="email"  class="form-control" id="correo" name="correo" placeholder="Correo">
                              </div>
                              <div class="col-lg-6">
                                  <input type="number" class="form-control" size="9" pattern="[0-9]{3}" id="movil" name="movil" placeholder="Número Celular">
                              </div>
                          </div>

                          <div class="form-group row justify-content-end">
                              <label for="textArea" class="col-lg-2 control-label">Sugerencia o Reclamo</label>

                              <div class="col-lg-10">
                                  <textarea class="form-control" required="required" rows="3" id="sugerencia" name="sugerencia"></textarea>
                                  <span class="help-block">Describanos su sugerencia</span>
                              </div>
                          </div>
                          <div class="form-group row justify-content-end">
                              <div class="col-lg-10">
                                  <button type="submit" class="btn btn-raised btn-primary">Enviar</button>
                                  <button type="button" class="btn btn-danger">Cancelar</button>
                              </div>
                          </div>
                  </form>

                </h3>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- MODAL DE AVISOS CHAT -->
<div class="modal" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog animated zoomIn animated-10x" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title color-primary" id="myModalLabel">ESPERE...</h1>
            </div>
            <div class="modal-body">
              <div class="panel panel-dark panel-default">
                  <h3>En breves momentos responderemos sus consultas.</h3>
                </div>
                <div class="form-group row justify-content-end">
                    <label for="textArea" class="col-lg-2 control-label">ESCRIBA SU CONSULTA</label>

                    <div class="col-lg-10">
                        <textarea class="form-control" rows="3" id="textArea"></textarea>
                        <span class="help-block">....</span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>


<!-- AVISO  -->
<div class="modal" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3">
    <div class="modal-dialog modal-sm animated zoomIn animated-3x" role="document">
        ...
    </div>
</div>

    <div class="ms-slidebar sb-slidebar sb-left sb-style-overlay" id="ms-slidebar">
      <div class="sb-slidebar-container">
        <header class="ms-slidebar-header">
          <div class="ms-slidebar-login">

            <a href="javascript:void(0)" class="withripple" data-toggle="modal" data-target="#ms-account-modal">
              <i class="zmdi zmdi-account"></i> ACCEDER</a>
          </div>
        </header>
        <ul class="ms-slidebar-menu" id="slidebar-menu" role="tablist" aria-multiselectable="true">
          <li class="card" role="tab" id="sch1">
            <a class="collapsed" role="button" data-toggle="collapse" href="#sc1" aria-expanded="false" aria-controls="sc1">
              <i class="zmdi zmdi-home"></i> QUIENES SOMOS </a>
            <ul id="sc1" class="card-collapse collapse" role="tabpanel" aria-labelledby="sch1" data-parent="#slidebar-menu">
              <li>
                <a href="historia.php">Historia</a>
              </li>
              <li>
                <a href="misionvision.php">Mision y Vision</a>
              </li>
              <li>
                <a href="objetivoinst.php">Objetivo Institucional</a>
              </li>
              <li>
                <a href="orginst.php">Organigrama Institucional</a>
              </li>
              <li>
                <a href="ubigeo.php">Ubicación Geografica</a>
              </li>
              <li>
                <a href="juntacionista.php">Junta de Accionista</a>
              </li>
              <li>
                <a href="directorio.php">Directorio</a>
              </li>
              <li>
                <a href="planagerencial.php">Plana Gerencial </a>
              </li>
            </ul>
          </li>
          <li class="card" role="tab" id="sch2">
            <a class="collapsed" role="button" data-toggle="collapse" href="#sc2" aria-expanded="false" aria-controls="sc2">
              <i class="zmdi zmdi-desktop-mac"></i> TRANSPARENCIA </a>
            <ul id="sc2" class="card-collapse collapse" role="tabpanel" aria-labelledby="sch2" data-parent="#slidebar-menu">

              <li>
                <a href="#">Presupuesto Anual</a>
              </li>
              <li>
                <a href="#">Plan Anual de Adquisicion</a>
              </li>
              <li>
                <a href="resoluciongerencia.php">Resolucion de Gerencia</a>
              </li>
              <li>
                <a href="directivasemitidas.php">Directivas Emitidas</a>
              </li>
              <li>
                <a href="metasgestion.php">Metas de Gestion</a>
              </li>
               <li>
                <a href="audienciapublica.php">Audiencia Publica</a>
              </li>
               <li>
                <a href="planopeinst.php">Plan Operativo Institucional</a>
              </li>
               <li>
                <a href="planestinst.php">Plan Estrategico Institucional</a>
              </li>
               <li>
                <a href="planorg.php">Planeamiento y Organizacion</a>
              </li>


            </ul>
          </li>
          <li class="card" role="tab" id="sch4">
            <a class="collapsed" role="button" data-toggle="collapse" href="#sc4" aria-expanded="false" aria-controls="sc4">
              <i class="zmdi zmdi-edit"></i> GOBERNABILIDAD </a>
            <ul id="sc4" class="card-collapse collapse" role="tabpanel" aria-labelledby="sch4" data-parent="#slidebar-menu">
               <li>
                <a href="#">Rendicion de Cuentas</a>
              </li>
               <li>
                <a href="#">Documentos de Gestion</a>
              </li>
               <li>
                <a href="estatuto.php">Estatuto</a>
              </li>
               <li>
                <a href="contexploacion.php">Contrato de Exploacion</a>
              </li>

               <li>
                <a href="paopeanual.php">Plan Operativo Anual</a>
              </li>

               <li>
                <a href="memoriaanual.php">Memoria Anual</a>
              </li>

               <li>
                <a href="resmodicion.php">Resolucion de Modificaciones</a>
              </li>
            </ul>
          </li>

          <li class="card" role="tab" id="sch6">
            <a class="collapsed" role="button" data-toggle="collapse" href="#sc6" aria-expanded="false" aria-controls="sc6">
              <i class="zmdi zmdi-collection-image-o"></i> ORIENTACION CLIENTE
            <ul id="sc6" class="card-collapse collapse" role="tabpanel" aria-labelledby="sch6" data-parent="#slidebar-menu">
               <li>
                <a href="valadmisibles.php">Valores Maximos Admisibles</a>
              </li>
               <li>
                <a href="horarioatencion.php">Horario de Atencion</a>
              </li>
               <li>
                <a href="centrocobranza.php">Centros de Cobranza</a>
              </li>
               <li>
                <a href="cronograma.php">Cronograma de Pagos</a>
              </li>
               <li>
                <a href="tarifarios.php">Tarifas</a>
              </li>

               <li>
                <a href="colaterales.php">Colaterales</a>
              </li>

            </ul>
          </li>
           <li class="card" role="tab" id="sch6">
            <a class="collapsed" role="button" data-toggle="collapse" href="#sc6" aria-expanded="false" aria-controls="sc6">
              <i class="zmdi zmdi-collection-image-o"></i> CONTACTOS
            <ul id="sc6" class="card-collapse collapse" role="tabpanel" aria-labelledby="sch6" data-parent="#slidebar-menu">
               <li>
                <a href="buzonsugerencias.php">Buzon de Sugerencias</a>
              </li>
               <li>
                <a href="contactenos.php">Contactenos</a>
              </li>

            </ul>
          </li>

        </ul>
        <div class="ms-slidebar-social ms-slidebar-block">
          <h4 class="ms-slidebar-block-title">NUESTRAS REDES SOCIALES</h4>
          <div class="ms-slidebar-social">
            <a href="https://www.facebook.com/empssapal.sicuani.7"   target="_blank"  class="btn-circle btn-circle-raised btn-facebook">
              <i class="zmdi zmdi-facebook"></i>
              <div class="ripple-container"></div>
            </a>
            <a href="https://www.youtube.com/channel/UCdg82YQefxbGK43Lu4fyZgg?view_as=subscriber"  target="_blank" class="btn-circle btn-circle-raised btn-google">
              <i class="zmdi zmdi-google"></i>
              <div class="ripple-container"></div>
            </a>

          </div>
        </div>
      </div>
    </div>



<!-- Modal -->
<div class="modal modal-danger" id="myModa111" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title color-white" id="myModalLabel"><?php echo $valor3; ?></h1>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
            </div>
            <div class="modal-body">
                <h3><?php echo $valor4 ?></h3>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">CERRAR</button>
                <button type="button" class="btn  btn-white"><?php echo $valor5; ?></button>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal" id="myModa222" tabindex="-1" role="dialog" aria-labelledby="myModalLabel11">
    <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
        <div class="modal-header red">
                <h1 class="modal-title color-white" id="myModalLabel"><?php echo $valor2; ?></h1>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
            </div>
        <div class="modal-content">
            <img src="<?php echo $valor1 ?>" width="100%" height="100%">

        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal" id="myModa333" tabindex="-1" role="dialog" aria-labelledby="myModalLabel11">
    <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
        <div class="modal-content">

            <div data-plyr-provider="youtube" data-plyr-embed-id="<?php echo $valor6 ?>" autoplay="1" class="js-player" width="1000px" hidden="500px"></div>
        </div>
    </div>
</div>
<!-- Modal -->

<div class="modal modal-danger" id="myModa444" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title color-white" id="myModalLabel"><?php echo $valor3; ?></h1>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
            </div>
            <div class="modal-body">
                <h3><?php echo $valor4 ?></h3>
            </div>
             <div class="modal-content">
                  <img src="<?php echo $valor1 ?>" width="100%" height="100%">

              </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">CERRAR</button>
            </div>
        </div>
    </div>
</div>
<script>
function texto() {
   $(document).ready(function() {
    $('#myModa111').modal('show');
});


<?php  ?>
}
function imagen() {
  $(document).ready(function() {
    $('#myModa222').modal('show');
});
}
function textoimagen() {
  $(document).ready(function() {
    $('#myModa444').modal('show');
});
}
function video() {
  $(document).ready(function() {
    $('#myModa333').modal('show');
});
}
</script>

    <script src="assets/js/plugins.min.js"></script>
    <script src="assets/js/app.min.js"></script>
    <script>
  function may(obj,id){
    obj = obj.toUpperCase();
    document.getElementById(id).value = obj;
  }
  $('#codigo').change( function(){
    $.post('consulta/ajax.php',{
      codigo:$('#codigo').val(),

        beforeSend: function(){
            $('.validacion').html("Espere un momento por favor..");
        }

    }, function(respuesta){
        $('.validacion').html(respuesta);

    });
  });
</script>

  </body>
</html>
