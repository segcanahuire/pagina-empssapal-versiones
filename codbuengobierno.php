<?php
include 'conexion/conexion.php';?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <title>EMPSSAPAL SA</title>
    <meta name="description" content="Material Style Theme">
    <link rel="shortcut icon" href="assets/img/empssapal/empssapal.jpg?v=3">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="assets/css/preload.min.css">
    <link rel="stylesheet" href="assets/css/plugins.min.css">
    <link rel="stylesheet" href="assets/css/style.light-blue-500.min.css">
    <link rel="stylesheet" type="text/css" href="dist/snackbar.min.css" />
  </head>
<body>
    <?php include 'menu.php'; ?>
    <section class="mt-12">
      <div class="container"><h4 class="color-royal">CODIGO DEL BUEN GOBIERNO CORPORATIVO</h4>
        <div class="row masonry-container">
          <div class="col-lg-6 col-md-2 masonry-item">
                      <div class="card card-royal">
                          <div class="card-body overflow-hidden text-center">
                              <span class="ms-icon ms-icon-round ms-icon-inverse color-royal ms-icon-lg mb-4"><i class="zmdi zmdi-city-alt"></i></span>
                              <h4 class="color-royal">Codigo del buen gobierno corporativo </h4>
                              <p>...</p>
                           <!-- Button trigger modal -->
                          <button type="button" class="btn btn-default btn-raised" data-toggle="modal" data-target="#resol">
                            VER
                          </button>
                           <a href="documentos/gobernabilidad/codbuengobierno/CODIGO-DE-BUEN-GOBIERNO-CORPORATIVO.pdf"  onclick="Snackbar.show({center: 'DESCARGANDO DOCUMENTOS'})" download="Resolucion de buen gobierno" class="btn btn-raised btn-primary"><i class="fa fa-download"></i> DESCARGAR</a>
                          </div>
                      </div>
                      <!-- Modal -->
                      <div class="modal" id="resol" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
                          <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
                             <iframe src="documentos/gobernabilidad/codbuengobierno/CODIGO-DE-BUEN-GOBIERNO-CORPORATIVO.pdf#zoom=100&view=fitH" frameborder="0" width="1000" height="1000" marginheight="0" marginwidth="0" id="pdf" style="border: none;"></iframe>
                          </div>
                      </div>
          </div>
          <div class="col-lg-6 col-md-2 masonry-item">
                      <div class="card card-royal">
                          <div class="card-body overflow-hidden text-center">
                              <span class="ms-icon ms-icon-round ms-icon-inverse color-royal ms-icon-lg mb-4"><i class="zmdi zmdi-city-alt"></i></span>
                              <h4 class="color-royal">Resolucion del codigo de buen gobierno </h4>
                              <p>...</p>
                           <!-- Button trigger modal -->
                          <button type="button" class="btn btn-default btn-raised" data-toggle="modal" data-target="#resol">
                            VER
                          </button>
                           <a href="documentos/gobernabilidad/codbuengobierno/CODIGO-BUEN-GOBIERNO-RESOLUCION.pdf"  onclick="Snackbar.show({center: 'DESCARGANDO DOCUMENTOS'})" download="Resolucion de buen gobierno" class="btn btn-raised btn-primary"><i class="fa fa-download"></i> DESCARGAR</a>
                          </div>
                      </div>
                      <!-- Modal -->
                      <div class="modal" id="resol" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
                          <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
                             <iframe src="documentos/gobernabilidad/codbuengobierno/CODIGO-BUEN-GOBIERNO-RESOLUCION.pdf#zoom=100&view=fitH" frameborder="0" width="1000" height="1000" marginheight="0" marginwidth="0" id="pdf" style="border: none;"></iframe>
                          </div>
                      </div>
          </div>


        </div>
        <div class="row masonry-container">

          <div class="col-lg-4 col-md-2 masonry-item">
                    <div class="card card-royal">
                        <div class="card-body overflow-hidden text-center">
                            <span class="ms-icon ms-icon-round ms-icon-inverse color-royal ms-icon-lg mb-4"><i class="zmdi zmdi-city-alt"></i></span>
                            <h4 class="color-royal">Informe anual </h4>
                            <p>...</p>
                         <!-- Button trigger modal -->
                        <button type="button" class="btn btn-default btn-raised" data-toggle="modal" data-target="#modal1111">
                        VER
                        </button>
                         <a href="documentos/gobernabilidad/codbuengobierno/INFORME-ANUAL-DE-GOBIERNO-CORPORATIVO.pdf"  onclick="Snackbar.show({center: 'DESCARGANDO DOCUMENTOS'})" download="Informe anual" class="btn btn-raised btn-primary"><i class="fa fa-download"></i> DESCARGAR</a>
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class="modal" id="modal1111" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
                        <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
                           <iframe src="documentos/gobernabilidad/codbuengobierno/INFORME-ANUAL-DE-GOBIERNO-CORPORATIVO.pdf#zoom=100&view=fitH" frameborder="0" width="1000" height="1000" marginheight="0" marginwidth="0" id="pdf" style="border: none;"></iframe>
                        </div>
                    </div>
          </div>
          <div class="col-lg-4 col-md-2 masonry-item">
                    <div class="card card-royal">
                        <div class="card-body overflow-hidden text-center">
                            <span class="ms-icon ms-icon-round ms-icon-inverse color-royal ms-icon-lg mb-4"><i class="zmdi zmdi-city-alt"></i></span>
                            <h4 class="color-royal">Informe Anual 2018 </h4>
                            <p>...</p>
                         <!-- Button trigger modal -->
                        <button type="button" class="btn btn-default btn-raised" data-toggle="modal" data-target="#modal22222">
                        VER
                        </button>
                         <a href="documentos/gobernabilidad/codbuengobierno/INFORME-AÑO-2018.pdf"  onclick="Snackbar.show({center: 'DESCARGANDO DOCUMENTOS'})" download="informe 2018" class="btn btn-raised btn-primary"><i class="fa fa-download"></i> DESCARGAR</a>
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class="modal" id="modal22222" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
                        <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
                           <iframe src="documentos/gobernabilidad/codbuengobierno/INFORME-AÑO-2018.pdf#zoom=100&view=fitH" frameborder="0" width="1000" height="1000" marginheight="0" marginwidth="0" id="pdf" style="border: none;"></iframe>
                        </div>
                    </div>
          </div>
          <div class="col-lg-4 col-md-2 masonry-item">
                    <div class="card card-royal">
                        <div class="card-body overflow-hidden text-center">
                            <span class="ms-icon ms-icon-round ms-icon-inverse color-royal ms-icon-lg mb-4"><i class="zmdi zmdi-city-alt"></i></span>
                            <h4 class="color-royal">Resultados de las Encuestas </h4>
                            <p>...</p>
                         <!-- Button trigger modal -->
                        <button type="button" class="btn btn-default btn-raised" data-toggle="modal" data-target="#encuesta">
                        VER
                        </button>
                         <a href="documentos/gobernabilidad/codbuengobierno/RESULTADO-DE-ENCUESTA-DE-USUARIOS.pdf"  onclick="Snackbar.show({center: 'DESCARGANDO DOCUMENTOS'})" download="Encuestas" class="btn btn-raised btn-primary"><i class="fa fa-download"></i> DESCARGAR</a>
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class="modal" id="encuesta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
                        <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
                           <iframe src="documentos/gobernabilidad/codbuengobierno/RESULTADO-DE-ENCUESTA-DE-USUARIOS.pdf#zoom=100&view=fitH" frameborder="0" width="1000" height="1000" marginheight="0" marginwidth="0" id="pdf" style="border: none;"></iframe>
                        </div>
                    </div>
          </div>


        </div>


      </div>
    </section>


    <?php include 'pie.php' ?>
