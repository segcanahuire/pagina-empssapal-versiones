<?php
include 'conexion/conexion.php';?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <title>EMPSSAPAL SA</title>
    <meta name="description" content="Material Style Theme">
    <link rel="shortcut icon" href="assets/img/empssapal/empssapal.jpg?v=3">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="assets/css/preload.min.css">
    <link rel="stylesheet" href="assets/css/plugins.min.css">
    <link rel="stylesheet" href="assets/css/style.light-blue-500.min.css">
    <link rel="stylesheet" type="text/css" href="dist/snackbar.min.css" />
  </head>
<body>
  <?php include 'menu.php'; ?>
      <section class="mt-12">
        <div class="container">
            <div class="ms-collapse no-margin" id="accordion11" role="tablist" aria-multiselectable="true">
                <div class="mb-0 card card-default">
                    <div class="card-header" role="tab" id="headingOne11">
                        <h4 class="card-title">
                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion11" href="#collapseOne11" aria-expanded="false" aria-controls="collapseOne11">
                                <i class="zmdi zmdi-pin"></i> Mision
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne11" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne11">
                        <div class="card-body">
                             Brindar un servicio de Agua Potable y Alcantarillado dentro de los parámetros de eficiencia y calidad, abasteciendo a la población de Sicuani y Santo Tomas Agua potable de muy buena calidad con una continuidad de 24 horas diarias.
                        </div>
                    </div>
                </div>
                <div class="mb-0 card card-default">
                    <div class="card-header" role="tab" id="headingTwo11">
                        <h4 class="card-title">
                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion11" href="#collapseTwo11" aria-expanded="false" aria-controls="collapseTwo11">
                                <i class="zmdi zmdi-pin"></i> Vision
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo11" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingTwo11">
                        <div class="card-body">
                           Ser una empresa líder en la prestación de servicios de Agua Potable y Alcantarillado en el País, para lo cual se ejecutara las transformaciones internas que lleve a alcanzar una cultura de gestión sustentada en el uso moderno de herramientas empresariales para el mejoramiento institucional y operativo.
                        </div>
                    </div>
                </div>

            </div>


        </div>
      </section>

    <?php include 'pie.php' ?>
