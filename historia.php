<?php
include 'conexion/conexion.php';?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <title>EMPSSAPAL SA</title>
    <meta name="description" content="Material Style Theme">
    <link rel="shortcut icon" href="assets/img/empssapal/empssapal.jpg?v=3">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="assets/css/preload.min.css">
    <link rel="stylesheet" href="assets/css/plugins.min.css">
    <link rel="stylesheet" href="assets/css/style.light-blue-500.min.css">
    <link rel="stylesheet" type="text/css" href="dist/snackbar.min.css" />
  </head>
<body>
  <?php include 'menu.php'; ?>


      <section class="mt-12">
        <div class="container">
          <div class="panel panel-warning">
            <div class="panel-heading">
                <h3 class="panel-title">Nuestra Reseña Historica</h3>
            </div>
            <div class="panel-body">
                Mediante D.L. N° 574 de 11 de abril de 1990, se promulga la Ley de Organización y funciones del Ministerio de Vivienda y Construcción, indicando en sus disposiciones complementarias transitorias en el numeral séptimo que SENAPA transferirá sus Empresas Filiales a las Municipalidades Provinciales y Distritales. En cumplimiento al D. L. N° 574 se promulga el procedimiento para la transferencia, mediante Decreto Ley N° 601 del 01 de mayo de 1990, posteriormente por Decreto supremo N° 134-90-PCM del 22 de Octubre de 1990, se transfirió a título gratuito las Acciones de Capital de SENAPA en su Empresa filial SEDAQOSQO, a varias Municipalidades Provinciales, entre ellas a la Municipalidad Provincial de Canchis. La administración de los servicios de agua potable y alcantarillado para el ámbito de la Provincia de Canchis, fue asumida directamente por la Municipalidad el 06 de Enero de 1995 se creo la Empresa Municipal Prestadora de Servicios de Saneamiento de Sicuani Sociedad Anónima – EMPSSSI S.A., regida por la Ley de la Actividad Empresarial del Estado y su Reglamento del Texto Único Ordenado de la Ley General de Sociedades, Ley General de Servicios de Saneamiento y su Reglamento, bajo la supervisión y fiscalización de la Superintendencia Nacional de servicios de Saneamiento –SUNASS, seguidamente se cambia la Razón Social de la Empresa, denominándola Empresa Municipal Prestadora de Servicios de Saneamiento de las Provincias Alto Andinas – EMPSSAPAL S.A. reconocido por la SUNASS en forma provisional el 09 de mayo de 1996, según Resolución de Superintendencia N° 097-96/PRES/VMI/SUNASS y posteriormente reconocida con Resolución de Superintendencia N° 017—97/PRES/VMI/SUNASS.
            </div>
          </div>
        </div>
      </section>

    <?php include 'pie.php' ?>
