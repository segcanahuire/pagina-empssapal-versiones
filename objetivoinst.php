<?php
include 'conexion/conexion.php';?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <title>EMPSSAPAL SA</title>
    <meta name="description" content="Material Style Theme">
    <link rel="shortcut icon" href="assets/img/empssapal/empssapal.jpg?v=3">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="assets/css/preload.min.css">
    <link rel="stylesheet" href="assets/css/plugins.min.css">
    <link rel="stylesheet" href="assets/css/style.light-blue-500.min.css">
    <link rel="stylesheet" type="text/css" href="dist/snackbar.min.css" />
  </head>
<body>
    <?php include 'menu.php'; ?>


      <section class="mt-12">
        <div class="container">
              <div class="ms-collapse" id="accordion5" role="tablist" aria-multiselectable="true">
    <div class="mb-0 card card-warning">
        <div class="card-header" role="tab" id="headingOne5">
            <h4 class="card-title">
                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion5" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                    <i class="zmdi zmdi-pin"></i> OBJETIVO GENERAL
                </a>
            </h4>
        </div>
        <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne5">
            <div class="card-body">
                 Ejecutar políticas del Sector de Saneamiento dispuestos por entidades Gubernamentales del Gobierno Central, Regional y Local; así como de la Junta General de Accionistas que conforman la EPS EMPSSAPAL S.A., en función de su capacidad operativa y disponibilidad de sus recursos económicos y financieros.
            </div>
        </div>
    </div>
    <div class="mb-0 card card-warning">
        <div class="card-header" role="tab" id="headingTwo5">
            <h4 class="card-title">
                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion5" href="#collapseTwo5" aria-expanded="false" aria-controls="collapseTwo5">
                    <i class="zmdi zmdi-pin"></i> OBJETIVO ESPECIFICO
                </a>
            </h4>
        </div>
        <div id="collapseTwo5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingTwo5">
            <div class="card-body">
                * Establecer las condiciones necesarias, para garantizar el normal abastecimiento de los servicios de Agua Potable y Alcantarillado
                * Establecer las condiciones necesarias, para garantizar el normal abastecimiento de los servicios de Agua Potable y Alcantarillado.
                * Ampliar la oferta de servicios de saneamiento de acuerdo al crecimiento de la población urbana que genera mayor demanda.
                * Ampliar la cobertura de mercado, instalando redes de distribución y nuevas conexiones de Agua Potable y Alcantarillado; comercializando dichos servicios.
                * Preservar el medio ambiente utilizando adecuadamente los recursos.
                * Modernizar la empresa optimizando la producción, incrementando la productividad, mejorando la calidad del servicio, y las relaciones entre la empresa y los clientes (colectividad).
                * Lograr un eficiente manejo Administrativo y Financiero de la empresa con la finalidad de fortalecer la capacidad técnica, económica y Administrativa de la Empresa, implementando acciones para el logro de la eficiencia y eficacia empresarial, como base del mejoramiento continuo de servicio que se presta.
            </div>
        </div>
    </div>
</div>
        </div>
      </section>

      <?php include 'pie.php' ?>
