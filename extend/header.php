<?php include '../conexion/conexion.php';
if (!isset($_SESSION['nick'])) {
  header('location:../');
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="stylesheet" type="text/css" href="../css/materialize.css">
	<link rel="stylesheet" type="text/css" href="../css/materialize.min.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="../fonstgoogle/icon.css">
	<title>EMPSSAPAL</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.3.2/sweetalert2.css">
  <link rel="stylesheet" type="text/css" href="../css/sweetalert2.css">
	 <link rel="icon" href="../img/empssapal.jpg" type="image/png" sizes="16x16"> 
	 <style media="screen">
	   
    header, main, footer {
      padding-left: 300px;
    }
    .button-collpase{
    	display: none;
    }

    @media only screen and (max-width : 992px) {
      header, main, footer {
        padding-left: 0;
      }

    .button-collpase{
    	display: inherit;
    }
    }
       
	 	
	 </style>
</head>
<body>
<main>

<?php 
switch ($_SESSION['nivel']) {
  case 'GERENCIA':
    include 'menu_admin.php'; 
    break;
  case 'FACTURACION':
    include 'menu_facturacion.php'; 
    break;
  case 'IMAGEN':
    include 'menu_imagen.php'; 
    break;  
  case 'INFORMATICA':
    include 'menu_informatica.php'; 
    break;  
  case 'G':
    include 'menu_g.php'; 
    break;  
  default:
}
/*if ($_SESSION['nivel']=='ADMINISTRADOR') {
  include 'menu_admin.php'; 
}else{
  include 'menu_asesor.php'; 
}*/
?>