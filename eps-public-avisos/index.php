<?php include '../extend/header.php'; ?>
	<div class="row">
	  <div class="col s7">
	    <div class="card">
	      <div class="card-content">
	       <span class="card-title">EMITE AVISOS EN IMAGEN,TEXTO o VIDEO</span>
	       
	      </div>

	    </div>
	  </div>

	  <div class="col s5">
	    <div class="card">
	      <div class="card-content">

	      	<?php 
				$sel1=$con->query("SELECT idaviso,tipo FROM aviso WHERE estado='1'");
				
				$f1 = $sel1->fetch_assoc();?>

					<span class="card-title"><h8 >TIPO AVISO</h8></span>
					<a href="cancel_aviso.php?us=<?php echo $f1['idaviso'] ?>"><i class="material-icons red-text">lock_outline</i></a>
				
					
	       
	      </div>

	    </div>
	  </div>

	</div>
	 <div class="col s6"> 
       <div class="card">
        
        <div class="card-tabs">
          <ul class="tabs tabs-fixed-width">
            <li class="tab"><a class="active" href="#imagen">IMAGEN</a></li>
            <li class="tab"><a href="#texto">TEXTO</a></li>
            <li class="tab"><a  href="#imgtexto">IMAGEN Y TEXTO</a></li>
            <li class="tab"><a  href="#video">VIDEO</a></li>
          </ul>
        </div>
        <div class="card-content grey lighten-4">
          <div id="imagen">
              <div class="row">
	  <div class="col s6">
	    <div class="card">
	      <div class="card-content">
	       <span class="card-title">FORMULARIO AVISO IMAGEN</span>
	       <form class="form" action="insavisoimg.php" method="POST"  enctype="multipart/form-data">
			<div class="input-field">				
				  
				  <input type="text" name="titulo" title="TITULO DE IMAGEN"  id="titulo" 
				      	required >
						<label for="titulo">Titulo imagen</label>
			</div>
			<div class="file-field input-field">
				<div class="btn red">
					<span>Foto:</span>
					<input type="file" name="foto">
				</div>
				<div class="file-path-wrapper">
					<input class="file-path validate" type="text" >
				</div>
			</div>
			<button type="submit" class="btn blue"> Guardar
				<i class="material-icons">send</i>
			</button>
	       </form>
	      </div>      
	    </div>
	  </div>
	</div>

          </div>
          <div id="imgtexto">
              <div class="row">
	  <div class="col s8">
	    <div class="card">
	      <div class="card-content">
	       <span class="card-title">FORMULARIO AVISO TEXTO CON IMAGEN</span>
	       <form class="form" action="insavisotextoimagen.php" method="POST"  enctype="multipart/form-data">
			<div class="input-field">				
				      	<input type="text" name="titulo" title="TITULO AVISO"  id="titulo" 
				      	required >
						<label for="titulo">Ingrese el titulo del Aviso</label>
			</div>
			<div class="input-field">				
				      	<input type="text" name="cuerpotexto" title="CUERPO DEL AVISO"  id="cuerpotexto" 
				      	required >
						<label for="cuerpotexto">Ingrese el cuerpo del Aviso</label>
			</div>
			<div class="file-field input-field">
				<div class="btn red">
					<span>Foto:</span>
					<input type="file" name="foto">
				</div>
				<div class="file-path-wrapper">
					<input class="file-path validate" type="text" >
				</div>
			</div>
			<button type="submit" class="btn blue"> Guardar
				<i class="material-icons">send</i>
			</button>
	       </form>
	      </div>      
	    </div>
	  </div>
	</div>

          </div>
          <div id="texto">
          	<div class="row">
	  <div class="col s6">
	    <div class="card">
	      <div class="card-content">
	       <span class="card-title">FORMULARIO AVISO TEXTO</span>
	       <form class="form" action="insavisotexto.php" method="POST"  enctype="multipart/form-data">
			<div class="input-field">				
				      	<input type="text" name="titulotexto" title="TITULO AVISO"  id="titulotexto" 
				      	required >
						<label for="titulotexto">Ingrese el titulo del Aviso</label>
			</div>
			<div class="input-field">				
				      	<input type="text" name="cuerpotexto" title="CUERPO DEL AVISO"  id="cuerpotexto" 
				      	required >
						<label for="cuerpotexto">Ingrese el cuerpo del Aviso</label>
			</div>
			<div class="input-field">				
				      	<input type="text" name="piepaginatexto" title="PIE DEL AVISO"  id="piepaginatexto" 
				      	required >
						<label for="piepaginatexto">Ingrese el pie del Aviso</label>
			</div>
			<button type="submit"  class="btn red"> Guardar
				<i class="material-icons">send</i>
			</button>
	       </form>
	      </div>      
	    </div>
	  </div>
	</div>

          </div>
          
          <div id="video">
       <div class="row">
	  <div class="col s6">
	    <div class="card">
	      <div class="card-content">
	       <span class="card-title">FORMULARIO AVISO VIDEO</span>
	       <form class="form" action="insavisovideo.php" method="POST"  enctype="multipart/form-data">
			<div class="input-field">				
				      	<input type="text" name="extencion" title="INGRESE LA EXTENCION DEL VIDEO"  id="extencion" 
				      	required >
						<label for="nombre">Ingrese la extencion del video</label>
			</div>
			<button type="submit"  class="btn orange"> Guardar
				<i class="material-icons">send</i>
			</button>
	       </form>
	      </div>      
	    </div>
	  </div>
	</div>   
              

          </div>
        </div>
      </div>
    </div>
<?php include '../extend/scripts.php'; ?>	
</body>
</html>

