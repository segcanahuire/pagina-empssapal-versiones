<?php 
include '../conexion/conexion.php';
date_default_timezone_set('America/Lima');
$fecha = date("Y-m-d H:i:s");
$nick=$_SESSION['nick'];
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	# code...
	$titulo = $con->real_escape_string(htmlentities($_POST['titulo']));
	$cuerpotexto = $con->real_escape_string(htmlentities($_POST['cuerpotexto']));
	$extension = '';
	$urlimg = 'eps-public-avisos/foto_perfil';
	$archivo = $_FILES['foto']['tmp_name'];
	$nombrearchivo=$_FILES['foto']['name'];
	$info = pathinfo($nombrearchivo);
	if ($archivo != '') {
		$extension = $info['extension'];
		if ($extension == "png" || $extension == "jpg" || $extension == "PNG" || $extension == "JPG") {
			$rand = rand(000,999); //nueva linea
      		move_uploaded_file($archivo,'foto_perfil/'.$nick.$rand.'.'.$extension); //linea actualizada
      		$urlimg = $urlimg."/".$nick.$rand.'.'.$extension;//linea actualizada
		}else{
			header('location:../extend/alerta.php?msj=No es el formato valido&c=home&p=in&t=error');
		}
	}else{
		$urlimg = "foto_perfil/perfil.jpg";
	}
	
	$up_aviso = $con->query("UPDATE aviso SET estado='0' ");
	$inss = $con->query("INSERT INTO aviso VALUES ('','4','$fecha','1')");
	$sel = $con->query("SELECT idaviso FROM aviso where estado='1'");
	$f4 = $sel->fetch_assoc();
	$idaaviso=$f4['idaviso'];
	echo "====".$idaaviso;
	/* INSERTAR TABLA */
	$ins = $con->query("INSERT INTO publicacion VALUES ('','$idaaviso','$urlimg','','$titulo','$cuerpotexto','','')");
	if ($ins) {
	 	header('location:../extend/alerta.php?msj=La imagen se publico correctamente&c=home&p=in&t=success');
	 }else{
		header('location:../extend/alerta.php?msj=No se publico la imagen&c=home&p=in&t=error');
	 }

	 $con->close(); 
}else{
	header('location:../extend/alerta.php?msj=Utiliza el formulario&c=us&p=in&t=error');
}
 ?>
