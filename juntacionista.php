<?php
include 'conexion/conexion.php';?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <title>EMPSSAPAL SA</title>
    <meta name="description" content="Material Style Theme">
    <link rel="shortcut icon" href="assets/img/empssapal/empssapal.jpg?v=3">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="assets/css/preload.min.css">
    <link rel="stylesheet" href="assets/css/plugins.min.css">
    <link rel="stylesheet" href="assets/css/style.light-blue-500.min.css">
    <link rel="stylesheet" type="text/css" href="dist/snackbar.min.css" />
  </head>
<body>
    <?php include 'menu.php'; ?>


      <section class="mt-12">
        <div class="container">
            <div class="ms-collapse" id="accordion5" role="tablist" aria-multiselectable="true">
                <div class="mb-0 card card-warning">
                    <div class="card-header" role="tab" id="headingOne5">
                        <h4 class="card-title">
                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion5" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                <i class="zmdi zmdi-pin"></i> JUNTA DE ACCIONISTAS
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne5">
                        <div class="card-body">
                            La Junta General de Accionistas es el órgano de mayor jerarquía de la EPS, realiza funciones que señala el Estatuto Social y todos aquellos que le atribuye la Ley Nº 26338 y su Reglamento, así como aquellas competencias que le establece suplementariamente la Ley General de Sociedades.
                        </div>
                    </div>
                </div>
            </div>
             <div class="row masonry-container">
                <div class="col-lg-6 col-md-2 masonry-item">
                    <a href="#" class="img-thumbnail withripple">
                        <div class="thumbnail-container">
                            <img src="img/alcalde/canchis.jpg" width="20%" height="20%" alt="..." class="img-fluid">
                        </div>
                        <h5>ALCALDE CANCHIS: Jorge Quispe Ccallo</h5>
                    </a>

                </div>
                <div class="col-lg-6 col-md-2 masonry-item">
                    <a href="#" class="img-thumbnail withripple">
                        <div class="thumbnail-container">
                            <img src="img/alcalde/chunbibilcas.jpg" width="20%" height="20%" alt="..." class="img-fluid">
                        </div>
                        <h5>ALCALDE Chumbivilcas: Marco Ibarra Suarez</h5>
                    </a>


                </div>
              </div>
        </div>
      </section>

    <?php include 'pie.php' ?>
