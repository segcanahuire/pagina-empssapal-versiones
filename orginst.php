<?php
include 'conexion/conexion.php';?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <title>EMPSSAPAL SA</title>
    <meta name="description" content="Material Style Theme">
    <link rel="shortcut icon" href="assets/img/empssapal/empssapal.jpg?v=3">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="assets/css/preload.min.css">
    <link rel="stylesheet" href="assets/css/plugins.min.css">
    <link rel="stylesheet" href="assets/css/style.light-blue-500.min.css">
    <link rel="stylesheet" type="text/css" href="dist/snackbar.min.css" />
  </head>
<body>
    <?php include 'menu.php'; ?>



      <section class="mt-12">
        <div class="container">
              <div class="ms-thumbnail-container">
                  <figure class="ms-thumbnail">
                      <img src="img/organigrama.jpg" alt="" class="img-fluid">
                      <figcaption class="ms-thumbnail-caption text-center">
                          <div class="ms-thumbnail-caption-content">
                              <h3 class="ms-thumbnail-caption-title">ORGANIGRAMA</h3>
                              EMPSSAPAL
                          </div>
                      </figcaption>
                  </figure>
              </div>

        </div>
      </section>

    <?php include 'pie.php' ?>
