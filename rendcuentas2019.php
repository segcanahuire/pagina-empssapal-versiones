<?php
include 'conexion/conexion.php';?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <title>EMPSSAPAL SA</title>
    <meta name="description" content="Material Style Theme">
    <link rel="shortcut icon" href="assets/img/empssapal/empssapal.jpg?v=3">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="assets/css/preload.min.css">
    <link rel="stylesheet" href="assets/css/plugins.min.css">
    <link rel="stylesheet" href="assets/css/style.light-blue-500.min.css">
    <link rel="stylesheet" type="text/css" href="dist/snackbar.min.css" />
  </head>
<body>

    <?php include 'menu.php'; ?>
      <section class="mt-12">
        <div class="container"><h4 class="color-royal">RENDICION DE CUENTAS DEL AÑO 2019</h4>
          <div class="row masonry-container">
            <div class="col-lg-4 col-md-2 masonry-item">
                  <div class="card card-royal">
                      <div class="card-body overflow-hidden text-center">
                          <span class="ms-icon ms-icon-round ms-icon-inverse color-royal ms-icon-lg mb-4"><i class="zmdi zmdi-city-alt"></i></span>
                          <h4 class="color-royal">RENDICION DE CUENTAS 2019 PRIMER TRIMESTRE</h4>
                          <p>...</p>
                       <!-- Button trigger modal -->
                      <button type="button" class="btn btn-default btn-raised" data-toggle="modal" data-target="#modal1">
                      VER
                      </button>
                       <a href="documentos/gobernabilidad/rendiciondecuentas/rendicioncuentas1tri2019.pdf"  onclick="Snackbar.show({center: 'DESCARGANDO DOCUMENTOS'})" download="Colaterales" class="btn btn-raised btn-primary"><i class="fa fa-download"></i> DESCARGAR</a>
                      </div>
                  </div>
                  <!-- Modal -->
                  <div class="modal" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
                      <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
                         <iframe src="documentos/gobernabilidad/rendiciondecuentas/rendicioncuentas1tri2019.pdf#zoom=100" width="1000" height="1000" marginheight="0" marginwidth="0" id="pdf" style="border: none;"></iframe>
                      </div>
                  </div>
            </div>
            <div class="col-lg-4 col-md-2 masonry-item">
                    <div class="card card-royal">
                        <div class="card-body overflow-hidden text-center">
                            <span class="ms-icon ms-icon-round ms-icon-inverse color-royal ms-icon-lg mb-4"><i class="zmdi zmdi-city-alt"></i></span>
                            <h4 class="color-royal">RENDICION DE CUENTAS 2019 SEGUNDO TRIMESTRE</h4>
                            <p>...</p>
                         <!-- Button trigger modal -->
                        <button type="button" class="btn btn-default btn-raised" data-toggle="modal" data-target="#modal2">
                        VER
                        </button>
                         <a href="documentos/gobernabilidad/rendiciondecuentas/rendicioncuentas1tri2019.pdf"  onclick="Snackbar.show({center: 'DESCARGANDO DOCUMENTOS'})" download="RENDICION DE CUENTAS 2019 DEL 1 TRIMESTRE" class="btn btn-raised btn-primary"><i class="fa fa-download"></i> DESCARGAR</a>
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class="modal" id="modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
                        <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
                           <iframe src="documentos/gobernabilidad/rendiciondecuentas/rendicioncuentas1tri2019.pdf#zoom=100" width="1000" height="1000" marginheight="0" marginwidth="0" id="pdf" style="border: none;"></iframe>
                        </div>
                    </div>
            </div>
            <div class="col-lg-4 col-md-2 masonry-item">

                      <div class="card card-royal">
                          <div class="card-body overflow-hidden text-center">
                              <span class="ms-icon ms-icon-round ms-icon-inverse color-royal ms-icon-lg mb-4"><i class="zmdi zmdi-city-alt"></i></span>
                              <h4 class="color-royal">RENDICION DE CUENTAS 2019 TERCER TRIMESTRE</h4>
                              <p>...</p>
                           <!-- Button trigger modal -->
                          <button type="button" class="btn btn-default btn-raised" data-toggle="modal" data-target="#modal3">
                          VER
                          </button>
                           <a href="documentos/gobernabilidad/rendiciondecuentas/rendicioncuentas3tri2019.pdf"  onclick="Snackbar.show({center: 'DESCARGANDO DOCUMENTOS'})" download="RENDICION DE CUENTAS 2019 DEL 3 TRIMESTRE" class="btn btn-raised btn-primary"><i class="fa fa-download"></i> DESCARGAR</a>
                          </div>
                      </div>
                      <!-- Modal -->
                      <div class="modal" id="modal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
                          <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
                             <iframe src="documentos/gobernabilidad/rendiciondecuentas/rendicioncuentas3tri2019.pdf#zoom=100" width="1000" height="1000" marginheight="0" marginwidth="0" id="pdf" style="border: none;"></iframe>
                          </div>
                      </div>
            </div>

          </div>


        </div>
      </section>

    <?php include 'pie.php' ?>
