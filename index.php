<?php
include 'conexion/conexion.php';?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <title>EMPSSAPAL SA</title>
    <meta name="description" content="Material Style Theme">
    <link rel="shortcut icon" href="assets/img/empssapal/empssapal.jpg?v=3">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="assets/css/preload.min.css">
    <link rel="stylesheet" href="assets/css/plugins.min.css">
    <link rel="stylesheet" href="assets/css/style.light-blue-500.min.css">
<link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"
  />
  <style type="text/css">
  
 .animate__infinite {
  --animate-repeat: 1000s;
}

  </style>
  </head>
<?php

$sel1=$con->query("SELECT * FROM aviso a, publicacion b WHERE a.idaviso=b.idaviso and a.estado=1");
$f1 = $sel1->fetch_assoc();
$valora=$f1['tipo'];
$valor1=$f1['urlimagen'];
$valor2=$f1['tituloimagen'];
$valor3=$f1['titulotexto'];
$valor4=$f1['cuerpotexto'];
$valor5=$f1['piepaginatexto'];
$valor6=$f1['extencionvideo'];

//IMAGEN 1
//TEXTO 2
//VIDEO 3
//NADA 0

if ($valora!=0) {
  if (2==$valora) {?>
    <body onload="texto()">
    <?php
  }
  if(1==$valora){?>
    <body onload="imagen()">
  <?php }
  if (3==$valora) {?>
    <body onload="video()">
  <?php }
  if (4==$valora) {?>
    <body onload="textoimagen()">
  <?php } ?>


<?php } else{
?>
<body>
<?php } ?>
<?php include 'menu.php'; ?>
       <div class="ms-hero ms-hero-material">
        <span class="ms-hero-bg"></span>
        <div class="container">
          <div class="row">
            <div class="col-xl-6 col-lg-7">
              <div id="carousel-hero" class="carousel slide carousel-fade" data-ride="carousel" data-interval="5000">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                  
                  
                  <div class="carousel-item active">
                    <div class="carousel-caption">
                      <div class="ms-hero-material-text-container">
                        <header class="ms-hero-material-title animated slideInLeft animation-delay-5">
                          <h1 class="animated fadeInLeft animation-delay-15">PLANTA
                            <strong>PTAR</strong>.</h1>
                          <h2 class="animated fadeInLeft animation-delay-18">Planta de Tratamiento de Aguas Residuales, Las aguas residuales pueden provenir de actividades industriales o agrícolas y del uso doméstico.<br></br></h2>
                        </header>
                        <div class="ms-hero-material-buttons text-right">
                          <a href="javascript:void(0);" class="btn btn-danger btn-raised animated fadeInLeft animation-delay-24 mr-2"  data-toggle="modal" data-target="#myModal4">
                            <i class="zmdi zmdi-settings"></i> Sugerencias</a>
                          <a href="http://35.198.55.197/e-siincoweb/empssapal/consultas/" class="btn btn-success btn-raised animated fadeInRight animation-delay-24"  >
                            <i class="zmdi zmdi-download"></i> Mi recibo</a>
                        <a href="javascript:void(0)" class="btn btn-info btn-raised"  data-toggle="modal" data-target="#myModal2"><i class="zmdi zmdi-settings"></i> VER CRONOGRAMA</a>
                        <!--<a href="javascript:void(0)" class="btn btn-danger btn-raised"  data-toggle="modal" data-target="#myModalsegundo"><i class="zmdi zmdi-settings"></i> HORARIO</a>-->
                        <a href="REQUISITOS-INSTALACION-AD.pdf" download="REQUISITOS PARA LA INSTALACION DE SERVICIOS" class="btn btn-danger btn-raised"><i class="zmdi zmdi-settings"></i> REQUISITOS DE INSTALACION</a>
                        </div>
                      </div>
                    </div>
                  </div>
<?php  
$selmensajes = $con->query("SELECT * FROM smspublicas WHERE estado = 1 ");
while($data12 = $selmensajes->fetch_assoc()){
    ?>
    <div class="carousel-item">
                    <div class="carousel-caption">
                      <div class="ms-hero-material-text-container">
                        <header class="ms-hero-material-title animated slideInLeft animation-delay-5">
                          <h1 class="animated fadeInLeft animation-delay-15"><?php echo $data12['titulo']; ?>
                            <strong><?php echo $data12['subtitulo']; ?></strong>.</h1>
                          <h2 class="animated fadeInLeft animation-delay-18"> <?php echo $data12['cuerpomensajes']; ?> <br></br></h2>
                        </header>
                        <div class="ms-hero-material-buttons text-right">
                            <a href="javascript:void(0);" class="btn btn-danger btn-raised animated fadeInLeft animation-delay-24 mr-2"  data-toggle="modal" data-target="#myModal4">
                            <i class="zmdi zmdi-settings"></i> Sugerencias</a>
                          <a href="http://35.198.55.197/e-siincoweb/empssapal/consultas/" class="btn btn-success btn-raised animated fadeInRight animation-delay-24"  >
                            <i class="zmdi zmdi-download"></i> Mi recibo</a>
                        
                        <a href="javascript:void(0)" class="btn btn-info btn-raised"  data-toggle="modal" data-target="#myModal2"><i class="zmdi zmdi-settings"></i> VER CRONOGRAMA</a>
                        <!--<a href="javascript:void(0)" class="btn btn-danger btn-raised"  data-toggle="modal" data-target="#myModalsegundo"><i class="zmdi zmdi-settings"></i> HORARIO</a>
                        </div>-->
                        <a href="REQUISITOS-INSTALACION-AD.pdf" download="REQUISITOS PARA LA INSTALACION DE SERVICIOS" class="btn btn-danger btn-raised"><i class="zmdi zmdi-settings"></i> REQUISITOS DE INSTALACION</a>
                        
                        </div>
                      </div>
                    </div>
                  </div>
<?php } ?>          
                  <div class="carousel-controls">
                    <!-- Controls -->
                    <a class="left carousel-control animated zoomIn animation-delay-30" href="#carousel-hero" role="button" data-slide="prev">
                      <i class="zmdi zmdi-chevron-left"></i>
                      <span class="sr-only">Anterior</span>
                    </a>
                    <a class="right carousel-control animated zoomIn animation-delay-30" href="#carousel-hero" role="button" data-slide="next">
                      <i class="zmdi zmdi-chevron-right"></i>
                      <span class="sr-only">Siguiente</span>
                    </a>
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <?php 
                        
                        $increment = $con->query("SELECT * FROM smspublicas WHERE estado = 1 ");

                        $incrementable = 1;
                        while($data13= $increment->fetch_assoc()){
                            
                        ?>
                      <li data-target="#carousel-hero" data-slide-to="<?php echo $incrementable++; ?>" class="animated fadeInUpBig animation-delay-27 active"></li>
                        
                        <?php }  ?>
                        
                    

                    </ol>
                  </div>
                  
                </div>
              </div>
            </div>
            <div class="col-xl-6 col-lg-5">
              <div class="ms-hero-img animated zoomInUp animation-delay-30">
                  <?php
                  $videoiniciando = $con->query("SELECT * FROM videoiniciando WHERE estado=1 ");
                  $selvideo = $videoiniciando->fetch_assoc();
                  $videopagina = $selvideo['url']; 
                  ?>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $videopagina;?>?rel=0&autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

              </div>
            </div>
          </div>
        </div>
        <!-- container -->
      </div>

<!---->
        <h1 class="color-primary text-center text-red" text-center>GALERIA DE VIDEOS</h1><br></br>
      <div class="ms-hero-page-override ms-hero-img-city ms-hero-bg-primary no-pb overflow-hidden ms-bg-fixed">
      
        <div class="container">
             <div class="row color-dark">
              <div class="col-xl-6 col-lg-6 col-sm-6">
                <div id="carousel-example-generic5" class="ms-carousel ms-carousel-thumb carousel slide" data-ride="carousel">
                    <div class="card card-relative">
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                                <div class="card">
                                    <div class="js-player" data-plyr-provider="youtube" data-plyr-embed-id="UwNoLolzA9U" ></div>
                                </div>
                            </div>
                              <?php
                                $sel = $con-> query("SELECT * FROM videoiniciando WHERE estado ='0' order by idvideo ASC ");
                                 ?>
                                 <?php while($f = $sel->fetch_assoc()){
                                     ?>
                            <div class="carousel-item">
                                <div class="card">
                                <div class="js-player" data-plyr-provider="youtube"
                                data-plyr-embed-id="<?php echo $f['url'] ?>"></div>
                            </div>

                            </div>
                            <?php } ?>
                        </div>
                            </div>
                </div>
              </div>
            <div class="col-xl-6 col-lg-6 col-sm-6">
                <div id="carousel-example-generic5" class="ms-carousel ms-carousel-thumb carousel slide" data-ride="carousel">
                    <div class="card card-relative">
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                                <div class="card">
                                    <div class="js-player" data-plyr-provider="youtube" data-plyr-embed-id="UwNoLolzA9U" ></div>
                                </div>
                            </div>
                              <?php
                                $sel = $con-> query("SELECT * FROM videoiniciando WHERE estado ='0'  order by idvideo DESC ");
                                 ?>
                                 <?php while($f = $sel->fetch_assoc()){
                                     ?>
                            <div class="carousel-item">
                                <div class="card">
                                <div class="js-player" data-plyr-provider="youtube"
                                data-plyr-embed-id="<?php echo $f['url'] ?>"></div>
                            </div>

                            </div>
                            <?php } ?>
                        </div>
                            </div>
                </div>
              </div>
              </div>

        </div>
        
        
    </div>
<!--   -->




     <section class="mt-6  ms-hero-bg-white">


        <div class="container ">
          <h1 class="color-primary text-center  animate__infinite animate__backInRight infinite" text-center>CONSULTA EN LINEA</h1>
          <div class="row d-flex justify-content-center">
            <div class="col-lg-4 col-md-6">
              <div class="card mt-4 card-danger wow zoomInUp">
                <div class="ms-hero-bg-danger ms-hero-img-city">
                  <img src="assets/img/checca/deseconomico.jpg" alt="..." class="img-avatar-circle"> </div>
                <div class="card-body pt-6 text-center">
                  <a href="http://35.198.55.197/e-siincoweb/empssapal/consultas/"   ><h3 class="color-danger">VER MI RECIBO DIGITAL</h3></a>
                  <p>Ahora podras consultar tus pagos.</p>

                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6">
              <div class="card mt-4 card-info wow zoomInUp">
                <div class="ms-hero-bg-info ms-hero-img-city">
                  <img src="assets/img/checca/desocial.jpg" alt="..." class="img-avatar-circle"> </div>
                <div class="card-body pt-6 text-center">
                  <a href="REQUISITOS-INSTALACION-AD.pdf" download="REQUISITOS PARA LA INSTALACION DE SERVICIOS" ><h3 class="color-info">REQUISITOS PARA UNA INSTALACION</h3></a>
                  <p>Descargue los requisitos para tramitar su Instalacion.</p>

                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6">
              <div class="card mt-4 card-warning wow zoomInUp">
                <div class="ms-hero-bg-warning ms-hero-img-city">
                  <img src="assets/img/checca/obras.png" alt="..." class="img-avatar-circle"> </div>
                <div class="card-body pt-6 text-center">
                  <a href="http://35.198.55.197/e-siincoweb/empssapal_st/reclamos_web/" ><h3 class="color-warning">Reclamos Virtuales.</h3></a>
                  <p>Ahora puedes realizar tus reclamos virtuales. .</p>

                </div>
              </div>
            </div>
          </div>
        </div>
      </section>


<!---->
 <div class="ms-hero-page-override  ms-hero-bg-white no-pb overflow-hidden ms-bg-fixed">
      <h1 class="color-primary text-center animate__infinite animate__backInLeft infinite" text-center>GALERIA DE IMAGENES</h1>
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="card wow zoomIn">
              <div class="ms-thumbnail card-body p-05 ">
                <div class="withripple zoom-img">
                      <div id="carousel-example-generic5" class="ms-carousel ms-carousel-thumb carousel slide" data-ride="carousel">
                    <div class="card card-relative">
                        <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">
                                <a href="assets/img/material/tercero.jpg" data-lightbox="gallery" data-title="My caption" c>
                                  <img class="d-block img-fluid" src="assets/img/material/segundo.jpg" >
                                </a>
                            </div>    
          <?php $selfotos = $con->query("SELECT * FROM imagenespublicas WHERE actividad='1'  order by Idimagenes ASC  ");
          while($datosfotos = $selfotos->fetch_assoc()){
          ?>
                            <div class="carousel-item">
                                <a href="$datosfotos['nombres']" data-lightbox="gallery" data-title="My caption" c>
                                  <img class="d-block img-fluid" src="<?php echo 'eps-public-imagenes/'.$datosfotos['nombres'];?>"  width="200" height="200" >
                                </a>
                            </div>
          <?php  }?>
                        </div>
                    </div>
                </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="card wow zoomIn">
              <div class="ms-thumbnail card-body p-05 ">
                <div class="withripple zoom-img">
                      <div id="carousel-example-generic5" class="ms-carousel ms-carousel-thumb carousel slide" data-ride="carousel">
                    <div class="card card-relative">
                        <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">
                                <a href="assets/img/material/tercero.jpg" data-lightbox="gallery" data-title="My caption" c>
                                  <img class="d-block img-fluid" src="assets/img/material/segundo.jpg" >
                                </a>
                            </div>    
          <?php $selfotos = $con->query("SELECT * FROM imagenespublicas WHERE actividad='1'  order by Idimagenes DESC  ");
          while($datosfotos = $selfotos->fetch_assoc()){
          ?>
                            <div class="carousel-item">
                                <a href="$datosfotos['nombres']" data-lightbox="gallery" data-title="My caption" c>
                                  <img class="d-block img-fluid" src="<?php echo 'eps-public-imagenes/'.$datosfotos['nombres'];?>"  width="200" height="200" >
                                </a>
                            </div>
          <?php  }?>
                        </div>
                    </div>
                </div>
                </div>
              </div>
            </div>
          </div>
          

          
          
          <?php $selfotos = $con->query("SELECT * FROM imagenespublicas WHERE actividad='1' order by Idimagenes asc  ");
          while($datosfotos = $selfotos->fetch_assoc()){
          ?>
          
          <div class="col-lg-3 col-md-3 col-sm-3">
            <div class="card wow zoomIn">
              <div class="ms-thumbnail card-body p-05 ">
                <div class="withripple zoom-img">
                      <div id="carousel-example-generic5" class="ms-carousel ms-carousel-thumb carousel slide" data-ride="carousel">
                    <div class="card card-relative">
                        <div class="carousel-inner" role="listbox">
                            
                           
                            <div class="carousel-item active">
                                <a href="$datosfotos['nombres']" data-lightbox="gallery" data-title="My caption" c>
                                  <img class="d-block img-fluid" src="<?php echo 'eps-public-imagenes/'.$datosfotos['nombres']; ?>" width="50" height="50">
                                </a>
                            </div>
                            
                            
                        </div>
                    </div>

                </div>

                </div>
              </div>
            </div>
          </div>
          <?php  }?>
          
          
          
          
          
          
          
          
          
        </div>
      </div>
  </div>





<?php include 'pie.php' ?>
