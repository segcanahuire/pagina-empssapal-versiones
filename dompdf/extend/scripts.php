</main>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="	  crossorigin="anonymous"></script>
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/materialize.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.3.2/sweetalert2.js"></script>
<script src="../js/sweetalert2.js"></script>
<script src="../js/materialize.js"></script>
<script>

	$('#buscarproceso').keyup(function(event){
		var contenido = new RegExp($(this).val(),'i');
		$('tr').hide();
		$('tr').filter(function(){
			return contenido.test($(this).text());
		}).show();
		$('.cabeceraproceso').attr('style','');
		$('.cabeceraterminado').attr('style','');
		$('.cuerpoterminado').attr('style','');
		$('.cuerpopago').attr('style','');
		$('.cabecerapago').attr('style','');
	});

	$('#buscarterminado').keyup(function(event){
		var contenido = new RegExp($(this).val(),'i');
		$('tr').hide();
		$('tr').filter(function(){
			return contenido.test($(this).text());
		}).show();
		$('.cabeceraproceso').attr('style','');
		$('.cabeceraterminado').attr('style','');
		$('.cuerpoproceso').attr('style','');
		$('.cuerpopago').attr('style','');
		$('.cabecerapago').attr('style','');
	});

	$('#buscarpago').keyup(function(event){
		var contenido = new RegExp($(this).val(),'i');
		$('tr').hide();
		$('tr').filter(function(){
			return contenido.test($(this).text());
		}).show();
		$('.cabeceraproceso').attr('style','');
		$('.cabeceraterminado').attr('style','');
		$('.cabecerapago').attr('style','');
		$('.cuerpoproceso').attr('style','');
		$('.cuerpoterminado').attr('style','');
	});
	$('#buscarnotas').keyup(function(event){
		var contenido = new RegExp($(this).val(),'i');
		$('tr').hide();
		$('tr').filter(function(){
			return contenido.test($(this).text());
		}).show();
		$('.cabeceranotas').attr('style','');
	});

	$('.button-collpase').sideNav();
	$('select').material_select();
	
	  $(document).ready(function(){
	    $('.modal').modal();
	  });
	//$('.datepicker').pickadate({
	
	//});
	$('.datepicker').pickadate({
		monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
		weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
		weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb'],
		selectMonths: true,
		selectYears: 100, // Puedes cambiarlo para mostrar más o menos años
		today: 'Hoy',
		clear: 'Limpiar',
		close: 'Ok',
		labelMonthNext: 'Siguiente mes',
		labelMonthPrev: 'Mes anterior',
		labelMonthSelect: 'Selecciona un mes',
		labelYearSelect: 'Selecciona un año',
	});
	
    
	function may(obj,id){
		obj = obj.toUpperCase();
		document.getElementById(id).value = obj;
	}
</script>