<?php include '../extend/header.php';
include '../extend/permiso.php';

 ?>
	<div class="row">
	  <div class="col s12">
	    <div class="card">
	      <div class="card-content">
	       <span class="card-title">Alta de Usuarios</span>
	       <form class="form" action="ins_usuario.php" method="POST"  enctype="multipart/form-data">
			<div class="input-field">				
		      	<input type="number" name="dni" required  title="Debe de ingresar su DNI" id="dni" data-length="8" >
				<label for="dni">DNI:</label>
			</div>
			<div class="validacion"></div>
			<div class="input-field">				
		      	<input type="text" name="nick" required autofocus title="Debe de contener entre 8 a 15 caracteres, solo letras"  id="nick" onblur="may(this.value,this.id)" pattern="A-Za-z0-9]{8,15}">
				<label for="nick">Nick:</label>
			</div>
	        <div class="validacion"></div>
			<div class="input-field">				
				      	<input type="text" name="pass1" title="Contraseña con Numeros, Letras Mayusculas y Minusculas entre 8 y 15 caracteres" pattern="[A-Za-z0-9]{8,15}"  id="pass1"  required >
				<label for="pass1">Contraseña</label>
			</div>
			<div class="input-field">				
				      	<input type="text"  title="Contraseña con Numeros, Letras Mayusculas y Minusculas entre 8 y 15 caracteres" pattern="[A-Za-z0-9]{8,15}"  id="pass2"  required >
				<label for="pass2">Verificar Contraseña</label>
			</div>

			<select name="nivel" required>
				<option value="" disabled  selected >ELIGE UN NIVEL DE USUARIO</option>
				<option value="ADMINISTRADOR"> ADMINISTRADOR</option>
				<option value="ASESOR">ASESOR</option>
				<option value="ALUMNO">CORDINADOR</option>
				<option value="ALUMNO">ALUMNO</option>
			</select>

			<div class="input-field">				
				      	<input type="text" name="nombre" title="NOMBRE DEL USUARIO"  id="nombre" 
				      	onblur="may(this.value,this.id)"
				      	required >
						<label for="nombre">Nombre Completo del usuario</label>
			</div>

			<div class="input-field">				
				      	<input type="email" name="correo" title="Correo"  id="correo" >
						<label for="correo">Correo Electronico</label>
			</div>
			<div class="file-field input-field">
				<div class="btn">
					<span>Foto:</span>
					<input type="file" name="foto">
				</div>
				<div class="file-path-wrapper">
					<input class="file-path validate" type="text" >
				</div>
			</div>
			<button type="submit" id="btn_guardar" class="btn black"> Guardar
				<i class="material-icons">send</i>
			</button>
	       </form>
	      </div>      
	    </div>
	  </div>
	</div>

<div class="row">
  <div class="col s12">
    <div class="brown ligfhten-3">
      <div class="nav-wrapper">
      	<div class="input-field">
      		<input type="search" name="buscar" id="buscar" autofocus="off">
      		<label for="buscar"> <i class="material-icons">search</i></label>    
      		<i class="material-icons" >close</i>
      	</div>
      </div>      
    </div>
  </div>
</div>
	<?php
	$sel = $con-> query("SELECT * FROM usuarios WHERE nivel !='ALUMNO'");
	$row = mysqli_num_rows($sel); 
	 ?>
<div class="row">
  <div class="col s12">
    <div class="card">
      <div class="card-content">
       <span class="card-title"></span>
       <table>
       	<caption>LISTA DE USUARIOS</caption>
       	<thead>
       		<tr class="cabecera">
       			<th>Nick</th>
       			<th>Nombre</th>
       			<th>Correo</th>
       			<th>Nivel</th>
       			<th></th>
       			<th>Foto</th>
       			<th>Bloqueo</th>
       			<!--<th>Eliminar</th>-->
       		</tr>
       	</thead>
       	<?php while($f = $sel->fetch_assoc()){ ?>
       	<tbody>
       		<tr>
       			<td><?php echo $f['nick'] ?></td>
       			<td><?php echo $f['nombre'] ?></td>
       			<td><?php echo $f['correo'] ?></td>
       			<td>
       				<form action="up_nivel.php" method="post" >
       					<input type="hidden" name="id" value="<?php echo $f['id'] ?>" >
	       				<select name="nivel" required>
								<option value="" disabled  selected ><?php echo $f['nivel'] ?></option>
								<option value="ADMINISTRADOR"> ADMINISTRADOR</option>
								<option value="ASESOR">ASESOR</option>
								<option value="ALUMNO">CORDINADOR</option>
								<!--<option value="ALUMNO">ALUMNO</option>-->
						</select>

       			</td>
       			<td><button type="submit" class="btn-floating"><i class="material-icons">repeat</i></button>
       				</form>
       			</td>
       			<td><img src="<?php echo $f['foto'] ?>"width="50" class="circle" ></td>
       			<td>
       				<?php if ($f['bloqueo']==1): ?>
							<a href="bloqueo_manual.php?us=<?php echo $f['id']?>&bl=<?php echo $f['bloqueo'] ?>"><i class="material-icons green-text">lock_open</i></a>
					<?php else: ?>
						<a href="bloqueo_manual.php?us=<?php echo $f['id']?>&bl=<?php echo $f['bloqueo'] ?>"><i class="material-icons red-text">lock_outline</i></a>
       				<?php endif;  ?>
       			</td>
       			<td>
       				
       				<a href="#" class="btn-floating red " onclick="swal({  title: 'Esta seguro que desea eliminar al usuario?', text: 'Al eliminarlo no podra recuperarlo!!',  type: 'warning',  showCancelButton: true,confirmButtonColor: '#3085d6', cancelButtonColor: '#d33',  confirmButtonText: 'Si, eliminar!'}).then(function() {  location.href='eliminar_usuario.php?id=<?php echo $f['id']?>';})" >
       					<i class="material-icons">clear</i></a>
       			</td>

       		</tr>
       	</tbody>
       <?php } ?>
       </table>
      </div>      
    </div>
  </div>
</div>
<script>
	function may(obj,id){
		obj = obj.toUpperCase();
		document.getElementById(id).value = obj;
	}
	$('#dni').change( function(){
		$.post('ajax_dni.php',{
			codigo:$('#dni').val(),

				beforeSend: function(){
						$('.validacion').html("Espere un momento por favor..");
				}
		
		}, function(respuesta){
				$('.validacion').html(respuesta);
			
		});
	});	
		

</script>

<?php include '../extend/scripts.php'; ?>
<script src="../js/validacion.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="	  crossorigin="anonymous"></script>
<script src="../js/materialize.min.js"></script>
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/sweetalert2.js"></script>
<script src="../js/materialize.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.3.2/sweetalert2.js"></script>	

</body>
</html>

