<?php 
include '../../conexion/conexion.php';
date_default_timezone_set('America/Lima');
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$dni = $con->real_escape_string(htmlentities($_POST['dni']));
	//$usuario = $con->real_escape_string(htmlentities($_POST['usuario']));
	$usuario = $dni;
	$pass1 = $con->real_escape_string(htmlentities($_POST['pass1']));
	$genero = $con->real_escape_string(htmlentities($_POST['genero']));
	$nombres = $con->real_escape_string(htmlentities($_POST['nombres']));
	$apepat = $con->real_escape_string(htmlentities($_POST['apepat']));
	$apemat = $con->real_escape_string(htmlentities($_POST['apemat']));
	$correo = $con->real_escape_string(htmlentities($_POST['correo']));
	$edad = $con->real_escape_string(htmlentities($_POST['edad']));
	$cel = $con->real_escape_string(htmlentities($_POST['cel']));
	$fechana = $con->real_escape_string(htmlentities($_POST['fechana']));
	$estadocivil = $con->real_escape_string(htmlentities($_POST['estadocivil']));
	$trabaja = $con->real_escape_string(htmlentities($_POST['trabaja']));
	$lugartrabajo = $con->real_escape_string(htmlentities($_POST['lugartrabajo']));
	$direccion = $con->real_escape_string(htmlentities($_POST['direccion']));
    //
    $idcicloprosem = $con->real_escape_string(htmlentities($_POST['programasemestre']));
	$idcolegio = $con->real_escape_string(htmlentities($_POST['colegio']));
	$fechaactual = getdate();
	//$fecha = "$fechaactual[hours] horas de  $fechaactual[mday] de $fechaactual[month] de $fechaactual[year]";
	//$fecha = new DateTime("now", new DateTimeZone('America/Lima'));
    //$fecha->format("Y-m-d H:i:s");
	$fecha = date("Y-m-d H:i:s");
	$estadomatriculados = "proceso";


	$departamento = $con->real_escape_string(htmlentities($_POST['departamento']));
	 $sel3=$con->query("SELECT namereg FROM regions WHERE idreg='$departamento' ");
	 $nombress = $sel3->fetch_assoc();
	 $departamento = $nombress['namereg'];

	$provincia = $con->real_escape_string(htmlentities($_POST['provincia']));
	 $sel4=$con->query("SELECT namepro FROM provinces WHERE idpro='$provincia' ");
	 $nombres4 = $sel4->fetch_assoc();
	 $provinces = $nombres4['namepro'];
	
	//VERIFICAR DNI ESTA REGISTRADO
	$sel4=$con->query("SELECT dni FROM  usuarios WHERE dni='$dni'");
	$idconf = $sel4->fetch_assoc();
	if ($idconf['dni'] =='') {
		//SI EL DNI NO TIENE LOS 8 DIGITOS
			$count = strlen($dni);
		if ($count == 8) {
					/* INSERTAR TABLA */
				$ins_usu = $con->query("INSERT INTO usuarios VALUES ('','$usuario','$dni','$pass1','$nombres','$correo','ALUMNO',0,'foto_perfil/perfil.jpg','$estadocivil','$fechana')");
				if ($ins_usu) {
				 	$sel3=$con->query("SELECT id,dni FROM  usuarios  WHERE nick='$usuario'");
					$ids = $sel3->fetch_assoc();
					$idusuario = $ids['id'];
					$dni = $ids['dni'];
					 /* INSERTAR TABLA */
					$ins_alu = $con->query("INSERT INTO alumno VALUES ('','$idusuario','$nombres','$apemat','$apepat','$genero','$edad','$departamento','$provinces','00000','$trabaja','$lugartrabajo','$direccion','$cel','')");
					if ($ins_alu) {
							$sel4=$con->query("SELECT idalumno FROM  alumno  WHERE id= '$idusuario'");
					        $ids4 = $sel4->fetch_assoc();
					        $idusuario4 = $ids4['idalumno'];

						$ins_premat = $con->query("INSERT INTO prematricula VALUES ('','$idcicloprosem','$idusuario4','$idcolegio','$fecha','$estadomatriculados')");
							if ($ins_premat) {
								//$idusuario4 = 44;
								  echo '<script src="../../js/sweetalert2.js"></script>';
								  echo '<script src="../../js/materialize.js></script>';
								  echo '<script src="../../js/materialize.min.js></script>';
								  echo '<link rel="stylesheet" type="text/css" href="../../css/sweetalert2.css">';
								  echo '<link rel="stylesheet" type="text/css" href="../../css/materialize.css">';
								  echo '<link rel="stylesheet" type="text/css" href="../../css/materialize.min.css">';
								  echo '<script type="text/javascript">';
								  echo 'setTimeout(function () { 
								  	location = "../pdf.php?cod='.$idusuario4.'"
									swal({
									  title: "MATRICULADO",
									  text: "Esperee se Descargara su ficha de matricula para que pueda imprimirlo y presentarlo en el area administracion del IEST CANCHIS",
									  type: "success",
									  showCancelButton: true,
									  confirmButtonColor: "#3085d6",
									  confirmButtonText: "Ok"
									}).then(function(){
									location.href="../../index.php";
									});
									/* si o si nos enviara*/
									$(document).click(function(){
									location.href="../../index.php";
									});
									$(document).keyup(function(){
									if (e.which == 27) {
									location.href="../../index.php";
									} });



								  	';
								  

								  echo '}, 100);</script>';
								//header('location:../pdf.php?cod='.$idusuario4);
							  	

							  	//header('location:../../extend/alerta.php?msj=FELICITACIONES LOGRASTES MATRICULARTE &c=te&p=in&t=success');
							  		
							  	//header('location:../../extend/alert_va.php?msj=DESCARGA TU FICHA DE MATRICULA Y IMPRIMELO&c=mat&p=con&t=success&cod='.$dni);



							 }else{
									header('location:../../extend/alerta.php?msj=APROXIMARSE AL INSTITUTO PARA SOLUCIONAR SU MATRICULA&c=con&p=in&t=error');
							}
					 	////////aqui trabajamos
					 	/*header('location:../../extend/alert_va.php?msj=Bien seleciona tu programa de estudios y semestre&c=mat&p=con&t=success&cod='.$dni);*/

					 }else{
						header('location:../../extend/alerta.php?msj=El Alumno no pudo ser registrado&c=ma&p=in&t=error');
					 }
				 }else{
					header('location:../../extend/alerta.php?msj=El usuario no pudo ser registrado&c=ma&p=in&t=error');
				 }
			}else{
				header('location:../../extend/alerta.php?msj=ERROR EN EL DNI&c=ma&p=in&t=error');
			}
	//		header('location:../../extend/alerta.php?msj=ERROR EN EL DNI&c=ma&p=in&t=error');
	}else{
		header('location:../../extend/alerta.php?msj=ERROR EN EL DNI&c=ma&p=in&t=error');
	 	}
	 $con->close(); 
}else{
	header('location:../../extend/alerta.php?msj=Utiliza el formulario&c=ma&p=in&t=error');
}
?>

