<?php 
include '../conexion/conexion.php';
$idalumno = $con->real_escape_string(htmlentities($_GET['cod']));
$sel=$con->query("SELECT e.nombreprograma, f.nomsemestre, c.dni,a.fecha_matri, b.nombrealu, b.apellidomaalu, b.apellidopaalu, b.departamento, b.provincia,b.direccion,c.fechanacimiento,b.edad,c.estadocivil,b.genero,b.cel,c.correo,  b.trabaja,b.lugartrabajo FROM prematricula a, alumno b, usuarios c, ciclo_programa d, programa e, semestre f WHERE d.idsemestre=f.idsemestre and a.idcipro=d.idcipro and d.idprograma=e.idprograma and c.id=b.id and b.idalumno=a.idalumno and a.idalumno='$idalumno'");
$resul = $sel->fetch_assoc();
$programa = $resul['nombreprograma'];
$semestre = $resul['nomsemestre'];
$dni = $resul['dni'];
$fechamatricula = $resul['fecha_matri'];
$nombre = $resul['nombrealu'];
$paterno = $resul['apellidopaalu'];
$materno = $resul['apellidomaalu'];
$departamento = $resul['departamento'];
$provincia = $resul['provincia'];
$direccion = $resul['direccion'];
$edad = $resul['edad'];
$estadocivil = $resul['estadocivil'];
$genero = $resul['genero'];
$fechanacimiento = $resul['fechanacimiento'];
$cel = $resul['cel'];
$correo = $resul['correo'];
$trabaja = $resul['trabaja'];
$lugartrabajo = $resul['lugartrabajo'];

ob_start(); ?>

<h5 align="left"><b><img src="../img/ficha_banner.JPG"  width="100%"  height="20%"></b></h5>

<br></br>
<table align="right" width="40%" cellpadding="3" border="1">
	<caption></caption>
	<tbody>
		<tr>
			<th>DNI</th>
			<td><?php echo $dni; ?></td>
		</tr>
	</tbody>
</table>
<table  width="40%" cellpadding="3" border="1">
	<caption></caption>
	<tbody>
		<tr>
			<th>FECHA</th>
			<td><?php echo $fechamatricula; ?></td>
		</tr>
	</tbody>
</table>
<br></br>
<table  width="100%" cellpadding="3" border="1">
	<caption>DATOS PERSONALES</caption>
	<thead>
		<tr>
			<th>NOMBRE</th>
			<th>APELLIDO PATERNO</th>
			<th>APELLIDO MATERNO</th>
			
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><?php  echo $nombre; ?></td>
			<td><?php  echo $paterno; ?></td>
			<td><?php  echo $materno; ?></td>
			
		</tr>
	</tbody>

	<thead>
		<tr>
			<th>DEPARTAMENTO</th>
			<th>PROVINCIA</th>
			<th>DIRECCION DOMICILIARIA</th>
			
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><?php echo $departamento; ?></td>
			<td><?php echo $provincia; ?></td>
			<td><?php echo $direccion; ?></td>
			<td> Pegue aqui su Foto</td>
		</tr>
	</tbody>

	<thead>
		<tr>
			
			<th>FECHA NAC.</th>
			<th>EDAD</th>
			<th>ESTADO CIVIL</th>
			<th>GENERO</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			
			<td><?php echo $fechanacimiento; ?></td>
			<td><?php echo $edad; ?></td>
			<td><?php echo $estadocivil; ?></td>
			<td><?php echo $genero; ?></td>
		</tr>
	</tbody>
</table>
<br></br>
<table  width="100%" cellpadding="3" border="1">
	<caption>DATOS ACADEMICO</caption>
	<thead>
		<tr>
			<th>PROGRAMA DE ESTUDIOS</th>
			<th>SEMESTRE MATRICULADO</th>

		</tr>
	</thead>
	<tbody>
		<tr>
			<td><?php echo $programa; ?></td>
			<td><?php echo $semestre; ?></td>
		</tr>
	</tbody>
	
</table>
<br></br>
<table  width="100%" cellpadding="3" border="1">
	<caption>DATOS DE CONTACTO</caption>
	<thead>
		<tr>
			<th>NUMERO MOVIL O FIJO</th>
			<th>EMAIL</th>

		</tr>
	</thead>
	<tbody>
		<tr>
			<td><?php echo $cel; ?></td>
			<td><?php echo $correo; ?></td>
		</tr>
	</tbody>
	
</table>
<br></br>

<br></br>
<br></br>
<br></br>

<h5 align="center"><b><img src="../img/firma.JPG"  width="100%"  height="20%"></b></h5>

<?php 
require_once '../dompdf/autoload.inc.php';
use Dompdf\Dompdf;
$dompdf = new Dompdf();
$dompdf->loadHtml(ob_get_clean());
$dompdf->setPaper('A4','portrait');
$dompdf->render();
$dompdf->stream('ficha-preinscripcion'.Date('Y-m-d').".pdf");
?>