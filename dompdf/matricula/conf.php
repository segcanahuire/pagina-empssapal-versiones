<!DOCTYPE html>
<?php include '../conexion/conexion.php'; ?>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="stylesheet" type="text/css" href="../css/materialize.css">
	<link rel="stylesheet" type="text/css" href="../css/materialize.min.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../fonstgoogle/icon.css">
  <link rel="stylesheet" href="fonstgoogle/icon.css">
  <title>Bienvenidos al Matricula Virtual</title>
  <link rel="icon" href="../img/image1.png" type="image/png" sizes="16x16"> 
  <link rel="stylesheet" type="text/css" href="../css/sweetalert2.css">
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="	  crossorigin="anonymous"></script>

</head>

<body  class="#d50000 red accent-4">
<?php

$num = $_GET['cod'];
if (is_numeric($num)) {
	$sel = $con->query("SELECT b.idalumno, a.dni, b.nombrealu, b.apellidomaalu, b.apellidopaalu FROM usuarios a, alumno b WHERE a.dni='$num' and a.id=b.id ");
	$datos = $sel->fetch_assoc();
	$cod = $datos['nombrealu'].' '.$datos['apellidomaalu'].' '.$datos['apellidopaalu'];
	//$cod = $datos['nombrealu'].$datos['apellidomaalu'].$datos['apellidopaalu'];
}else{
	header('location:../../extend/alerta.php?msj=Error&c=ma&p=in&t=error');
}
 ?>
<nav>
    <div class="nav-wrapper #d50000 red accent-4">
      <a href="#" class="brand-logo center"><img src="../img/cabecera.JPG" height="100" width="100%" ></a>
      <ul id="nav-mobile" class="left hide-on-med-and-down">
       
      </ul>
    </div>
  </nav><br></br><br></br><br></br>

	<div class="row">
	  <div class="col s6">
	    <div class="card">
	      <div class="card-content">
	       <span class="card-title">PERFIL DE ALUMNO(A)</span>
			
			    <h2 class="header"></h2>
				    <div class="card horizontal">
				      <div class="card-image">
				        <img width="200" height="100" src="../usuario/foto_perfil/perfil.jpg" > 
				      </div>
				      <div class="card-stacked">
				        <div class="card-content">
				            <div class="input-field col s12">
	
					          <input disabled value="<?php echo $cod; ?>" id="disabled" type="text" class="validate">
					          <label for="">Nombres y Apellidos</label>
					        </div>
					       
					      </div>
				      </div>
				    </div>
	      </div>      
	    </div>
	  </div>	
	  	  <div class="col s6">
	    <div class="card">
	      <div class="card-content">
	       <span class="card-title">TU MATRICULA</span>
			
			    <h2 class="header"></h2>
				    <div class="card horizontal">
				      
				      <div class="card-stacked">
				        <div class="card-content">
				            <div class="input-field col s12">
	
					          <a class="waves-effect waves-light btn-large" href="pdf.php?cod=<?php echo $datos['idalumno'] ?>" ><i class="material-icons left">picture_as_pdf</i>DESCARGAR Ficha de PRE MATRICULA</a>
							</div>
					        
					       
					      </div>
				      </div>
				    </div>
	      </div>      
	    </div>
	  </div>
	</div>






<script>
	$('select').material_select();
	$('.button-collpase').sideNav();
</script>
</script>
<?php include '../extend/scripts.php'; ?>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="	  crossorigin="anonymous"></script>
<script src="../js/materialize.min.js"></script>
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/sweetalert2.js"></script>
<script src="../js/materialize.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.3.2/sweetalert2.js"></script>	
</body>
</html>