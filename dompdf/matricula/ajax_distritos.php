<?php 
include '../conexion/conexion.php';
$provincia = htmlentities($_POST['provincia']);
?>
<select id="distrito" name="distrito"  >
	<option value="" disabled selected>SELECCIONA UN DISTRITO</option>
				<?php

				 $sel_dist = $con->prepare("SELECT iddis, namedis FROM  districts WHERE province_id =
					?");
				$sel_dist->bind_param('i',$provincia);
				$sel_dist->execute();
				$res_dist = $sel_dist->get_result(); 
				while ($f_dist = $res_dist->fetch_assoc()) {
				?>
					<option value="<?php echo $f_dist['iddis']?>"><?php echo $f_dist['namedis']?></option>
				<?php } $sel_dist->close(); 
				?>
</select>
<script >
	$('select'.concat('#distrito')).material_select();
</script>