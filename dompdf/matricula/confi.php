<?php include '../extend/header.php';
include '../extend/permiso.php';?>
  <link rel="stylesheet" type="text/css" href="../css/materialize.css">
  <link rel="stylesheet" type="text/css" href="../css/materialize.min.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../fonstgoogle/icon.css">
  <link rel="stylesheet" href="fonstgoogle/icon.css">
  <link rel="stylesheet" type="text/css" href="../css/sweetalert2.css">
<div class="row">
	  <div class="col s12" >
	    
		    <div class="card-tabs">
		      <ul class="tabs tabs-fixed-width">
		        <li class="tab"><a class="active" href="#prematricula">PRE-MATRICULA</a></li>
		      </ul>
		    </div>
		    <div class="card-content grey lighten-4">
		      <div id="prematricula">
		      			        <div class="row">
									  <div class="col s12">
									    <div class="brown ligfhten-3">
									      <div class="nav-wrapper">
									      	<div class="input-field">
									      		<input type="search" name="buscar" id="buscarproceso" autofocus="off">
									      		<label for="buscar"> <i class="material-icons">search</i></label>    
									      		<i class="material-icons" >close</i>
									      	</div>
									      </div>      
									    </div>
									  </div>
									</div>
										<?php
										$sel = $con-> query("SELECT b.nombrealu,b.idalumno, b.apellidomaalu, b.apellidopaalu, c.idprematricula FROM usuarios a, alumno b, prematricula c  WHERE c.idalumno=b.idalumno and  a.id=b.id and a.nivel ='ALUMNO' and c.estadomatriculados='proceso'"); 
										$row = mysqli_num_rows($sel); 
										 ?>
									<div class="row">
									  <div class="col s12">
									    <div class="card">
									      <div class="card-content">
									       <span class="card-title"></span>
									       <table>
									       	<caption>LISTA DE PRE MATRICULAS</caption>
									       	<thead>
									       		<tr class="cabeceraproceso">
									       			<th>NOMBRE</th>
									       			<th>ELIMINAR</th>
									       			<th>PDF</th>
									       			<th>CONFIRMAR</th>
									       		</tr>
									       	</thead>
									       	<?php while($f = $sel->fetch_assoc()){ ?>
									       	<tbody>
									       		<tr class="cuerpoproceso">
									       			<td><?php echo $f['nombrealu']." ".$f['apellidopaalu']." ".$f['apellidomaalu'] ?></td>
									       			<td>
									       				<a href="#" class="btn-floating red " onclick="swal({  title: 'Esta seguro que deseas eliminar la PRE-MATRICULA?', text: 'Al eliminarlo no podra recuperarlo!!',  type: 'warning',  showCancelButton: true,confirmButtonColor: '#3085d6', cancelButtonColor: '#d33',  confirmButtonText: 'Si, eliminar!'}).then(function() {  location.href='php/eliminar_maaluusu.php?id=<?php echo $f['idalumno']?>';})" >
									       					<i class="material-icons">clear</i></a>
									       			</td>
									       			<td><a href="pdf.php?cod=<?php echo $f['idalumno'] ?>" class="btn-floating green" ><i class="material-icons">picture_as_pdf</i></a></td>
									       			<td>
									       				<form action="php/ins_pago.php" method="post" >
									       					<input type="hidden" name="prematricula" value="<?php echo $f['idprematricula'] ?>" >
										       				<input type="hidden" name="idalumno" value="<?php echo $f['idalumno'] ?>" >
															<select name="tipopago"  required>
															<option value="" disabled  selected >ELIGE EL TIPO DE PAGO </option>
																<?php 
																$sell = $con->prepare("SELECT idpago ,tipopago FROM  pago WHERE idciclo='2'");
																$sell ->execute();
																$sell -> bind_result($idpago ,$tipopago);
																 while ($sell->fetch()) { ?>
																<option value="<?php echo $idpago ?>"> <?php echo $tipopago ?></option>
																<?php } ?>
															</select>
									       			</td>
									       			<td><button type="submit" class="btn-floating blue"><i class="material-icons">check</i></button>
									       			</form>
									       			</td>
									       		</tr>
									       	</tbody>
									       <?php } ?>
									       </table>
									      </div>      
									    </div>
					  </div>
					</div>
			  </div>
		     
		    </div>
	  </div>
	</div>			  
<script>
    $('.modal').modal();
</script>
<?php include '../extend/scripts.php'; ?>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="	  crossorigin="anonymous"></script>
<script src="../js/materialize.min.js"></script>
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/sweetalert2.js"></script>
<script src="../js/materialize.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.3.2/sweetalert2.js"></script>
</body>
</html>