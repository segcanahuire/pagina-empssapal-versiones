<?php 
include '../conexion/conexion.php';
$departamento = htmlentities($_POST['depa']);
?>
<div class="input-field">
				        <i class="material-icons prefix">location_on</i>
<select  name="provincia"  id="provincia" required="" >
				<option value="" disabled selected>SELECCIONA UNA PROVINCIA</option>

				<?php $sel_prov = $con->prepare("SELECT idpro, namepro FROM  provinces WHERE region_id = ?");
				$sel_prov->bind_param('i',$departamento);
				$sel_prov->execute();
				$res_pro = $sel_prov -> get_result(); 
				while ($f_pro = $res_pro->fetch_assoc()) {
				?>
					<option value="<?php echo $f_pro['idpro']?>"><?php echo $f_pro['namepro']?></option>
				<?php } 
				?>
</select>
</div>
<!--<script src="../js/distritos.js"></script>-->
<script >
	$('select'.concat('#provincia')).material_select();
</script>