<!DOCTYPE html>
<?php include '../conexion/conexion.php'; ?>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="stylesheet" type="text/css" href="../css/materialize.css">
	<link rel="stylesheet" type="text/css" href="../css/materialize.min.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../fonstgoogle/icon.css">
  <link rel="stylesheet" href="fonstgoogle/icon.css">
  <link rel="stylesheet" type="text/css" href="../css/sweetalert2.css">
  <title>Bienvenidos al Matricula Virtual</title>
  <link rel="icon" href="../img/ico.png" type="image/png" sizes="16x16"> 
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="	  crossorigin="anonymous"></script>
<script src="../js/jquery-3.3.1.min.js"></script>
</head>
<body  class="#d50000 red accent-4">

  <nav>
    <div class="nav-wrapper #d50000 red accent-4">
      <a href="#" class="<brand-logo></brand-logo> center"><img src="../img/cabecera.JPG" height="100" width="100%" ></a>
      <ul id="nav-mobile" class="left hide-on-med-and-down"></ul>
    </div>
  </nav>
	<div class="row">
  <div class="col s12">
    <div class="card">
      <form  action="php/ins_matricula.php" method="post" autocomplete="off" >
      <div class="card-content">
        
        
        <center><span class="card-title" >DATOS PERSONALES</span></center>
          <div class="row">
			          <div class="col s10">
			          	<div class="input-field">
							<i class="material-icons prefix">perm_identity</i>				
					      	<input type="number" name="dni" required  title="Debe de ingresar su DNI" id="dni" data-length="8" >
							<label for="dni">DNI:</label>
						</div>
						<div class="validacion">
							
						</div>
			          </div>
			          <div class="col s2">
			          	<a class="waves-effect waves-light btn modal-trigger" href="#modal1">NOTA</a>
						
							  <!-- Modal Structure -->
							  <div id="modal1" class="modal">
							    <div class="modal-content">

							      <h4>IMPORTANTE Informacion!!</h4>
							      <p>Su ficha de PRE MATRICULA SE DESCARGARA AUTOMATICAMENTE al finalizar la matricula.</p>
								  <p>Terminastes tu pre matricula ahora puedes aproximarte al IEST CANCHIS para terminar la matricula.</p><p> Guarda la contraseña para qué puedas acceder a las aulas virtuales.</p>
								  
					      
							    </div>
							    <div class="modal-footer">
							      <a href="#!" class="modal-close waves-effect waves-green btn-flat">CERRAR</a>
							    </div>
							  </div>
			          </div>
			          <div class="col s6">
				        <div class="input-field">				
							      	<i class="material-icons prefix">person</i>				
							      	<input type="text" name="nombres" title="NOMBRES" onblur="may(this.value,this.id)" required  id="nombres"> 
							      	<label for="nombre">Nombres completos</label>
						</div>
						<div class="input-field">				
							      	<i class="material-icons prefix">person</i>				
							      	<input type="text" name="apepat" title="APEPA" onblur="may(this.value,this.id)" required id="apepat"> 
							      	<label for="apepat">Apellidos Paterno</label>
						</div>
				       <div class="input-field">				
							      	<i class="material-icons prefix">person</i>				
							      	<input type="text" name="apemat" title="APEMA" onblur="may(this.value,this.id)" required id="apemat"> 
							      	<label for="apemat">Apellidos Materno</label>
						</div>
			          </div> <!--Termina Primer columna -->
			          <div class="col s6">
						<div class="input-field">				
							      	<i class="material-icons prefix">insert_emoticon</i>	
							      	<input type="number" name="edad" title="NOMBRES" required id="edad"> 
							      	<label for="edad">Edad</label>
						</div>
						<div class="input-field">				
							      	<i class="material-icons prefix">wc</i>	
						<select name="genero" required>
							<option value="" disabled  selected >ELIGE GENERO</option>
							<option value="MASCULINO"> MASCULINO</option>
							<option value="FEMENINO">FEMENINO</option>
						</select></div>
						<div class="input-field">				
							      	<i class="material-icons prefix">group</i>
						<select name="estadocivil" required>
							<option value="" disabled  selected >ELIGE ESTADO CIVIL</option>
							<option value="SOLTERO"> SOLTERO (A)</option>
							<option value="CASADO">CASADO (A)</option>
							<option value="VIUDO">CASADO (A)</option>
							<option value="CONVIVIENTE">CONVIVIENTE (A)</option>
						</select></div>
						
			          </div><!-- TerminaSegunda columna -->
        </div>
        
		
		<center><span class="card-title" >DATOS ADICIONALES</span></center>
        <div class="row">
	          <div class="col s6">
		          	<div class="input-field">
						        <i class="material-icons prefix">perm_data_setting</i>
					<select name="trabaja" required>
						<option value="" disabled  selected >ELIGE SI TRABAJA</option>
						<option value="SI"> Si, trabaja</option>
						<option value="NO">No, Trabaja</option>
					</select>
				   </div>
					<div class="input-field">
					  <i class="material-icons prefix">directions_walk</i>
			          <textarea id="work" name="lugartrabajo" class="materialize-textarea"   ></textarea>
			          <label for="textarea1">Donde Labora y Puesto Describanos</label>
			        </div>
			        <div class="input-field">
					  <i class="material-icons prefix">home</i>
			          <textarea id="direccion" name="direccion" class="materialize-textarea" required></textarea>
			          <label for="textarea1">Direccion Domiciliaria</label>
			        </div>

	          </div><!-- Termina Primera columna -->

              <div class="col s6">
					<div class="input-field">	
							<i class="material-icons prefix">perm_contact_calendar</i>
					      	<input type="date" class="datepicker"  name="fechana" required title="fechana"  id="fechana"> 
					        <label for="fechana">Selecciona tu Fecha de Nacimiento</label>
				    </div>
			        <label for="text">LUGAR DE NACIMIENTO</label>	
					<div class="input-field">
						        <i class="material-icons prefix">location_on</i>
						<select name="departamento" id="departamento" required>
							<option value="" disabled selected>SELECCIONA UN DEPARTAMENTO</option>
							<?php $selll = $con->prepare("SELECT idreg, namereg FROM  regions");
							$selll->execute();
							$res_reg = $selll -> get_result(); 
							while ($f_reg = $res_reg->fetch_assoc()) {
								
							?>	
								<option value="<?php echo $f_reg['idreg']?>"><?php echo $f_reg['namereg']?></option>
							
							<?php  }
							?>
						</select>
				    </div>
					<div class="provincia"></div>
              </div><!-- Termina Segunda columna -->
        </div>


		<center><span class="card-title" >DATOS PARA CONTACTO</span></center>
       <div class="row">
		          <div class="col s6">
						<div class="input-field">
						        <i class="material-icons prefix">phone</i>				
						      	<input type="number" name="cel" title="MOVIL" required  id="cel"> 
						      	<label for="icon_telephone">Número de Celular o Telefno</label>
						</div>
		          </div><!--Termina Primer columna -->

		          <div class="col s6">
						<div class="input-field">				
						      	<i class="material-icons prefix">email</i>	
						      	<input type="email" name="correo" title="Correo" required id="correo" >
								<label for="correo">Correo Electronico</label>
					</div>
		          </div><!-- TerminaSegunda columna -->
        </div>

       <center><span class="card-title" >DATOS DE ACADEMICOS</span></center>
       <div class="row">
	          <div class="col s6">
		          	<div class="input-field">
						        <i class="material-icons prefix">account_balance</i>
						<select name="colegio" required>
						<option value="" disabled  selected >ELIGE TU COLEGIO</option>
							
						<?php 

							$sel = $con->prepare("SELECT idcolegio,nomcole,departamento,provincia,distrito  FROM colegio");
							$sel ->execute();
							$sel -> bind_result($idcolegio,$nomcole,$departamento,$provincia,$distrito);

						while ($sel->fetch()) {
						 ?>
							<option value="<?php echo $idcolegio ?>"> <?php echo $nomcole." de ".$distrito ?></option>
					<?php } ?>
						</select>
					</div>

	          </div><!-- Termina Primera columna -->
          <div class="col s6">
          			<label for="text">PROGRAMA DE ESTUDIOS A MATRICULARSE</label>
				  

		          	<div class="input-field">
						        <i class="material-icons prefix">school</i>
						<select name="programa" id="programa" required>
						<option value="" disabled  selected >ELIGE TU PROGRAMA DE ESTUDIOS </option>
							<?php 
							$sell = $con->prepare("SELECT idprograma,nombreprograma FROM  programa ");
							$sell ->execute();
							$sell -> bind_result($idprograma,$nombreprograma);
							 while ($sell->fetch()) { ?>
							<option value="<?php echo $idprograma ?>"> <?php echo $nombreprograma ?></option>
							<?php } ?>
						</select>
					</div>
					
		          
		          <div class="semestres"></div>
        </div>
        </div>
        

       <center><span class="card-title" >DATOS DE ACCESO AL PORTAL VIRTUAL</span></center>
        <div class="row">
          <div class="col s6">
			<div class="input-field">				
				  <i class="material-icons prefix">security</i>
				  <input type="password" name="pass1" title="Contraseña con Numeros, Letras Mayusculas y Minusculas entre 8 y 15 caracteres" pattern="[A-Za-z0-9]{8,15}"  id="pass1"  required >
				  <label for="pass1">Contraseña</label>
			</div>

          </div><!-- Termina Primera columna -->

          <div class="col s6">
			<div class="input-field">				
				  <i class="material-icons prefix">security</i>
				  <input type="password"  title="Contraseña con Numeros, Letras Mayusculas y Minusculas entre 8 y 15 caracteres" pattern="[A-Za-z0-9]{8,15}"  id="pass2"  required >
				  <label for="pass2">Verificar Contraseña</label>
			</div>
          </div><!-- Termina Segunda columna -->
        </div>
        <center>
        	<button type="submit" id="btn_guardar" class="btn black"> MATRICULARME
				<i class="material-icons">send</i>
			</button>
        </center>
        
      </div>
      </form>
    </div>
  </div>
</div>



	
<script>
	$('select').material_select();
	$('.button-collpase').sideNav();
	$('#textarea1').val('New Text');
    M.textareaAutoResize($('#direccion'));
</script>
<script>
	function may(obj,id){
		obj = obj.toUpperCase();
		document.getElementById(id).value = obj;
	}
	$('#dni').change( function(){
		$.post('ajax_validacion_dni.php',{
			codigo:$('#dni').val(),

				beforeSend: function(){
						$('.validacion').html("Espere un momento por favor..");
				}
		
		}, function(respuesta){
				$('.validacion').html(respuesta);
	8		
		});
	});	
		$('#usuario').change( function(){
		$.post('ajax_validacion_usuario.php',{
			usuario:$('#usuario').val(),

				beforeSend: function(){
						$('.validacionusuario').html("Espere un momento por favor..");
				}
		
		}, function(respuesta){
				$('.validacionusuario').html(respuesta);
			
		});
	});
		$('#programa').change( function(){
		$.post('ajax_programaestuidos.php',{
			programa:$('#programa').val(),

			beforeSend: function(){
						$('.semestres').html("Espere un momento por favor..");
				}
		
		}, function(respuesta){
				$('.semestres').html(respuesta);
			
		});
	});
	
	document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.modal');
    var instances = M.Modal.init(elems, options);
  });

  // Or with jQuery

  $(document).ready(function(){
    $('.modal').modal();
  });	

</script>
<?php include '../extend/scripts.php'; ?>
<script src="../js/validacion.js"></script>
<script src="../js/distritos.js"></script>
<script src="../js/departamentos.js"></script>
<script src="../js/programasestudio.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="	  crossorigin="anonymous"></script>
<script src="../js/materialize.min.js"></script>
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/sweetalert2.js"></script>
<script src="../js/materialize.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.3.2/sweetalert2.js"></script>	
</body>
</html>