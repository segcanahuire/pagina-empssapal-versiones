<?php 
include '../conexion/conexion.php';
$programa = htmlentities($_POST['programa']);
?>
<div class="input-field">
				        <i class="material-icons prefix">school</i>
<select  name="programasemestre"  id="programasemestre" required="" >
				<option value="" disabled selected>SELECCIONA UN SEMESTRE</option>

				<?php $sel_sem = $con->prepare("SELECT a.idcipro, b.idsemestre, b.nomsemestre FROM  ciclo_programa a,  semestre b WHERE a.idsemestre=b.idsemestre and  idprograma = ?");
				$sel_sem->bind_param('i',$programa);
				$sel_sem->execute();
				$res_sem = $sel_sem -> get_result(); 
				while ($f_sem = $res_sem->fetch_assoc()) {
				?>
					<option value="<?php echo $f_sem['idcipro']?>"><?php echo $f_sem['nomsemestre']?></option>
				<?php } 
				?>
</select>
</div>
<!--<script src="../js/distritos.js"></script>-->
<script >
	$('select'.concat('#programasemestre')).material_select();
</script>