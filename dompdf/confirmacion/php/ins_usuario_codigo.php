<?php 
include '../../conexion/conexion.php';
$nombrecompleto = $con->real_escape_string(htmlentities($_GET['nombres']));
/*$conn = $con->query("SELECT nombres FROM academico WHERE  nombres='$nombrecompleto' ");
	$dato = $conn->fetch_assoc();
	$cod = $dato['nombres'];*/
?>
<link rel="stylesheet" type="text/css" href="../../css/materialize.css">
<link rel="stylesheet" type="text/css" href="../../css/sweetalert2.css">
<div class="row">
    <div class="col s12 m6">
        <div class="card-content black-text">
          <span class="card-title">GUARDAR CODIGO </span>
          <div class="row">
        
			<form class="form" action="../php/ins_usuario_codigo.php" method="POST"  enctype="multipart/form-data">
				<div>
					<input  type="text" disabled value='<?php echo $nombrecompleto ;?>' id="disabled" type="text" class="validate" >
				</div>
				<div class="input-field">				
				    <input type="text" name="codigo_inst" required title="Debe de contener entre 8 a 15 caracteres, solo letras"  pattern="A-Za-z0-9]{8,15}">
					<label for="codigo_inst">CODIGO DEL INSTITUTO:</label>
				</div>
				<button type="submit" id="btn_guardar" class="btn black"> Registrarse
					<i class="material-icons">send</i>
				</button>
			</form>
          </div>    
      </div>
    </div>
  </div>
  <script>
  	$(document).ready(function() {
    M.updateTextFields();
  });
  </script> 
<script src="../../js/jquery.js" type="text/javascript" charset="utf-8" ></script>
<script src="../../js/materialize.js"></script>