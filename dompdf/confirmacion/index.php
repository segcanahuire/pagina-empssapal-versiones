<?php include '../extend/header.php';
include '../extend/permiso.php';?>
  <link rel="stylesheet" type="text/css" href="../css/materialize.css">
  <link rel="stylesheet" type="text/css" href="../css/materialize.min.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../fonstgoogle/icon.css">
  <link rel="stylesheet" href="fonstgoogle/icon.css">
  <link rel="stylesheet" type="text/css" href="../css/sweetalert2.css">
<div class="row">
									  	
									  <div class="col s3">
									    <div class=" ligfhten-3">
									      <div class="nav-wrapper">
									      	<div class="input-field">
									      		
									      	 <!-- Modal Trigger -->
											  <a class="btn waves-effect blue darken-2" href="#modal1">Nuevo Ingreso <i class="material-icons right">add_circle</i></a>

											  <!-- Modal Structure -->
											  <div id="modal1" class="modal">
											    <div class="modal-content">
											      <h4>REGISTRAR INGRESANTE</h4>
											      <form class="form" action="php/ins_usuario_nuevo.php" method="POST"  enctype="multipart/form-data">
													<div class="input-field">				
												      	<input type="number" name="cod_nota" required  title="Codigo de matricula" data-length="8" >
														<label for="cod_nota">DNI:</label>
													</div>
													<div class="validacion"></div>
													<div class="input-field">				
												      	<input type="text" name="codigo_inst" required title="Debe de contener entre 8 a 15 caracteres, solo letras"  pattern="A-Za-z0-9]{8,15}">
														<label for="codigo_inst">CODIGO DEL INSTITUTO:</label>
													</div>
											        <button type="submit" id="btn_guardar" class="btn black"> Registrarse
														<i class="material-icons">send</i>
													</button>
											       </form>
											    </div>
											    <div class="modal-footer">
											      <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cancelar</a>
											    </div>
											  </div>

											</div>
									      </div> 

									    </div>
									  </div>
									  <div class="col s9">
									    <div class="brown ligfhten-3">
									      <div class="nav-wrapper">
									      	<div class="input-field">
									      		<input type="search"  name="buscar" id="search"  autofocus="off">
									      		<label for="buscar"> <i class="material-icons">search</i></label>    
									      		<i class="material-icons" >close</i>
									      	</div>

									      </div> 

									    </div>									      
										</div>
										
											   
											      <div class="col-md-12 col-md-offset-12" id="result"></div>
											    
									
										
										
										<div id='modal2' class='modal'>
										    <div class='modal-content'>
										        <div class="res_modal">
										        	
										        </div>
											    <div class='modal-footer'>
											      <a href='#!' class='modal-close waves-effect waves-green btn-flat'>CANCELAR</a>
											    </div>
										     </div>

										</div>
										<div id='modal3' class='modal'>
										    <div class='modal-content'>
										        <div class="res_modal_codigo">
										        	
										        </div>
											    <div class='modal-footer'>
											      <a href='#!' class='modal-close waves-effect waves-green btn-flat'>CANCELAR</a>
											    </div>
										     </div>

										</div>
<script>
    $('.modal2').modal();

    function enviar(valor){
    	$.get('php/ins_usuario_antiguo.php',{
    		id:valor,
    		beforeSend: function(){
    			$('.res_modal').html("Espere un momento por favor")
    		}
    	}, function(respuesta){
			$('.res_modal').html(respuesta);
    	});
    }
    function enviar_sincodigo(valor){
    	$.get('php/ins_usuario_codigo.php',{
    		nombres:valor,
    		beforeSend: function(){
    			$('.res_modal_codigo').html("Espere un momento por favor")
    		}
    	}, function(respuesta){
			$('.res_modal_codigo').html(respuesta);
    	});
    }
</script>
<?php include '../extend/scripts.php'; ?>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="	  crossorigin="anonymous"></script>
<script src="../js/materialize.min.js"></script>
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/sweetalert2.js"></script>
<script src="../js/materialize.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.3.2/sweetalert2.js"></script>
<script type="text/javascript" src="js/index.js"></script>
</body>
</html>


