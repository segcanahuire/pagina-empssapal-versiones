<?php include '../extend/header.php'; ?>
	<div class="row">
	  <div class="col s12">
	    <div class="card">
	      <div class="card-content">
	       <span class="card-title">ADMINISTRE EL SLIDER</span>  
	      </div>      
	    </div>
	  </div>
	</div>
	<div class="row">
	  <div class="col s8 center">
	    <div class="card">
	      <div class="card-content">
	       <span class="card-title">FORMULARIO DE SLIDER</span>
	       <form class="form" action="ins_slider.php" method="POST"  enctype="multipart/form-data">
			<div class="input-field">				
				<input type="text" name="titulo" title="TITULO AVISO"  id="titulo" required >
				<label for="titulo">Ingrese el titulo del Aviso</label>
			</div>
			<div class="input-field">				
				 <input type="text" name="extencion" title="EXTENCION DEL VIDEO"  id="extencion" required >
				<label for="extencion">Ingrese la extencion del video</label>
			</div>
			<div class="input-field">				
				 <input type="text" name="pie" title="PIE DEL AVISO"  id="pie" required >
				 <label for="pie">Ingrese el pie del Aviso</label>
			</div>
			<button type="submit" id="btn_guardar" class="btn orange"> Guardar
				<i class="material-icons">send</i>
			</button>
	       </form>
	      
	      </div>      
	    </div>
	  </div>
	</div>
	<?php
	$sel = $con-> query("SELECT * FROM slider ");
	$row = mysqli_num_rows($sel); 
	 ?>
<div class="row">
  <div class="col s12">
    <div class="card">
      <div class="card-content">
       <span class="card-title"></span>
       <table>
       	<caption>LISTA DE Slider</caption>
       	<thead>
       		<tr class="cabecera">
       			<th>TITULO</th>
       			<th>PIE PAGINA</th>
       			<th>EXTENCION</th>
       			<th>FECHA</th>
       			<th>OPCION</th>
       			<th>ELIMINAR</th>
       		</tr>
       	</thead>
       	<?php while($f = $sel->fetch_assoc()){ ?>
       	<tbody>
       		<tr>
       			<td><?php echo $f['titulo'] ?></td>
       			<td><?php echo $f['piepagina'] ?></td>
       			<td><?php echo $f['fechapublicacion'] ?></td>
       			<td><?php echo $f['nomyou'] ?></td>
       			<td>
       				<?php if ($f['estado']==1): ?>
							<a href="bloqueo_manual.php?us=<?php echo $f['idslider']?>&bl=<?php echo $f['estado'] ?>"><i class="material-icons blue-text">lock_open</i></a>
					<?php else: ?>
						<a href="bloqueo_manual.php?us=<?php echo $f['idslider']?>&bl=<?php echo $f['estado'] ?>"><i class="material-icons red-text">lock_outline</i></a>
       				<?php endif;  ?>
       			</td>
       			<td>
       				
       				<a href="#" class="btn-floating red " onclick="swal({  title: 'Esta seguro que desea eliminar al usuario?', text: 'Al eliminarlo no podra recuperarlo!!',  type: 'warning',  showCancelButton: true,confirmButtonColor: '#3085d6', cancelButtonColor: '#d33',  confirmButtonText: 'Si, eliminar!'}).then(function() {  location.href='aliminar_slider.php ?id=<?php echo $f['idslider']?>';})" >
       					<i class="material-icons">clear</i></a>
       			</td>
       			
       			

       		</tr>
       	</tbody>
       <?php } ?>
       </table>
      </div>      
    </div>
  </div>
</div>






<?php include '../extend/scripts.php'; ?>	
</body>
</html>

