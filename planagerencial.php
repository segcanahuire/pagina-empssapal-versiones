<?php
include 'conexion/conexion.php';?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <title>EMPSSAPAL SA</title>
    <meta name="description" content="Material Style Theme">
    <link rel="shortcut icon" href="assets/img/empssapal/empssapal.jpg?v=3">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="assets/css/preload.min.css">
    <link rel="stylesheet" href="assets/css/plugins.min.css">
    <link rel="stylesheet" href="assets/css/style.light-blue-500.min.css">
    <link rel="stylesheet" type="text/css" href="dist/snackbar.min.css" />
  </head>
<body>
  <?php include 'menu.php'; ?>

      <section class="mt-12">
        <div class="container">
             <div class="row masonry-container">
                <div class="col-lg-6 col-md-2 masonry-item">
                    <a href="#" class="img-thumbnail withripple">
                        <div class="thumbnail-container">
                            <img src="img/psr.jpg" width="20%" height="20%" alt="..." class="img-fluid">
                        </div>
                        <h5>ABOG. JUAN CARLOS ALVAREZ CCASA</h5>
                        <h6>GERENTE GENERAL</h6>
                        <a href="documentos/planagerencial/gerentegeneral.pdf" class="btn btn-raised btn-info">CV</a>
                        <a href="documentos/planagerencial/RESOLUCION-GG.pdf" class="btn btn-raised btn-info">Resolucion</a>
                    </a>

                </div>
                <div class="col-lg-6 col-md-2 masonry-item">
                    <a href="#" class="img-thumbnail withripple">
                        <div class="thumbnail-container">
                            <img src="img/gerentes.png" width="50%" height="10%" alt="..." class="img-fluid">
                        </div>

                    </a>
                </div>


              </div>
             <div class="row masonry-container">
                <div class="col-lg-4 col-md-2 masonry-item">
                    <a href="#" class="img-thumbnail withripple">
                        <div class="thumbnail-container">
                            <img src="img/psr.jpg" width="20%" height="20%" alt="..." class="img-fluid">
                        </div>
                        <h5>ISAAC QUISPE MOROCCO </h5>
                        <h6>GERENTE OPERACIONAL</h6>
                        <a href="documentos/CVs/gerencia/Cvissacc.pdf" class="btn btn-raised btn-info">CV</a>
                        <a href="#" class="btn btn-raised btn-info">Resolucion</a>
                    </a>

                </div>
                <div class="col-lg-4 col-md-2 masonry-item">
                    <a href="#" class="img-thumbnail withripple">
                        <div class="thumbnail-container">
                            <img src="img/psr.jpg" width="20%" height="20%" alt="..." class="img-fluid">
                        </div>
                        <h5>ING, FANY RUTH CARDEÑA APARICIO</h5>
                        <h6>GERENTE DE ADMINISTRACION Y FINANZAS</h6>
                        <a href="#" class="btn btn-raised btn-info">CV</a>
                        <a href="doc/EPS-CV-PERS001.pdf" class="btn btn-raised btn-info">Resolucion</a>
                    </a>
                </div>
                <div class="col-lg-4 col-md-2 masonry-item">
                    <a href="#" class="img-thumbnail withripple">
                        <div class="thumbnail-container">
                            <img src="img/psr.jpg" width="20%" height="20%" alt="..." class="img-fluid">
                        </div>
                        <h5>ABOG. FERNANDO MEDINA MENA </h5>
                        <h6>GERENTE COMERCIAL</h6>
                        <a href="documentos/CVs/gerencia/CvFernando.pdf" class="btn btn-raised btn-info">CV</a>
                        <a href="#" class="btn btn-raised btn-info">Resolucion</a>
                    </a>
                </div>

              </div>
        </div>
      </section>

      <?php include 'pie.php' ?>
