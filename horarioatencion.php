<?php
include 'conexion/conexion.php';?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <title>EMPSSAPAL SA</title>
    <meta name="description" content="Material Style Theme">
    <link rel="shortcut icon" href="assets/img/empssapal/empssapal.jpg?v=3">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="assets/css/preload.min.css">
    <link rel="stylesheet" href="assets/css/plugins.min.css">
    <link rel="stylesheet" href="assets/css/style.light-blue-500.min.css">
  </head>
<body>
    <?php include 'menu.php'; ?>

      <section class="mt-6">
        <div class="container">
          <h2 class="modal-title color-primary" id="myModalLabel">HORARIOS DE ATENCION.</h2>
           <form action="sugerencia/ins_sug.php" method="POST" >
          <div class="row d-flex justify-content-center">
            <div class="col-lg-4 col-md-6">
             <img src="imgoage/horarioatencion.png" alt="" width="100%" height="100%">
            </div>
            <div class="col-lg-8 col-md-6">
              <o>
                  <li> De Lunes a Viernes:  </li>
                  Mañanas: 8.00 a.m a 2:00 p.m.
              </o>
              <o>
                  <li>Sábados: ¡¡¡No hay atencion!!! </li>
                  AV:CONFEDERACION N° 567
              </o>
          </div>
        </div>
        </form>
        </div>
      </section>


      <?php include 'pie.php' ?>
