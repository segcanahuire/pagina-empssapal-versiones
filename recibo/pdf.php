
<?php 
include '../conexion/conexion.php';

$idcod = $con->real_escape_string(htmlentities($_GET['cod']));


$sel=$con->query("select * from mensuales a, clientes b WHERE a.cod_recibo=b.cod_recibo and b.cod_recibo='$idcod'");
$resul = $sel->fetch_assoc();
$nom_ape = $resul['nom_ape'];
$cod_recibo = $resul['cod_recibo'];
$direccion = $resul['direccion'];
$num_medidor = $resul['num_medidor'];
$tarifa = $resul['tarifa'];
$sector = $resul['sector'];
$mes = $resul['mes'];
$total = $resul['total'];
$vol = $resul['vol'];
$cargofijo = $resul['cargofijo'];
$c_agua = $resul['c_agua'];
$c_desague = $resul['c_desague'];
$otros = $resul['otros'];
$moras = $resul['moras'];
$igv = $resul['igv'];
$redondeo = $resul['redondeo'];
$cons_1 = $resul['cons_1'];
$cons_2 = $resul['cons_2'];
$cons_3 = $resul['cons_3'];
$cons_4 = $resul['cons_4'];
$cons_5 = $resul['cons_5'];
$cons_6 = $resul['cons_6'];



ob_start(); ?>

<h5 align="left"><b><img src="../img/cabecerarecibo.jpg"  width="100%"  height="20%"></b></h5>

<table border="1" style=”width: 100%” class="egt">
			<colgroup>
				<col style="width: 20%"/>
				<col style="width: 40%"/>
				<col style="width: 40%"/>
			</colgroup>
			<thead>
				<tr>
					<th rowspan="2"><h2>COD USUARIO: <?php echo $cod_recibo; ?></h2></th>
					<th colspan="2"><h4>DATOS PERSONALES </h4></th>
				</tr>
				<tr>
					<th><h6><?php echo $nom_ape ; ?></h6></th>
					<th><h6><?php echo $direccion; ?></h6></th>
				</tr>
			</thead>
			
			
			
		</table>
		<table border="0" style=”width: 100%” class="egt">
			<colgroup>
				<col style="width: 20%"/>
				<col style="width: 40%"/>
				<col style="width: 40%"/>
			</colgroup>
			
			
			<tbody>
				<tr>
					<td>TARIFA</td>
					<td><?php echo $tarifa ?></td>
				</tr>
				<tr>
					<td>SECTOR</td>
					<td><?php echo $sector ?></td>
				</tr>
				<tr>
					<td>NUMERO MEDIDOR</td>
					<td><?php echo $num_medidor ?></td>
				</tr>
			</tbody>
			
		</table>
		<table border="1">
			
			<tr>
				<th>GRAFICO</th>
				<th>CONSUMO ANTERIORES MESES</th>
				<th>DETALLE</th>
				<th>CONSUMO TOTAL</th>
			</tr>
			<tr>
				<td rowspan="3"><img src="../img/graficos.PNG" alt="" width="20%" height="20%"></td>
				<td>celda 6</td>
				<td>celda 8</td>
			</tr>
			
			<tr>
				<td>celda 8</td>
				<td>celda 9</td>
				<td rowspan="2"><?php echo '::    '.$total.'  SOLES'  ?></td>
			</tr>
			<tr>
				<td colspan="2">TOTAL A PAGAR</td>
			</tr>
		</table>

<h5 align="center"><b><img src="../img/BARRA.jpg"  width="100%"  height="5%"></b></h5>

<?php 
require_once '../dompdf/autoload.inc.php';
use Dompdf\Dompdf;
$dompdf = new Dompdf();
$dompdf->loadHtml(ob_get_clean());
$dompdf->setPaper('A4','portrait');
$dompdf->render();
$dompdf->stream('RECIBO VIRTUAL'.".pdf");
?>