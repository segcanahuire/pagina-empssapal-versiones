<?php
include 'conexion/conexion.php';?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <title>EMPSSAPAL SA</title>
    <meta name="description" content="Material Style Theme">
    <link rel="shortcut icon" href="assets/img/empssapal/empssapal.jpg?v=3">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="assets/css/preload.min.css">
    <link rel="stylesheet" href="assets/css/plugins.min.css">
    <link rel="stylesheet" href="assets/css/style.light-blue-500.min.css">
  </head>
<body>
    <?php include 'menu.php'; ?>


      <section class="mt-6">
        <div class="container">
          <h2 class="modal-title color-primary" id="myModalLabel">CENTROS DE COBRANZA.</h2>
           <form action="sugerencia/ins_sug.php" method="POST" >
          <div class="row d-flex justify-content-center">
            <div class="col-lg-4 col-md-6">
             <img src="imgoage/centroscobranza.png" alt="" width="50%" height="80%">
            </div>
            <div class="col-lg-8 col-md-6">
                <ul class="list-group">
                     <li class="list-group-item"><i class="zmdi zmdi-money"></i>F.F SERVICIOS GENERALES<span class="ml-auto badge-pill">Jirón Julio C. Tello N" 562 Urb. Manuel Prado</span></li>
                     <li class="list-group-item"><i class="zmdi zmdi-money"></i>ELECTROSAT COMUNICACIONES<span class="ml-auto badge-pill">Av. Arequipa Nº 505</span></li>
                     <li class="list-group-item"><i class="zmdi zmdi-money"></i>MULTISERVICIOS - INTERNET WARRIOR.NET<span class="ml-auto badge-pill">Av. San Felipe Nº 369</span></li>
                     <li class="list-group-item"><i class="zmdi zmdi-money"></i>TIENDA “MEGALITO”<span class="ml-auto badge-pill">Av. Miguel Grau Nº 1005</span></li>
                     <li class="list-group-item"><i class="zmdi zmdi-money"></i>MULTISERVICIOS LA PROVEEDORA<span class="ml-auto badge-pill">Jirón 2 de Mayo N" 544</span></li>
                     <li class="list-group-item"><i class="zmdi zmdi-money"></i>IMPRESORAS MARANGA<span class="ml-auto badge-pill">Jirón 2 de Mayo N" 147</span></li>
                     <li class="list-group-item"><i class="zmdi zmdi-money"></i>MINI MARKET VALENTINA<span class="ml-auto badge-pill">Av. Tomasa tito condemayta costado del colegio amauta</span></li>
                     <li class="list-group-item"><i class="zmdi zmdi-money"></i>INTERNET MARANGA<span class="ml-auto badge-pill">Av. Arequipa N" 135 (Altura de la UGEL Y UNSAAC)</span></li>
                     <li class="list-group-item"><i class="zmdi zmdi-money"></i>MULTISERVICIOS "KIG MOTOR"<span class="ml-auto badge-pill">Av. Arequipa N" 1233</span></li>
                     <li class="list-group-item"><i class="zmdi zmdi-money"></i>APV INDEPENDENCIA<span class="ml-auto badge-pill">Altura del Restaurant Langueño</span></li>
                </ul>
            </div>
        </div>
        </form>
        </div>
      </section>

<?php include 'pie.php' ?>
