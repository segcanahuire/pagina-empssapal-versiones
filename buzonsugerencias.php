<?php
include 'conexion/conexion.php';?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <title>EMPSSAPAL SA</title>
    <meta name="description" content="Material Style Theme">
    <link rel="shortcut icon" href="assets/img/empssapal/empssapal.jpg?v=3">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="assets/css/preload.min.css">
    <link rel="stylesheet" href="assets/css/plugins.min.css">
    <link rel="stylesheet" href="assets/css/style.light-blue-500.min.css">
  </head>
<body>
    <?php include 'menu.php'; ?>


      <section class="mt-6">
        <div class="container">
          <h4 class="modal-title color-primary" id="myModalLabel">En breve nos pondremos en contacto con usted.</h4>
           <form action="sugerencia/ins_sug.php" method="POST" >
          <div class="row d-flex justify-content-center">
            <div class="col-lg-4 col-md-6">
              <div class="card mt-4 card-danger wow zoomInUp">
                           <div class="form-group row">
                              <div class="col-lg-6">
                                  <input type="number" class="form-control" id="dni" name="dni" placeholder="DNI" required="off">
                              </div>
                              <div class="col-lg-12">
                                  <input type="text" class="form-control" id="nom_pe" name="nom_pe" placeholder="Nombres y apellidos" required="off">
                              </div>
                          </div>
                          <div class="form-group row">
                              <div class="col-lg-12">
                                  <input type="email" class="form-control" id="correo" name="correo" placeholder="Correo" required="off">
                              </div>
                              <div class="col-lg-12">
                                  <input type="number" class="form-control" id="movil" name="movil" placeholder="Número Celular" required="off">
                              </div>
                          </div>

              </div>
            </div>
            <div class="col-lg-8 col-md-6">
              <div class="card mt-4 card-warning wow zoomInUp">
                         <div class="form-group row justify-content-end">
                              <label for="textArea" class="col-lg-2 control-label">Sugerencia o Reclamo</label>

                              <div class="col-lg-10">
                                  <textarea class="form-control" rows="3" id="sugerencia" name="sugerencia" required="off"></textarea>
                                  <span class="help-block">Describanos su sugerencia</span>
                              </div>
                          </div>
                          <div class="form-group row justify-content-end">
                              <div class="col-lg-10">
                                  <button type="submit" class="btn btn-raised btn-primary">Enviar</button>
                                  <button type="button" class="btn btn-danger">Cancelar</button>
                              </div>
                          </div>
              </div>
          </div>
        </div>
        </form>
        </div>
      </section>

<?php include 'pie.php' ?>
