<?php include '../extend/header.php'; ?>
	<div class="row">
	  <div class="col s12"> 
	     <div class="card">
		    <div class="card-content">
		      <h1>Editar Perfil
		    </div>
		    <div class="card-tabs">
		      <ul class="tabs tabs-fixed-width">
		        <li class="tab"><a href="#datos">DATOS</a></li>
		        <li class="tab"><a class="active" href="#pass">CONTRASEÑA</a></li>
		      </ul>
		    </div>
		    <div class="card-content grey lighten-4">
		      <div id="datos">
		      	<form class="form" action="up_perfil.php" method="post" enctype="multipart/form-data" autocomplete="off" >
					
					
					<div class="input-field">				
      	<input type="text" name="nombre" title="NOMBRE DEL USUARIO"  id="nombre" 
      	onblur="may(this.value,this.id)"
      	required  value="<?php echo $_SESSION['nombre']?>">
		<label for="nombre">Nombre Completo del usuario</label>
					</div>

					<div class="input-field">				
      	<input type="email" name="correo" title="Correo"  id="correo"  value="<?php echo $_SESSION['correo']?>" >
		<label for="correo" >Correo Electronico</label>
					</div>
					<button type="submit" class="btn black"> ACTUALIZAR
<i class="material-icons">send</i>
					</button>
	       		</form>

		      </div>
		      <div id="pass">
			    <form class="form" action="up_pass.php" method="POST"  enctype="multipart/form-data">
				
			        <div class="input-field">				
      	<input type="text" name="pass1" title="Contraseña con Numeros, Letras Mayusculas y Minusculas entre 8 y 15 caracteres" pattern="[A-Za-z0-9]{8,15}"  id="pass1"  required >
<label for="pass1">Contraseña</label>
					</div>
					<div class="input-field">				
      	<input type="text"  title="Contraseña con Numeros, Letras Mayusculas y Minusculas entre 8 y 15 caracteres" pattern="[A-Za-z0-9]{8,15}"  id="pass2"  required >
<label for="pass2">Verificar Contraseña</label>
					</div>

					<button type="submit" id="btn_guardar" class="btn black"> Actualizar
<i class="material-icons">send</i>
					</button>
		       </form>
		    			

		      </div>
		    </div>
		  </div>
	  </div>
	</div>
<?php include '../extend/scripts.php'; ?>
<script src="../js/validacion.js"></script>

	
</body>
</html>

