<?php
include 'conexion/conexion.php';?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <title>EMPSSAPAL SA</title>
    <meta name="description" content="Material Style Theme">
    <link rel="shortcut icon" href="assets/img/empssapal/empssapal.jpg?v=3">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="assets/css/preload.min.css">
    <link rel="stylesheet" href="assets/css/plugins.min.css">
    <link rel="stylesheet" href="assets/css/style.light-blue-500.min.css">
    <link rel="stylesheet" type="text/css" href="dist/snackbar.min.css" />
  </head>
<body>
    <?php include 'menu.php'; ?>

      <section class="mt-12">
        <!--<div class="container">
        <div class="card card-royal">
            <div class="card-body overflow-hidden text-center">
                <span class="ms-icon ms-icon-round ms-icon-inverse color-royal ms-icon-lg mb-4"><i class="zmdi zmdi-city-alt"></i></span>
                <h4 class="color-royal">MOF</h4>
                <p>...</p>
            <button type="button" class="btn btn-default btn-raised" data-toggle="modal" data-target="#myModal2">
            VER
            </button>
             <a href="documentos/gobernabilidad/indicadoresdegestion/mof.pdf"  onclick="Snackbar.show({center: 'DESCARGANDO DOCUMENTOS'})" download="Colaterales" class="btn btn-raised btn-primary"><i class="fa fa-download"></i> DESCARGAR</a>
            </div>
        </div>
        <div class="modal" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
            <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
               <iframe src="documentos/gobernabilidad/indicadoresdegestion/mof.pdf#zoom=100&view=fitH" frameborder="0" width="1000" height="1000" marginheight="0" marginwidth="0" id="pdf" style="border: none;"></iframe>
            </div>
        </div>

        </div>-->
        <div class="row masonry-container">
          <h1 class="color-primary text-center" text-center>DOCUMENTOS DE ORGANO DE CONTROL INSTITUCIONAL  OCI</h1>
    <div class="col-lg-3 col-md-6 masonry-item">
       <div class="card card-royal">
            <div class="card-body overflow-hidden text-center">
                <span class="ms-icon ms-icon-round ms-icon-inverse color-royal ms-icon-lg mb-4"><i class="zmdi zmdi-city-alt"></i></span>
                <h4 class="color-royal">Modificacion del estatuto</h4>
                <p>...</p>
            <button type="button" class="btn btn-default btn-raised" data-toggle="modal" data-target="#modiesta">
            VER
            </button>
             <a href="documentos/ultimos/MODIFICACION DE ESTATUTO007.pdf"  onclick="Snackbar.show({center: 'DESCARGANDO DOCUMENTOS'})" download="MODIFICACION DE ESTATUTO" class="btn btn-raised btn-primary"><i class="fa fa-download"></i> DESCARGAR</a>
            </div>
        </div>
        <div class="modal" id="modiesta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
            <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
               <iframe src="documentos/ultimos/MODIFICACION DE ESTATUTO007.pdf#zoom=100&view=fitH" frameborder="0" width="1000" height="1000" marginheight="0" marginwidth="0" id="pdf" style="border: none;"></iframe>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 masonry-item">
        <div class="card card-royal">
            <div class="card-body overflow-hidden text-center">
                <span class="ms-icon ms-icon-round ms-icon-inverse color-royal ms-icon-lg mb-4"><i class="zmdi zmdi-city-alt"></i></span>
                <h4 class="color-royal">Oficio N° 146-2018</h4>
                <p>...</p>
            <button type="button" class="btn btn-default btn-raised" data-toggle="modal" data-target="#ofico146">
            VER
            </button>
             <a href="documentos/ultimos/OFICIO Nº 146-2018-OCI-MPC006.pdf"  onclick="Snackbar.show({center: 'DESCARGANDO DOCUMENTOS'})" download="OFICIO-146-2018-OCI" class="btn btn-raised btn-primary"><i class="fa fa-download"></i> DESCARGAR</a>
            </div>
        </div>
        <div class="modal" id="ofico146" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
            <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
               <iframe src="documentos/ultimos/OFICIO Nº 146-2018-OCI-MPC006.pdf#zoom=100&view=fitH" frameborder="0" width="1000" height="1000" marginheight="0" marginwidth="0" id="pdf" style="border: none;"></iframe>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 masonry-item">
       <div class="card card-royal">
            <div class="card-body overflow-hidden text-center">
                <span class="ms-icon ms-icon-round ms-icon-inverse color-royal ms-icon-lg mb-4"><i class="zmdi zmdi-city-alt"></i></span>
                <h4 class="color-royal">Documento de Implementacion</h4>
                <p>...</p>
            <button type="button" class="btn btn-default btn-raised" data-toggle="modal" data-target="#docimple">
            VER
            </button>
             <a href="documentos/ultimos/IMPLEMANTACION 1005.pdf"  onclick="Snackbar.show({center: 'DESCARGANDO DOCUMENTOS'})" download="DOC-IMPLEMENTACION" class="btn btn-raised btn-primary"><i class="fa fa-download"></i> DESCARGAR</a>
            </div>
        </div>
        <div class="modal" id="docimple" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
            <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
               <iframe src="documentos/ultimos/IMPLEMANTACION 1005.pdf#zoom=100&view=fitH" frameborder="0" width="1000" height="1000" marginheight="0" marginwidth="0" id="pdf" style="border: none;"></iframe>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 masonry-item">
       <div class="card card-royal">
            <div class="card-body overflow-hidden text-center">
                <span class="ms-icon ms-icon-round ms-icon-inverse color-royal ms-icon-lg mb-4"><i class="zmdi zmdi-city-alt"></i></span>
                <h4 class="color-royal">SEGUIMIENTO DE RECOMENDACIONES OCI</h4>
                <p>...</p>
            <button type="button" class="btn btn-default btn-raised" data-toggle="modal" data-target="#segoci">
            VER
            </button>
             <a href="documentos/ultimos/SEGUIMIENTO-OCI-2019.pdf"  onclick="Snackbar.show({center: 'DESCARGANDO DOCUMENTOS'})" download="SEGUIMIENTO-OCI" class="btn btn-raised btn-primary"><i class="fa fa-download"></i> DESCARGAR</a>
            </div>
        </div>
        <div class="modal" id="segoci" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
            <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
               <iframe src="documentos/ultimos/SEGUIMIENTO-OCI-2019.pdf#zoom=100&view=fitH" frameborder="0" width="1000" height="1000" marginheight="0" marginwidth="0" id="pdf" style="border: none;"></iframe>
            </div>
        </div>
    </div>

</div>
      </section>

    <?php include 'pie.php' ?>
