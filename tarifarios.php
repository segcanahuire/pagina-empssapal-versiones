<?php
include 'conexion/conexion.php';?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <title>EMPSSAPAL SA</title>
    <meta name="description" content="Material Style Theme">
    <link rel="shortcut icon" href="assets/img/empssapal/empssapal.jpg?v=3">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="assets/css/preload.min.css">
    <link rel="stylesheet" href="assets/css/plugins.min.css">
    <link rel="stylesheet" href="assets/css/style.light-blue-500.min.css">
    <link rel="stylesheet" type="text/css" href="dist/snackbar.min.css" />
  </head>
<body>
    <?php include 'menu.php'; ?>

      <section class="mt-12">
        <div class="container">
          <h4 class="modal-title color-primary" id="myModalLabel">TARIFAS.</h4>
          <center>   <a href="documentos/orientaciondelcliente/tarifas/tarifas.pdf"  onclick="Snackbar.show({center: 'DESCARGANDO DOCUMENTOS'})" download="tarifas" class="btn btn-raised btn-primary"><i class="fa fa-download"></i> DESCARGAR</a>
        </center>
        </div>

      </section>


    <?php include 'pie.php' ?>
