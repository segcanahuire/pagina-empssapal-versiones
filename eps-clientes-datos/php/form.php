<?php include '../extend/header.php';
include '../conexion/conexion.php';
include '../extend/permiso_doc.php';?>
  <link rel="stylesheet" type="text/css" href="../css/materialize.css">
  <link rel="stylesheet" type="text/css" href="../css/materialize.min.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../fonstgoogle/icon.css">
  <link rel="stylesheet" href="fonstgoogle/icon.css">
  <link rel="stylesheet" type="text/css" href="../css/sweetalert2.css">
<?php 
$id = $con->real_escape_string(htmlentities($_GET['id']));
$sel = $con->query("SELECT idclientes,nom_ape FROM clientes WHERE idclientes='$id'");
$datos = $sel->fetch_assoc();
?>
<div class="row">

    <div class="col s12">
      <div class="card">
        <div class="card-content">
         <span class="card-title">AGREGAR CORREO Y MOVIL DEL CLIENTE:</span>
         <br></br>
         <h4><?php echo $datos['nom_ape']?></h4>
        </div>      
      </div>
    </div>
<form action="insdatos.php" method="POST" enctype="multipart/form-data">
	  <div class="col s6 ">
    <div class="card vertical">
      	  <div class="input-field">
			<input type="hidden" name="idcapacidad" value="<?php echo $datos['idcapacidad'] ?>"  id="idcapacidad">
		 </div>
      	<div class="input-field">
				<i class="material-icons prefix">contact_phone</i>
				<input type="number" name="movil" title="movil" value="" onblur="may(this.value,this.id)" required  id="movil">
				<label for="movil">CELULAR</label>
		    </div>
        <div class="input-field">
        <i class="material-icons prefix">contact_mail</i>
        <input type="email" name="correo" title="correo" value="" onblur="may(this.value,this.id)" required  id="correo">
        <label for="correo">CORREO</label>
        </div>
    </div>
    <div class="card-content grey lighten-4"> 
        	<a href="index.php" class="btn-floating black"><i class="material-icons">arrow_back</i></a>
        	<button type="reset" class="btn-floating red"><i class="material-icons">cancel</i></button>
        	<button type="submit" class="btn-floating blue"><i class="material-icons">check</i></button>
        </div>
  </div>

</form>
    </div>




			  
<script>
    $('.modal').modal();
</script>
<?php include '../extend/scripts.php'; ?>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="	  crossorigin="anonymous"></script>
<script src="../js/materialize.min.js"></script>
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/sweetalert2.js"></script>
<script src="../js/materialize.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.3.2/sweetalert2.js"></script>
</body>
</html>