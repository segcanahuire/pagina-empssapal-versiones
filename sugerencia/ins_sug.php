<?php 
include '../conexion/conexion.php';
date_default_timezone_set('America/Lima');
if ($_SERVER['REQUEST_METHOD']=='POST') {
	$dni = $con->real_escape_string(htmlentities($_POST['dni']));
	//$nom_pe = $con->real_escape_string(htmlentities($_POST['nom_pe']));
	$correo = $con->real_escape_string(htmlentities($_POST['correo']));
	$movil = $con->real_escape_string(htmlentities($_POST['movil']));
	$sugerencia = $con->real_escape_string(htmlentities($_POST['sugerencia']));
	$fecha = date("Y-m-d H:i:s");
	
	
	
	 $url = "https://dniruc.apisperu.com/api/v1/dni/".$dni."?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6ImFsZWphbmRyby5jYW5haHVpcmUuaGlsYXJpQGdtYWlsLmNvbSJ9.7K-XaAUtQUZlouz1k3kRA9nRr-LRrCXjxhCY1PXaUa4";
	 $json = file_get_contents($url);
	 $datos =json_decode($json);
	 $dnivalidado = $datos->{'dni'};
	 $nombres = $datos->{'nombres'};
	 $apellidoPaterno = $datos->{'apellidoPaterno'};
	 $apellidoMaterno = $datos->{'apellidoMaterno'};
	 $nombrecompletos =   $nombres." ".$apellidoPaterno." ".$apellidoMaterno;
	 
	if($dnivalidado){
	    
	    if(strlen($movil)==9 ||strlen($movil)==0 ){
	
        	    $insmovil=$con->query("INSERT INTO mensajes VALUES ('','$dni','$nombrecompletos','$correo','$movil','$sugerencia','0','$fecha','')");
        	
        	header('location:../extend/alerta.php?msj=SU SUGERENCIA O RECLAMO FUE ENVIADO CON EXITO ESPERE QUE NOS PONDREMOS EN CONTACTO CON USTED....&c=home&p=in&t=success');
        	    
        	            
	        
	    }else{
	   header('location:../extend/alerta.php?msj=Su Celular no es valido&c=salir&p=in&t=error');
	}
    
	    
	}else{
	   header('location:../extend/alerta.php?msj=DNI Invalido&c=salir&p=in&t=error');
	}
	
	
	
}else{
	header('location:../extend/alerta.php?msj=UTILIZA EL FORMULARIO&c=us&p=in&t=error');
}
$con->close();
?>