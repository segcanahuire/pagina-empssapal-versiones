<?php include '../extend/header.php';
include '../conexion/conexion.php'; ?>
    <!-- Compiled and minified CSS -->
	<div class="row">
	  <div class="col s12">
	    <div class="card">
	      <div class="card-content">
	       <span class="card-title">BANDEJA DE RECEPCIONADOS</span>
	       <p>...</p>
	      </div>      
	    </div>
	  </div>
	</div>
<?php
$sel=$con->query("SELECT * FROM mensajes WHERE estado=0 ORDER BY idmensaje desc");
?>
<div class="row">
  <div class="col s12">
    <div class="card">
      <div class="card-content">
       <span class="card-title"></span>
       <table>
        <caption>SUGERENCIAS Y RECLAMOS</caption>
        <thead>
          <tr class="cabecera">
            <th>FECHA</th>
            <th>NOMBRES Y APELLIDOS</th>
            <th>MENSAJE</th>
            <th>RESPONDER</th>
            <th>ARCHIVAR</th>
          </tr>
        </thead>
        
        <tbody>
          <?php while($f = $sel->fetch_assoc()){ ?>
          <tr>
            <td><?php echo $f['fecha'] ?></td>
            <td><?php echo $f['nom_ape'] ?></td>
            <td><?php echo $f['sugerencia'] ?></td>
          <td><a href="respuesta.php?id=<?php echo $f['idmensaje'];  ?>'" class="btn-floating blue">
       			<i class="material-icons">sms</i></a>
       		</td>
                   
       		<td><a href="ins_arch.php?id=<?php echo $f['idmensaje']?>'" class="btn-floating red">
       					<i class="material-icons">clear</i></a>
       		</td>
          </tr>

       <?php } ?>
        </tbody>
       </table>
      </div>      
    </div>
  </div>

   <!-- Modal Structure -->
  <div id="modal1" class="modal">
    <div class="modal-content">
      <h4>Modal Header</h4>
      <div class="res_modal">
        
      </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-close waves-effect waves-green btn-flat">Agree</a>
    </div>
  </div>

</div><?php include '../extend/scripts.php'; ?>	

</body>
</html>






