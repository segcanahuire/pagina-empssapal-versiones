<?php include '../extend/header.php'; ?>
	<div class="row">
	  <div class="col s12">
	    <div class="card">
	      <div class="card-content">
	       <span class="card-title">BANDEJA DE SUGERENCIAS</span>
	       <p>...</p>
	      </div>      
	    </div>
	  </div>
	</div>
	<div class="row">
	    <div class="col s4 m4">
		    
		    <div class="card horizontal">
		      <div class="card-image">
		        <img src="img/recepcion.jpg">
		      </div>
		      <div class="card-stacked">
		        <div class="card-content">
		          <p>Reclamos y Sugerencias Recepcionados.</p>
		        </div>
		        <div class="card-action">
		          <a href="recepcion.php">VER</a>
		        </div>
		      </div>
		    </div>
		  </div>
		  <div class="col s4 m4">
		    <div class="card horizontal">
		      <div class="card-image">
		        <img src="img/respondido.png">
		      </div>
		      <div class="card-stacked">
		        <div class="card-content">
		          <p>Reclamos y Sugerencias Respondidas</p>
		        </div>
		        <div class="card-action">
		          <a href="respondidos.php">VER</a>
		        </div>
		      </div>
		    </div>
		  </div>
		  <div class="col s4 m4">
		    <div class="card horizontal">
		      <div class="card-image">
		        <img src="img/archivado.jpg">
		      </div>
		      <div class="card-stacked">
		        <div class="card-content">
		          <p>Reclamos y Sugerencias Archivadas</p>
		        </div>
		        <div class="card-action">
		          <a href="archivado.php">VER</a>
		        </div>
		      </div>
		    </div>
		  </div>
	</div>
<?php include '../extend/scripts.php'; ?>	
</body>
</html>

