<?php 
include '../conexion/conexion.php';
	$idmensaje = $con->real_escape_string(htmlentities($_GET['id']));
     $sel = $con->query("SELECT * FROM mensajes WHERE idmensaje='$idmensaje' ");
	 $var = $sel->fetch_assoc();
	 $buscardni = $var['dni'];

	 $url = "https://dniruc.apisperu.com/api/v1/dni/".$buscardni."?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6ImFsZWphbmRyby5jYW5haHVpcmUuaGlsYXJpQGdtYWlsLmNvbSJ9.7K-XaAUtQUZlouz1k3kRA9nRr-LRrCXjxhCY1PXaUa4";
	 $json = file_get_contents($url);
	 $datos =json_decode($json);
	 $dni = $datos->{'dni'};
	 $nombres = $datos->{'nombres'};
	 $apellidoPaterno = $datos->{'apellidoPaterno'};
	 $apellidoMaterno = $datos->{'apellidoMaterno'};
	 $nombrecompletos =   $nombres." ".$apellidoPaterno." ".$apellidoMaterno;
	 


 ?>
<?php include '../extend/header.php';
include '../conexion/conexion.php'; ?>
    <!-- Compiled and minified CSS -->
	<div class="row">
	  <div class="col s12">
	    <div class="card">
	      <div class="card-content">
	       <span class="card-title">RESPONDER AL USUARIO</span>
	      </div>      
	    </div>
	  </div>
	</div>
	<div class="row">
		<div class="col s6">
		<div class="card">
	      <div class="card-content">
	       <span class="card-title">INFORMACION DE USUARIO</span>
	         <form >
	         	<label for="">NOMBRES COMPLETOS</label>
	         	<h4><?php echo $nombrecompletos; ?></h4>
	         	<label for="">CELULAR </label>
	         	<h6><?php echo $var['movil']?></h6>
	         	<label for=""> CORREO</label>
	         	<h6><?php echo $var['correo'] ?></h6>
	         	<label for="">MENSAJE</label>
	         	<h6><?php echo $var['sugerencia'] ?></h6>


	         </form>
	      </div>      
	    </div>
		</div>
		<div class="col s6">
		<div class="card">
	      <div class="card-content">
	       <span class="card-title">OPCIONES DE RESPONDER</span>
	         <form action="enviar_mensaje.php" method="POST" accept-charset="utf-8">
				    <input type="hidden" name="id" value="<?php echo $idmensaje; ?>" placeholder="">
				    <input type="hidden" name="nombres" value="<?php echo $nombrecompletos; ?>" placeholder="">
				    <input type="hidden" name="movil" value="<?php echo $var['movil']?>" placeholder="">
				    <input type="hidden" name="correo" value="<?php echo $var['correo'] ?>" placeholder="">
				    <input type="hidden" name="" value="<?php echo $dni ?>" placeholder="">
				    <div class="input-field col s12">
			            <textarea id="textarea1" name="respuesta" class="materialize-textarea" placeholder="Escriba la respuesta"></textarea>
			        </div>
	         	<select class="browser-default" name="enviar">
	         		<option value="1">SOLO AL WHASSAT</option>
	         		<option value="2">SOLO AL CORREO</option>
	         		<option value="3">WHASSAT Y CORREO</option>
	         	</select>	
	         	<br></br><br></br>
	         	<button type="submit" class=" btn btn-small red">enviar</button>
	         </form>
	      </div>      
	    </div>	
		</div>
	</div>



</div><?php include '../extend/scripts.php'; ?>	

</body>
</html>




