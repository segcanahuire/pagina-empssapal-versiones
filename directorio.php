<?php
include 'conexion/conexion.php';?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <title>EMPSSAPAL SA</title>
    <meta name="description" content="Material Style Theme">
    <link rel="shortcut icon" href="assets/img/empssapal/empssapal.jpg?v=3">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="assets/css/preload.min.css">
    <link rel="stylesheet" href="assets/css/plugins.min.css">
    <link rel="stylesheet" href="assets/css/style.light-blue-500.min.css">
    <link rel="stylesheet" type="text/css" href="dist/snackbar.min.css" />
  </head>
<body>
    <?php include 'menu.php'; ?>


      <section class="mt-12">
        <div class="container">
            <div class="ms-collapse" id="accordion5" role="tablist" aria-multiselectable="true">
                <div class="mb-0 card card-primary">
                    <div class="card-header" role="tab" id="headingOne51">
                        <h4 class="card-title">
                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion5" href="#collapseOne51" aria-expanded="false" aria-controls="collapseOne5">
                                <i class="zmdi zmdi-pin"></i> ING JOSE MATEO SULLCA MEJIA
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne51" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne51">
                        <div class="card-body">
                            <div class="col-lg-6 col-md-2 masonry-item">
                                <a href="#" class="img-thumbnail withripple">
                                    <div class="thumbnail-container">
                                       <a href="documentos/CVs/cvsullcamejia.pdf" class="btn btn-xlg btn-raised btn-danger"><i class="zmdi zmdi-cloud"></i>  C.V.</a>
                                    </div>
                                    <div class="thumbnail-container">
                                        <a href="documentos/CVs/desigsullcamejia.pdf" class="btn btn-xlg btn-raised btn-danger"><i class="zmdi zmdi-cloud"></i>  Designacion</a>
                                    </div>
                                </a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ms-collapse" id="accordion5" role="tablist" aria-multiselectable="true">
                <div class="mb-0 card card-danger">
                    <div class="card-header" role="tab" id="headingOne52">
                        <h4 class="card-title">
                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion5" href="#collapseOne52" aria-expanded="false" aria-controls="collapseOne5">
                                <i class="zmdi zmdi-pin"></i> ING JUAN CALLAÑAUPA QUISPE
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne52" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne52">
                        <div class="card-body">
                            <div class="col-lg-6 col-md-2 masonry-item">
                                <a href="#" class="img-thumbnail withripple">
                                    <div class="thumbnail-container">
                                        <a href="documentos/CVs/cvcallanahupa.pdf" class="btn btn-xlg btn-raised btn-danger"><i class="zmdi zmdi-cloud"></i>  C.V.</a>
                                    </div>
                                    <div class="thumbnail-container">
                                        <a href="documentos/CVs/desigcallanahupa.pdf" class="btn btn-xlg btn-raised btn-danger"><i class="zmdi zmdi-cloud"></i>  Designacion</a>
                                    </div>
                                </a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ms-collapse" id="accordion5" role="tablist" aria-multiselectable="true">
                <div class="mb-0 card card-success">
                    <div class="card-header" role="tab" id="headingOne53">
                        <h4 class="card-title">
                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion5" href="#collapseOne53" aria-expanded="false" aria-controls="collapseOne5">
                                <i class="zmdi zmdi-pin"></i> ING JOSE MANUEL MONROY MEZA
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne53" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne53">
                        <div class="card-body">
                            <div class="col-lg-6 col-md-2 masonry-item">
                                <a href="#" class="img-thumbnail withripple">
                                    <div class="thumbnail-container">
                                        <a href="documentos/CVs/cvmonrroy.pdf" class="btn btn-xlg btn-raised btn-danger"><i class="zmdi zmdi-cloud"></i>  C.V.</a>
                                    </div>
                                    <div class="thumbnail-container">
                                        <a href="documentos/CVs/desicnacionmonrroy.pdf" class="btn btn-xlg btn-raised btn-danger"><i class="zmdi zmdi-cloud"></i>  Designacion</a>
                                    </div>
                                </a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
      </section>

      <?php include 'pie.php' ?>
