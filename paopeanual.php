<?php
include 'conexion/conexion.php';?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <title>EMPSSAPAL SA</title>
    <meta name="description" content="Material Style Theme">
    <link rel="shortcut icon" href="assets/img/empssapal/empssapal.jpg?v=3">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="assets/css/preload.min.css">
    <link rel="stylesheet" href="assets/css/plugins.min.css">
    <link rel="stylesheet" href="assets/css/style.light-blue-500.min.css">
    <link rel="stylesheet" type="text/css" href="dist/snackbar.min.css" />
  </head>
<body>
    <?php include 'menu.php'; ?>
         <section class="mt-6">

        <div class="container">
        <div class="ms-collapse" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="mb-0 card card-default">
        <div class="card-header" role="tab" id="headingOne">
            <h4 class="card-title ms-rotate-icon">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                    <i class="zmdi zmdi-attachment-alt"></i> Plan Operativo Anual 2015
                </a>
            </h4>
        </div>
        <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-body">
                <div class="card card-royal">
                    <div class="card-body overflow-hidden text-center">
                        <span class="ms-icon ms-icon-round ms-icon-inverse color-royal ms-icon-lg mb-4"><i class="zmdi zmdi-city-alt"></i></span>
                        <h4 class="color-royal">Plan Operativo Anual 2015 </h4>
                        <p>...</p>
                     <!-- Button trigger modal -->
                    <button type="button" class="btn btn-default btn-raised" data-toggle="modal" data-target="#myModal1">
                    VER
                    </button>
                     <a href="documentos/gobernabilidad/planoperativoanual/planoperativoanual2015.pdf"  onclick="Snackbar.show({center: 'DESCARGANDO DOCUMENTOS'})" download="Colaterales" class="btn btn-raised btn-primary"><i class="fa fa-download"></i> DESCARGAR</a>
                    </div>
                </div>
                <!-- Modal -->
                <div class="modal" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
                    <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
                       <iframe src="documentos/gobernabilidad/planoperativoanual/planoperativoanual2015.pdf#zoom=100&view=fitH" frameborder="0" width="1000" height="1000" marginheight="0" marginwidth="0" id="pdf" style="border: none;"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mb-0 card card-default">
        <div class="card-header" role="tab" id="headingTwo">
            <h4 class="card-title ms-rotate-icon">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    <i class="zmdi zmdi-attachment-alt"></i> Plan Operativo Anual 2016
                </a>
            </h4>
        </div>
        <div id="collapseTwo" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
            <div class="card-body">
                <div class="card card-royal">
                      <div class="card-body overflow-hidden text-center">
                          <span class="ms-icon ms-icon-round ms-icon-inverse color-royal ms-icon-lg mb-4"><i class="zmdi zmdi-city-alt"></i></span>
                          <h4 class="color-royal">Plan Operativo Anual 2016 </h4>
                          <p>...</p>
                       <!-- Button trigger modal -->
                      <button type="button" class="btn btn-default btn-raised" data-toggle="modal" data-target="#myModal2">
                      VER
                      </button>
                       <a href="documentos/gobernabilidad/planoperativoanual/planoperativoanual2016.pdf"  onclick="Snackbar.show({center: 'DESCARGANDO DOCUMENTOS'})" download="Colaterales" class="btn btn-raised btn-primary"><i class="fa fa-download"></i> DESCARGAR</a>
                      </div>
                  </div>
                  <!-- Modal -->
                  <div class="modal" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
                      <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
                         <iframe src="documentos/gobernabilidad/planoperativoanual/planoperativoanual2016.pdf#zoom=100&view=fitH" frameborder="0" width="1000" height="1000" marginheight="0" marginwidth="0" id="pdf" style="border: none;"></iframe>
                      </div>
                  </div>
            </div>
        </div>
    </div>
    <div class="mb-0 card card-default">
        <div class="card-header" role="tab" id="headingThree">
            <h4 class="card-title ms-rotate-icon">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    <i class="zmdi zmdi-attachment-alt"></i> Plan Operativo Anual 2019
                </a>
            </h4>
        </div>
        <div id="collapseThree" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
            <div class="card-body">
                <div class="card card-royal">
                    <div class="card-body overflow-hidden text-center">
                        <span class="ms-icon ms-icon-round ms-icon-inverse color-royal ms-icon-lg mb-4"><i class="zmdi zmdi-city-alt"></i></span>
                        <h4 class="color-royal">Plan Operativo Anual 2019 </h4>
                        <p>...</p>
                     <!-- Button trigger modal -->
                    <button type="button" class="btn btn-default btn-raised" data-toggle="modal" data-target="#myModal3">
                    VER
                    </button>
                     <a href="documentos/gobernabilidad/planoperativoanual/planoperativoanual2019.pdf"  onclick="Snackbar.show({center: 'DESCARGANDO DOCUMENTOS'})" download="Colaterales" class="btn btn-raised btn-primary"><i class="fa fa-download"></i> DESCARGAR</a>
                    </div>
                </div>
                <!-- Modal -->
                <div class="modal" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
                    <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
                       <iframe src="documentos/gobernabilidad/planoperativoanual/planoperativoanual2019.pdf#zoom=100&view=fitH" frameborder="0" width="1000" height="1000" marginheight="0" marginwidth="0" id="pdf" style="border: none;"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</section>


<?php include 'pie.php' ?>
