<?php
include 'conexion/conexion.php';?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <title>EMPSSAPAL SA</title>
    <meta name="description" content="Material Style Theme">
    <link rel="shortcut icon" href="assets/img/empssapal/empssapal.jpg?v=3">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="assets/css/preload.min.css">
    <link rel="stylesheet" href="assets/css/plugins.min.css">
    <link rel="stylesheet" href="assets/css/style.light-blue-500.min.css">
    <link rel="stylesheet" type="text/css" href="dist/snackbar.min.css" />
  </head>
<body>
    <?php include 'menu.php'; ?>


      <section class="mt-12">
        <div class="container">
             <div class="panel panel-danger">
                  <div class="panel-heading">
                      <h3 class="panel-title">NUESTRA UBICACION GEOGRAFICA</h3>
                  </div>
                  <div class="panel-body">
                     <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1367.161096481157!2d-71.22895961377289!3d-14.25814841054106!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x916ed52e2b4e4309%3A0xea1c3a04b0f4d4e4!2sEPS-EMPSSAPAL+SA!5e0!3m2!1ses!2ses!4v1487622657606" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                  </div>
              </div>

        </div>
      </section>
<?php include 'pie.php' ?>
